"use strict";


$(window).on('load', function() {
    //for use in production please remove this setTimeOut
    setTimeout(function(){
        $('.preloader').addClass('preloader-deactivate');
    }, 2000);
    //uncomment this line for use this snippet in production
    //	$('.preloader').addClass('preloader-deactivate');
});

//check for browser os
    var isMobile = false;
    var isiPhoneiPad = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        isMobile = true;
    }

    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        isiPhoneiPad = true;
    }

    function SetMegamenuPosition() {
        if ($(window).width() > 991) {
            setTimeout(function () {
                var totalHeight = $('nav.navbar').outerHeight();
                $('.mega-menu').css({top: totalHeight});
                if ($('.navbar-brand-top').length === 0)
                    $('.dropdown.simple-dropdown > .dropdown-menu').css({top: totalHeight});
            }, 200);
        } else {
            $('.mega-menu').css('top', '');
            $('.dropdown.simple-dropdown > .dropdown-menu').css('top', '');
        }
    }

    $(window).on("scroll", init_scroll_navigate);

    function init_scroll_navigate() {

        /* ===================================
            sticky nav Start
         ====================================== */
        var headerHeight = $('nav').outerHeight();
        if (!$('header').hasClass('no-sticky')) {
            if ($(document).scrollTop() >= headerHeight) {
                $('header').addClass('sticky');

            } else if ($(document).scrollTop() <= headerHeight) {
                $('header').removeClass('sticky');
            }
        }

        /* ===================================
         header appear on scroll up
         ====================================== */

        //scroll nav colors
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > 70) { // Set position from top to add class
                $('.sticky').addClass("header-appear");
                $('.dropdown.on').removeClass('on').removeClass('open').find('.dropdown-menu').fadeOut(100);
                $('.logo-appear').addClass("display_none");
                $('.index-only-side-nav .navbar  .navbar-brand').addClass("display_none");
            }
            else {
                $('header').removeClass("header-appear");
                $('.logo-appear').removeClass("display_none");
                $('.index-only-side-nav .navbar  .navbar-brand').removeClass("display_none");
            }
        });
    }

    /*==============================================================*/
//Search - START CODE
    /*==============================================================*/
    function ScrollStop() {
        return false;
    }

    function ScrollStart() {
        return true;
    }

    function validationSearchForm() {
        var error = true;
        $('#search-header input[type=text]').each(function (index) {
            if (index === 0) {
                if ($(this).val() === null || $(this).val() === "") {
                    $("#search-header").find("input:eq(" + index + ")").css({
                        "border": "none",
                        "border-bottom": "2px solid red"
                    });
                    error = false;
                } else {
                    $("#search-header").find("input:eq(" + index + ")").css({
                        "border": "none",
                        "border-bottom": "2px solid #000"
                    });
                }
            }
        });
        return error;
    }

    /*==============================================================
     Search - END CODE
     ==============================================================*/


    /* ===================================
     Scroll Top
     ====================================== */

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 150)
            $('.scroll-top-arrow').fadeIn('slow');
        else
            $('.scroll-top-arrow').fadeOut('slow');
    });
//Click event to scroll to top
    $(document).on('click', '.scroll-top-arrow', function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    /*==============================================================
    smooth scroll
    ==============================================================*/


//scroll sections
    $(".scroll").on('click', function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 700);
    });

    /*==============================================================
     counter
     ==============================================================*/

    $(function ($) {
        animatecounters();
    });

    function animatecounters() {
        $('.timer').each(count);

        function count(options) {
            var $this = $(this);
            options = $.extend({}, options || {}, $this.data('countToOptions') || {});
            $this.countTo(options);
        }
    }

    /*==============================================================*/
//magnificPopup Start
    /*==============================================================*/
    $('.header-search-form').magnificPopup({
        mainClass: 'mfp-fade',
        closeOnBgClick: true,
        preloader: false,
        // for white backgriund
        fixedContentPos: false,
        closeBtnInside: false,
        callbacks: {
            open: function () {
                setTimeout(function () {
                    $('.search-input').focus();
                }, 500);
                $('#search-header').parent().addClass('search-popup');
                if (!isMobile) {
                    $('body').addClass('overflow-hidden');
                    //$('body').addClass('position-fixed');
                    $('body').addClass('width-100');
                    document.onmousewheel = ScrollStop;
                } else {
                    $('body, html').on('touchmove', function (e) {
                        e.preventDefault();
                    });
                }
            },
            close: function () {
                if (!isMobile) {
                    $('body').removeClass('overflow-hidden');
                    //$('body').removeClass('position-fixed');
                    $('body').removeClass('width-100');
                    $('#search-header input[type=text]').each(function (index) {
                        if (index == 0) {
                            $(this).val('');
                            $("#search-header").find("input:eq(" + index + ")").css({
                                "border": "none",
                                "border-bottom": "2px solid rgba(255,255,255,0.5)"
                            });
                        }
                    });
                    document.onmousewheel = ScrollStart;
                } else {
                    $('body, html').unbind('touchmove');
                }
            }
        }
    });

    /* ===================================
     Log In Form
     ====================================== */

    $(function ($) {
        var modal = document.getElementById('id01');
    });


    /* ===================================
     owl Carousel xbox
         ====================================== */

    var owl4 = $('.owl-xbox');
    owl4.owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,

            },
            600: {
                items: 1,

            },
            1000: {
                items: 1,
            }
        }
    });

    var owl4 = $('.owl-team');
    owl4.owlCarousel({
        loop: true,
        autoplay: true,
        dots: false,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,

            },
            600: {
                items: 2,

            },
            1000: {
                items: 3,
            }
        }
    });

    var owl4 = $('.owl-blog');
    owl4.owlCarousel({
        loop: true,
        autoplay: true,
        dots: false,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,

            },
            600: {
                items: 2,

            },
            1000: {
                items: 3,
            }
        }
    });

    var owl4 = $('.owl-testimonial');
    owl4.owlCarousel({
        loop: true,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,

            },
            600: {
                items: 1,

            },
            1000: {
                items: 1,
            }
        }
    });

    /* ===================================
     owl end
         ====================================== */


    /*-- Side Nav ----*/

// Push Menus

    if ($("body").hasClass("index-only-side-nav")) {
        var $menuLeft = $(".pushmenu-left");
        var $menuRight = $(".pushmenu-right");
        var $toggleleft = $(".menu_bars.left");
        var $toggleright = $(".menu_bars.right");
        $toggleright.on("click", function () {
            $('.menu_bars').toggleClass("active");
            $menuRight.toggleClass("pushmenu-open");
            $("body").toggleClass("pushmenu-push-toLeft");
            $(".navbar").toggleClass("navbar-right_open");
            return false;
        });

        $('.push_nav li a').on('click', function () {
            $toggleright.toggleClass("active");
            $menuRight.toggleClass("pushmenu-open");
            $("body").toggleClass("pushmenu-push-toLeft");
            $(".navbar").toggleClass("navbar-right_open");
            return true;
        });
    }

    /*-- Toggle Nav ----*/
    $('.navbar .collapse li a').on('click', function () {
        $('.navbar .collapse').removeClass('in');
        $('.navbar a.dropdown-toggle').addClass('collapsed');
    });

    /* Initializing Particles Plugin */

    if ($(window).width() > 767) {

        if ($("body").hasClass("particles_special_id")) {
            window.onload = function () {
                Particles.init({
                    selector: '#particles_bg',
                    color: '#ffffff',
                    connectParticles: true,
                    sizeVariations: 7,
                    maxParticles: 140,
                });
                console.log('callback - particles.js config loaded');
            };
        }
    }

    /*-- cube Portfolio ----*/

    (function ($, window, document, undefined) {
        // init cubeportfolio
        $('#js-grid-mosaic-flat').cubeportfolio({
            filters: '#js-filters-mosaic-flat',
            layoutMode: 'mosaic',
            sortByDimension: true,
            mediaQueries: [{
                width: 1500,
                cols: 6,
            }, {
                width: 1100,
                cols: 4,
            }, {
                width: 800,
                cols: 3,
            }, {
                width: 480,
                cols: 2,
                options: {
                    caption: '',
                    gapHorizontal: 15,
                    gapVertical: 15,
                }
            }],
            defaultFilter: '*',
            animationType: 'fadeOutTop',
            gapHorizontal: 0,
            gapVertical: 0,
            gridAdjustment: 'responsive',
            caption: 'fadeIn',
            displayType: 'fadeIn',
            displayTypeSpeed: 100,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

            plugins: {
                loadMore: {
                    element: '#js-loadMore-mosaic-flat',
                    action: 'click',
                    loadItems: 3,
                }
            },
        });
    })(jQuery, window, document);


    /*-- push menu ----*/

    if ($("body").hasClass("blue-push-menu")) {
        jQuery(document).ready(function ($) {
            $('.toggle-menu').jPushMenu();
        });
    }

if ($("body").hasClass("typeit-version")) {
/*-- wow intialize--*/
if ($(window).width() > 767) {
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: false,
        live: true
    });
    new WOW().init();
}

/*Personal resume page*/
$("#my_designation").typeIt({
    speed: 40,
    autoStart: false,
    loop: true,
})
    .tiType("Financial Advisor")
    .tiSettings({
        speed: 700
    })
    .tiPause(1000)
    .tiSettings({
        speed: 50
    })
    .tiDelete()
    .tiType("Charted Accountent");
    }
