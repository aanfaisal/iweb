@extends('layouts.auth.app')

@section('title')
Request Reset Password
@endsection

@section('content')

<div class="container container-login container-transparent animated fadeIn">
    <h3 class="text-center">@yield('title')</h3>
    <div class="login-form">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif

        <form id="reset-pass" name="reset-pass" method="POST" action="{{ route('password.email') }}"
            enctype="multipart/form-data" novalidate>
            @csrf

            <input type="text" hidden id="no_admin" value="{{ $data->no_whatapps }}">
            <div class="form-group">
                <label for="login" class="placeholder">E-mail / Username</label>

                <input id="login" type="text"
                    class="form-control{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                    name="login" value="{{ old('username') ?: old('email') }}" required autofocus>

                @if ($errors->has('username') || $errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="captcha" class="placeholder"><b>Captcha</b></label>
                <div class="position-relative">
                    <span id="captcha">{!! Captcha::img() !!}</span>
                    <button type="button" class="btn btn-danger" class="reload" id="reload">
                        &#x21bb;
                    </button>
                </div>
            </div>
            <div class="form-group">
                <label for="captcha" class="placeholder">Enter Captcha</label>
                <div class="position-relative">
                    <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                </div>
            </div>
    </div>
    <div class="row form-action">
        <div class="col-md-6">
            <a href="{{ route('login') }}" id="show-signin" class="btn btn-danger w-100 fw-bold">Cancel</a>
        </div>
        <div class="col-md-6">
            <button id="resetpass" class="btn btn-primary w-100 fw-bold">
                Kirim Password
            </button>
        </div>
    </div>
    </form>
</div>
@endsection

@section('js')
<!--  validation script  -->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>

<script type="text/javascript">
    (function() {
    'use strict';
        window.addEventListener('load', function() {
            var form = document.getElementById('reset-pass');
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        }, false);
    })();


    $(document).ready(function() {
        $('#reload').click(function () {
            $.ajax({
                type: 'GET',
                url: '{{ route('reloadCaptcha') }}',
                success: function (data) {
                    $("#captcha").html(data.captcha);
                }
            });
        });

        $('#resetpass').click(function() {
            if ($("#reset-pass").valid()) {

                // ajax
                var input_nama = $("#login").val();
                var no_admin = $("#no_admin").val();

                /* Whatsapp Settings */
                var walink = 'https://web.whatsapp.com/send',
                            phone = no_admin,
                            walink2 =
                            'Halo saya ingin Reset Password IWEB ini dengan rincian :',
                            text_yes = 'Terkirim.',
                            text_no = 'Isi semua Formulir lalu klik Send.';

                /* Smartphone Support */
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                            var walink = 'whatsapp://send';
                }
                if ("" != input_nama.value) {
                            /* Call Input Form */
                            var input_wa = $("#username").val(),
                                input_password = $("#password").val(),
                                input_paket = $("#paket").val();

                            /* Price Total*/
                            /* Final Whatsapp URL */
                            var ojan_whatsapp = walink + '?phone=' + phone +
                                    '&text=' + walink2 + '%0A%0A' +
                                    '*Username/Email* : ' + input_nama + '%0A';

                            /* Whatsapp Window Open */
                            //   $("#isi_dong").reset();
                            window.open(ojan_whatsapp, '_blank');

                            document.getElementById("reset-pass").submit();
                } else {
                            Swal.fire({
                                    icon: "error",
                                    title: "Mohon Maaf !",
                                    text: text_no
                            });
                }

            }
        });
    });

</script>

@endsection
