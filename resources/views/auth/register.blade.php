@extends('layouts.auth.app')

@section('title')
Registrasi
@endsection

@section('content')

<div class="container container-login container-transparent animated fadeIn">
    <h3 class="text-center">@yield('title')</h3>
    <div class="login-form">
        <form id="register-form" name="register-form" method="POST" action="{{ route('register') }}"
            enctype="multipart/form-data" novalidate>
            @csrf

            <input type="text" hidden id="no_admin" value="{{ $data->no_whatapps }}">
            <div class="form-group">
                <div class="input-icon">
                    <span class="input-icon-addon">
                        <i class="fas fa-user-astronaut"></i>
                    </span>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                        value="{{ old('name') }}" required autocomplete="name" placeholder="Nama Lengkap" autofocus>
                </div>

                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <span class="invalid-feedback">
                    Nama Wajib Diisi
                </span>
            </div>

            <div class="form-group">
                <div class="input-icon">
                    <span class="input-icon-addon">
                        <i class="fas fa-birthday-cake"></i>
                    </span>
                    <input onfocus="(this.type='date')" onblur="(this.type='text')" id="birthday" type="text"
                        class="form-control @error('birthday') is-invalid @enderror" name="birthday"
                        value="{{ old('birthday') }}" required autocomplete="birthday" placeholder="Birthday" autofocus>
                </div>

                @error('birthday')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <span class="invalid-feedback">
                    Birthday Wajib Diisi
                </span>
            </div>



            <div class="form-group">
                <div class="input-icon">
                    <span class="input-icon-addon">
                        <i class="fa fa-user"></i>
                    </span>
                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror"
                        name="username" value="{{ old('username') }}" placeholder="Username" required
                        autocomplete="username" autofocus>
                </div>

                @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <span class="invalid-feedback">
                    Username Wajib Diisi
                </span>
            </div>

            <div class="form-group">
                <div class="input-icon">
                    <span class="input-icon-addon">
                        <i class="fas fa-envelope"></i>
                    </span>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email">

                </div>


                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <span class="invalid-feedback">
                    Email Wajib Diisi
                </span>
            </div>
            <div class="form-group">
                <div class="input-icon">
                    <span class="input-icon-addon">
                        <i class="fab fa-whatsapp-square"></i>
                    </span>
                    <input id="no_whatapps" type="number"
                        class="form-control @error('no_whatapps') is-invalid @enderror" name="no_whatapps"
                        value="{{ old('no_whatapps') }}" placeholder="Phone Number (Connect to Whatapps)" required>
                </div>

                @error('no_whatapps')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <span class="invalid-feedback">
                    No. Whatsapps Wajib Diisi
                </span>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-address-card"></i></span>
                    </div>
                    <input id="webaddress" type="text" class="form-control @error('webaddress') is-invalid @enderror"
                        name="webaddress" value="{{ old('webaddress') }}" placeholder="Web Link" required>
                    <div class="input-group-append">
                        <span class="input-group-text">.iweb.co.id</span>
                    </div>
                </div>

                @error('webaddress')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <span class="invalid-feedback">
                    Web Link Wajib Diisi
                </span>
            </div>

            <div class="form-group">
                <div class="position-relative">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" placeholder="Password" required autocomplete="new-password">

                    <div class="show-password">
                        <i class="icon-eye"></i>
                    </div>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <span class="invalid-feedback">
                        Password Wajib Diisikan
                    </span>
                </div>
            </div>
            <div class="form-group">
                <div class="position-relative">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                        placeholder="Repeat Password" required autocomplete="new-password">

                    <div class="show-password">
                        <i class="icon-eye"></i>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="captcha" class="placeholder"><b>Captcha</b></label>
                <div class="position-relative">
                    <span id="captcha">{!! Captcha::img() !!}</span>
                    <button type="button" class="btn btn-danger" class="reload" id="reload">
                        &#x21bb;
                    </button>
                </div>
            </div>
            <div class="form-group">
                <div class="position-relative">
                    <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                </div>
            </div>
            <div class="row form-sub m-0">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="agree" id="agree">
                    <label class="custom-control-label" for="agree">Saya setuju dengan Syarat & Ketentuan.</label>
                </div>
            </div>
            <div class="row form-action">

                <div class="col-md-12">
                    <button id="register" class="btn btn-primary w-100 fw-bold">
                        CREATE MY IWEB
                    </button>
                </div>
            </div>

        </form>


    </div>
</div>

@endsection

@section('js')
<!--  validation script  -->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>

<script type="text/javascript">
    (function() {
    'use strict';
        window.addEventListener('load', function() {
            var form = document.getElementById('register-form');
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        }, false);
    })();


    $(document).ready(function() {
        $('#reload').click(function () {
            $.ajax({
                type: 'GET',
                url: '{{ route('reloadCaptcha') }}',
                success: function (data) {
                    $("#captcha").html(data.captcha);
                }
            });
        });

    });
        // $('#register').click(function() {
        //     if ($("#register-form").valid()) {

        //         // ajax
        //         var input_nama = $("#name").val();
        //         var no_admin = $("#no_admin").val();

        //         /* Whatsapp Settings */
        //         var walink = 'https://web.whatsapp.com/send',
        //                     phone = no_admin,
        //                     walink2 =
        //                     'Halo saya ingin Registrasi IWEB ini dengan rincian :',
        //                     text_yes = 'Terkirim.',
        //                     text_no = 'Isi semua Formulir lalu klik Send.';

        //         /* Smartphone Support */
        //         if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        //                     var walink = 'whatsapp://send';
        //         }
        //         if ("" != input_nama.value) {
        //                     /* Call Input Form */
        //                     var input_wa = $("#no_whatapps").val(),
        //                         input_password = $("#password").val(),
        //                     /* Price Total*/
        //                     /* Final Whatsapp URL */
        //                     var ojan_whatsapp = walink + '?phone=' + phone +
        //                             '&text=' + walink2 + '%0A%0A' +
        //                             '*Name* : ' + input_nama + '%0A' +
        //                             '*Nomor Whatsapp* : ' + input_wa + '%0A';

        //                     /* Whatsapp Window Open */
        //                     //   $("#isi_dong").reset();
        //                     window.open(ojan_whatsapp, '_blank');

        //                     document.getElementById("register-form").submit();
        //         } else {
        //                     Swal.fire({
        //                             icon: "error",
        //                             title: "Mohon Maaf !",
        //                             text: text_no
        //                     });
        //         }

        //     }
        // });

</script>

@endsection
