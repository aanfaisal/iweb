@extends('layouts.auth.app')

@section('title')
Login
@endsection

@section('content')

<div class="container container-login container-transparent animated fadeIn">
    <h3 class="text-center">Login</h3>
    <div class="login-form">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group">
                <div class="input-icon">
                    <span class="input-icon-addon">
                        <i class="fa fa-user"></i>
                    </span>
                    <input id="login" type="text"
                        class="form-control{{ $errors->has('username') || $errors->has('email') ? ' is-invalid' : '' }}"
                        name="login" value="{{ old('username') ?: old('email') }}" placeholder="Username / Email"
                        required autofocus>

                    @if ($errors->has('username') || $errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">

                <div class="position-relative">
                    <input id="password" name="password" type="password"
                        class="form-control  @error('password') is-invalid @enderror" required
                        autocomplete="current-password" placeholder="Password">
                    <div class="show-password">
                        <i class="icon-eye"></i>
                    </div>
                </div>

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror

            </div>
            <div class="form-group form-action-d-flex mb-3">
                <div class="row">
                    <div class="col">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{
                                old('remember') ? 'checked' : '' }}>

                            <label class="custom-control-label m-0" for="remember">
                                Ingat Saya
                            </label>

                        </div>
                    </div>
                    <div class="col">

                        @if (Route::has('password.request'))
                        <a class="link" href="{{ route('password.request') }}">
                            Lupa Password
                        </a>
                        @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-primary col-md-5 float-right mt-3 mt-sm-0 fw-bold">
                    LOGIN IWEB
                </button>
            </div>

            @if (Route::has('register'))

            <div class="login-account">
                <span class="msg">Belum Punya Akun ?</span>
                <a href="{{ route('register') }}" id="show-signup" class="link">{{ __('Register') }}</a>

            </div>
            @endif



        </form>

    </div>
</div>

@endsection
