<!-- blog -->
<section id="blog" class="blog bg-light-gray xs-text-center">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
                <div class="blog-title margin-20px-bottom">
                    <div class="alt-font text-small margin-5px-bottom text-capitalize text-dark-gray">Publish what
                        you
                        think
                    </div>
                    <h5 class="text-capitalize alt-font text-extra-dark-gray font-weight-500 sm-width-100 xs-width-100">
                        Recent News</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 no-padding xs-padding-15px-lr">
                <div class="owl-blog owl-carousel owl-theme blog-grid blog-post-style5">
                    <!-- start blog post item -->
                    <div class="item margin-10px-lr grid-item last-paragraph-no-margin margin-20px-bottom">
                        <div class="blog-post bg-white">
                            <div class="blog-post-images overflow-hidden">
                                <a href="#.">
                                    <img src="{{ asset('main/img/blog-img5.jpg') }}" alt="blog">
                                </a>
                                <div
                                    class="blog-categories bg-white text-capitalize bg-blue text-extra-small alt-font text-white">
                                    Finza News
                                </div>
                            </div>
                            <div
                                class="post-details inner-match-height padding-40px-all sm-padding-20px-all xs-padding-30px-tb">
                                <div class="blog-hover-color"></div>
                                <a href="#."
                                    class="alt-font post-title text-medium text-extra-dark-gray display-block md-width-100 margin-5px-bottom">Everything
                                    You Need To Know About Finza.</a>
                                <div class="author">
                                    <span
                                        class="text-medium-gray text-capitalize text-extra-small display-inline-block"><i
                                            class="fa fa-user text-blue margin-5px-right"></i><a href="#."
                                            class="text-medium-gray"> Admin</a>&nbsp;&nbsp;|&nbsp;&nbsp;20 Feb
                                        2018</span>
                                </div>
                                <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum standard dummy...</p>
                            </div>
                        </div>
                    </div>
                    <!-- end blog post item -->
                    <!-- start blog post item -->
                    <div class="item margin-10px-lr grid-item last-paragraph-no-margin margin-20px-bottom">
                        <!-- start blog item -->
                        <div class="blog-post bg-white">
                            <div class="blog-post-images overflow-hidden">
                                <a href="#.">
                                    <img src="{{ asset('main/img/blog-img4.jpg') }}" alt="blog">
                                </a>
                                <div
                                    class="blog-categories bg-white text-capitalize bg-green text-extra-small alt-font text-white">
                                    Finza News
                                </div>
                            </div>
                            <div
                                class="post-details inner-match-height padding-40px-all sm-padding-20px-all xs-padding-30px-tb">
                                <div class="blog-hover-color"></div>
                                <a href="#."
                                    class="alt-font post-title text-medium text-extra-dark-gray display-block md-width-100 margin-5px-bottom">How
                                    Difficult Is It To Make Payment Finza</a>
                                <div class="author">
                                    <span
                                        class="text-medium-gray text-capitalize text-extra-small display-inline-block"><i
                                            class="fa fa-user text-blue margin-5px-right"></i><a href="#."
                                            class="text-medium-gray"> Admin</a>&nbsp;&nbsp;|&nbsp;&nbsp;20 Feb
                                        2018</span>
                                </div>
                                <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum standard dummy...</p>
                            </div>
                        </div>
                        <!-- end blog item -->
                    </div>
                    <!-- end blog post item -->
                    <!-- start blog post item -->
                    <div class="item margin-10px-lr grid-item last-paragraph-no-margin margin-20px-bottom">
                        <div class="blog-post bg-white">
                            <div class="blog-post-images overflow-hidden">
                                <a href="#.">
                                    <img src="{{ asset('main/img/blog-img6.jpg') }}" alt="blog">
                                </a>
                                <div
                                    class="blog-categories bg-white text-capitalize bg-blue text-extra-small alt-font text-white">
                                    Finza News
                                </div>
                            </div>
                            <div
                                class="post-details inner-match-height padding-40px-all sm-padding-20px-all xs-padding-30px-tb">
                                <div class="blog-hover-color"></div>
                                <a href="#."
                                    class="alt-font post-title text-medium text-extra-dark-gray display-block md-width-100 margin-5px-bottom">How
                                    Does Company Help To Secure Finza.</a>
                                <div class="author">
                                    <span
                                        class="text-medium-gray text-capitalize text-extra-small display-inline-block"><i
                                            class="fa fa-user text-blue margin-5px-right"></i><a href="#."
                                            class="text-medium-gray"> Admin</a>&nbsp;&nbsp;|&nbsp;&nbsp;20 Feb
                                        2018</span>
                                </div>
                                <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum standard dummy...</p>
                            </div>
                        </div>
                    </div>
                    <!-- end blog post item -->
                    <!-- start blog post item -->
                    <div class="item margin-10px-lr grid-item last-paragraph-no-margin margin-20px-bottom">
                        <!-- start blog item -->
                        <div class="blog-post bg-white">
                            <div class="blog-post-images overflow-hidden">
                                <a href="#.">
                                    <img src="{{ asset('main/img/blog-img4.jpg') }}" alt="blog">
                                </a>
                                <div
                                    class="blog-categories bg-white text-capitalize bg-green text-extra-small alt-font text-white">
                                    Finza News
                                </div>
                            </div>
                            <div
                                class="post-details inner-match-height padding-40px-all sm-padding-20px-all xs-padding-30px-tb">
                                <div class="blog-hover-color"></div>
                                <a href="#."
                                    class="alt-font post-title text-medium text-extra-dark-gray display-block md-width-100 margin-5px-bottom">How
                                    Difficult Is It To Make Payment Finza</a>
                                <div class="author">
                                    <span
                                        class="text-medium-gray text-capitalize text-extra-small display-inline-block"><i
                                            class="fa fa-user text-blue margin-5px-right"></i><a href="#."
                                            class="text-medium-gray"> Admin</a>&nbsp;&nbsp;|&nbsp;&nbsp;20 Feb
                                        2018</span>
                                </div>
                                <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum standard dummy...</p>
                            </div>
                        </div>
                        <!-- end blog item -->
                    </div>
                    <!-- end blog post item -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog end -->
