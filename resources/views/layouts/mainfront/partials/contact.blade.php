<!-- contact-->
<section id="contact" class="btn-version">
    <div class="get-quote-section xs-text-center">
        <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
        <div class="container">
            <div class="row clearfix">
                <!--Form Column-->
                <div class="col-sm-6 wow fadeInLeft" data-wow-duration=".5s">
                    <div class="sec-title margin-50px-bottom">
                        <h5
                            class="text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                            Lets Get In Touch</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum
                            illum ratione atque praesentium
                            laudantium quaerat laborum consecteturLorem ipsum dolor sit amet, consectetur
                            adipisicing
                            elit.</p>
                    </div>
                    <div class="row margin-15px-bottom">
                        <div class="col-sm-1 no-padding">
                            <div class="contact-icon text-blue">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <p class="text-small">San Francisco, CA 560 Bush St & 20th Ave,<br> Apt 5 San Francisco,
                                230909</p>
                        </div>
                    </div>
                    <div class="row margin-15px-bottom">
                        <div class="col-sm-1 no-padding">
                            <div class="contact-icon text-green">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <p class="text-small">+01 2323 7328 322<br>+01 2323 7328 322</p>
                        </div>
                    </div>
                    <div class="row margin-15px-bottom">
                        <div class="col-sm-1 no-padding">
                            <div class="contact-icon text-blue">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-sm-11 xs-margin-50px-bottom">
                            <p class="text-small">email@demo.com<br>www.demo.com</p>
                        </div>
                    </div>
                </div>
                <div class="form-column col-sm-6 wow fadeInRight" data-wow-duration=".5s">
                    <div class="contact-form">
                        <!--Title-->
                        <form class="form_class">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form_inputs" name="name" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form_inputs" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form_inputs" name="phone no" placeholder="Phone">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="form-group">
                                    <textarea name="message" id="message" class="form_inputs form_inputs_two" rows="6"
                                        cols="25" placeholder="Type Your Message"></textarea>
                                </div>
                                <div class="button_div">
                                    <a href="#." class="btn btn-blue btn-large text-extra-small width-100">Get
                                        Started</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- contact end -->
