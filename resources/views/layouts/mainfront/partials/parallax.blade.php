<!-- parallax -->
<section class="parallax bg-3">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container position-relative">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
                <h5
                    class="text-capitalize alt-font text-white margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                    Helping Small Business</h5>
                <p class="width-75 margin-lr-auto text-very-light-gray md-width-90 xs-width-100 xs-margin-30px-bottom">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo
                    tristique, tempor sagittis dolor.
                </p>
                <a href="#." class="btn btn-green btn-large text-extra-small margin-30px-top xs-no-margin-top">Get
                    Support</a>
            </div>
        </div>
    </div>
</section>
<!-- parallax -->
