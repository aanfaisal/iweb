<!--footer Start-->
<footer class="bg-extra-dark-gray padding-30px-tb text-center">
    <div class="footer-widget-area">
        <div class="container">
            <div class="row">
                <div class="medium-icon social-icon-style-3 social_icon list-inline margin-20px-top">
                    <a class="facebook text-white facebook-bg-hvr wow fadeInUp" data-wow-duration=".5s" href="#."><i
                            class="fa fa-facebook" aria-hidden="true"></i><span></span></a>
                    <a class="twitter text-white twitter-bg-hvr wow fadeInDown" data-wow-duration=".5s" href="#."><i
                            class="fa fa-twitter" aria-hidden="true"></i><span></span></a>
                    <a class="pinterest text-white pinterest-bg-hvr wow fadeInUp" data-wow-duration=".5s" href="#."><i
                            class="fa fa-pinterest-p" aria-hidden="true"></i><span></span></a>
                    <a class="google text-white google-bg-hvr wow fadeInDown" data-wow-duration=".5s" href="#."><i
                            class="fa fa-google" aria-hidden="true"></i><span></span></a>
                </div>
            </div>
            <p class="text-white text-large margin-10px-bottom margin-20px-top"></p>
            <p class="text-light-gray margin-10px-bottom">Made with <i class="fa fa-heart"></i> © IWEB</p>
        </div>
    </div>

</footer>
<!-- end footer -->
