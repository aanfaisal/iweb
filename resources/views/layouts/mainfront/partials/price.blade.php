<!-- Price -->
<section id="price" class="text-center btn-version bg-light-gray">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
            <div class="sec-title margin-70px-bottom">
                <h5
                    class="text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                    Our Pricing</h5>
                <p class="width-75 margin-lr-auto md-width-90 xs-width-100 xs-margin-30px-bottom">Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                    laborum. Perspiciatis unde omnis iste natus error sit.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 xs-margin-30px-bottom">
                <div class="price-table padding-30px-tb wow fadeInLeft" data-wow-duration=".5s">
                    <div class="text-large margin-30px-bottom">Premium</div>
                    <div class="price margin-30px-bottom">
                        <span class="currency text-lg-gray">$</span>
                        <span class="value text-blue">49</span>
                        <span class="duration text-lg-gray">/month</span>
                    </div>
                    <p class="no-margin-bottom">Billed Annually or<br>
                        $12.99 Month-to-Month</p>
                    <a href="#." class="btn btn-transparent-blue btn-large text-extra-small margin-20px-tb width-80">Buy
                        Now</a>
                    <p class="margin-10px-bottom">5 Credit Reports</p>
                    <p class="margin-10px-bottom">100 Accounts Download</p>
                    <p class="margin-10px-bottom">100 Accounts Table Exports</p>
                </div>
            </div>
            <div class="col-sm-4 xs-margin-30px-bottom">
                <div class="price-table padding-30px-tb wow fadeInUp" data-wow-duration=".5s">
                    <div class="text-large margin-30px-bottom">Professional</div>
                    <div class="price margin-30px-bottom">
                        <span class="currency text-lg-gray">$</span>
                        <span class="value text-green">99</span>
                        <span class="duration text-lg-gray">/month</span>
                    </div>
                    <p class="no-margin-bottom">Billed Annually or<br>
                        $12.99 Month-to-Month</p>
                    <a href="#."
                        class="btn btn-transparent-green btn-large text-extra-small margin-20px-tb width-80">Buy
                        Now</a>
                    <p class="margin-10px-bottom">5 Credit Reports</p>
                    <p class="margin-10px-bottom">100 Accounts Download</p>
                    <p class="margin-10px-bottom">100 Accounts Table Exports</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="price-table padding-30px-tb wow fadeInRight" data-wow-duration=".5s">
                    <div class="text-large margin-30px-bottom">Extreme</div>
                    <div class="price margin-30px-bottom">
                        <span class="currency text-lg-gray">$</span>
                        <span class="value text-blue">149</span>
                        <span class="duration text-lg-gray">/month</span>
                    </div>
                    <p class="no-margin-bottom">Billed Annually or<br>
                        $12.99 Month-to-Month</p>
                    <a href="#." class="btn btn-transparent-blue btn-large text-extra-small margin-20px-tb width-80">Buy
                        Now</a>
                    <p class="margin-10px-bottom">5 Credit Reports</p>
                    <p class="margin-10px-bottom">100 Accounts Download</p>
                    <p class="margin-10px-bottom">100 Accounts Table Exports</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- price end -->
