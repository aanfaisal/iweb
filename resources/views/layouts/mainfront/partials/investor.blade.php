<!-- investor -->
<section id="investor" class="iq-news text-center">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
                <div class="area-heading text-center margin-50px-bottom">
                    <h5
                        class="alt-font text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                        Our Investors</h5>
                    <p class="width-75 margin-lr-auto md-width-90 xs-width-100 xs-margin-30px-bottom">Maecenas ut
                        lectus
                        a ipsum gravida ornare. Cras scelerisque in mauris id pretium.
                        Praesent convallis neque quis erat lobortis gravida id et dolor.</p>
                </div>
            </div>
        </div>
        <div class="row iq-team2">
            <div class="owl-team owl-carousel owl-theme">
                <div class="team-blog padding-20px-bottom text-center item">
                    <div class="iq-image">
                        <img alt="investor" class="img-fluid text-center" src="{{ asset('main/img/team-1.jpg') }}">
                    </div>
                    <div class="iq-font-blue iq-mt-20">
                        <div class="text-large text-extra-dark-gray margin-20px-top">Cris Martin</div>
                        <div class="text-medium text-blue margin-20px-bottom">Investor, Finza</div>
                        <p class="">Simply dummy text ever sincehar the 1500s, when an unknownshil printer took.</p>
                        <ul class="iq-media-blog margin-5px-top">
                            <li><a href="#." class="facebook-bg-hvr"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="#." class="google-bg-hvr"><i class="fa fa-google"></i></a></li>
                            <li><a href="#." class="twitter-bg-hvr"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="team-blog padding-20px-bottom text-center item">
                    <div class="iq-image">
                        <img alt="investor" class="img-fluid text-center" src="{{ asset('main/img/team-3.jpg') }}">
                    </div>
                    <div class="iq-font-blue iq-mt-20">
                        <div class="text-large text-extra-dark-gray margin-20px-top">Elyse Perry</div>
                        <div class="text-medium text-green margin-20px-bottom">Investor, Finza</div>
                        <p class="">Simply dummy text ever sincehar the 1500s, when an unknownshil printer took.</p>
                        <ul class="iq-media-blog margin-5px-top">
                            <li><a href="#." class="facebook-bg-hvr"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="#." class="twitter-bg-hvr"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#." class="pinterest-bg-hvr"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="team-blog padding-20px-bottom text-center item">
                    <div class="iq-image">
                        <img alt="investor" class="img-fluid text-center" src="{{ asset('main/img/team-4.jpg') }}">
                    </div>
                    <div class="iq-font-blue iq-mt-20">
                        <div class="text-large text-extra-dark-gray margin-20px-top">Danny Mock</div>
                        <div class="text-medium text-blue margin-20px-bottom">Investor, Finza</div>
                        <p class="">Simply dummy text ever sincehar the 1500s, when an unknownshil printer took.</p>
                        <ul class="iq-media-blog margin-5px-top">
                            <li><a href="#." class="facebook-bg-hvr"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="#." class="linkedin-bg-hvr"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#." class="twitter-bg-hvr"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="team-blog padding-20px-bottom text-center item">
                    <div class="iq-image">
                        <img alt="investor" class="img-fluid text-center" src="{{ asset('main/img/team-2.jpg') }}">
                    </div>
                    <div class="iq-font-blue iq-mt-20">
                        <div class="text-large text-extra-dark-gray margin-20px-top">Sarah Taylor</div>
                        <div class="text-medium text-green margin-20px-bottom">Investor, Finza</div>
                        <p class="">Simply dummy text ever sincehar the 1500s, when an unknownshil printer took.</p>
                        <ul class="iq-media-blog margin-5px-top">
                            <li><a href="#." class="facebook-bg-hvr"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="#." class="pinterest-bg-hvr"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#." class="google-bg-hvr"><i class="fa fa-google"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- investor end -->
