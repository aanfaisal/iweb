<!-- counter -->
<section class="text-center bg-grd-blue">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="row">
            <!-- start counter item -->
            <div class="col-md-3 col-sm-6 col-xs-12 sm-margin-ten-bottom xs-margin-50px-bottom">
                <div class="feature-box-5 position-relative">
                    <div class="count-icon-two"><i class="fa fa-globe" aria-hidden="true"></i></div>
                    <div class="feature-content">
                        <h6 class="display-block text-white margin-5px-bottom font-weight-600 timer appear"
                            data-speed="10000" data-to="375">375</h6>
                        <span class="text-medium text-white text-capitalize position-relative">Support
                            Countries</span>
                    </div>
                </div>
            </div>
            <!-- end counter item -->
            <!-- start counter item -->
            <div class="col-md-3 col-sm-6 col-xs-12 sm-margin-ten-bottom xs-margin-50px-bottom">
                <div class="feature-box-5 position-relative">
                    <div class="count-icon-two"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <div class="feature-content">
                        <h6 class="display-block text-white margin-5px-bottom font-weight-600 timer appear"
                            data-speed="10000" data-to="735">735</h6>
                        <span class="text-medium text-white text-capitalize position-relative">Operators</span>
                    </div>
                </div>
            </div>
            <!-- end counter item -->
            <!-- start counter item -->
            <div class="col-md-3 col-sm-6 col-xs-12 xs-margin-50px-bottom">
                <div class="feature-box-5 position-relative">
                    <div class="count-icon-two"><i class="fa fa-comments-o" aria-hidden="true"></i></div>
                    <div class="feature-content">
                        <h6 class="display-block text-white margin-5px-bottom font-weight-600 timer appear"
                            data-speed="10000" data-to="485">485</h6>
                        <span class="text-medium text-white text-capitalize position-relative">Producers</span>
                    </div>
                </div>
            </div>
            <!-- end counter item -->
            <!-- start counter item -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="feature-box-5 position-relative">
                    <div class="count-icon-two"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
                    <div class="feature-content">
                        <h6 class="display-block text-white margin-5px-bottom font-weight-600 timer appear"
                            data-speed="10000" data-to="780">780</h6>
                        <span class="text-medium text-white text-capitalize position-relative">Finza ATMs</span>
                    </div>
                </div>
            </div>
            <!-- end counter item -->
        </div>
    </div>
</section>
<!-- counter end -->
