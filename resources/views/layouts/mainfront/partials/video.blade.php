<!-- parallax video-->
<section class="gradient-background xs-text-center bg-grd-blue">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="row">
            <div class="col-sm-7 col-md-6 xs-margin-30px-bottom padding-40px-right xs-padding-10px-lr">
                <div class="text-medium text-white">What is Finza</div>
                <h5
                    class="section_header text-capitalize alt-font text-white margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                    Introductory Video </h5>
                <p class="text-white">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                    nonumy eirmod tempor
                    invidunt ut laboret dolore magna aliquyam erat, sed diam voluptua laboret dolore magna.</p>
                <a href="#." class="btn btn-transparent-white btn-large text-extra-small margin-20px-top">Buy
                    Now</a>
            </div>
            <div class="col-sm-5 col-md-6">
                <div class="video-img">
                    <img src="{{ asset('main/img/3.jpg') }}" alt="video">
                    <a href="https://www.youtube.com/watch?v=uVju5--RqtY" data-lity>
                        <div class="play-hvr">
                            <div class="play-icon">
                                <i class="fa fa-play"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- parallax end -->
