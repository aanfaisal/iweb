<!-- start header -->
<header>
    <!-- start navigation -->
    <nav
        class="navbar navbar-default bootsnav navbar-fixed-top header-light bg-transparent nav-box-width white-link on no-full">
        <div class="container nav-header-container">
            <div class="row">
                <!-- start logo -->
                <div class="col-md-2 col-xs-5">
                    <a href="{{ route('homepage.index') }}" title="Logo" class="logo">
                        <img src="{{ asset('img/logo-blue.png') }}" class="logo-dark" alt="@yield('title')">
                        <img src="{{ asset('img/logo.png') }}" alt="@yield('title')" class="logo-light default"></a>
                </div>
                <!-- end logo -->
                <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu">
                    <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse"
                        data-target="#navbar-collapse-toggle-1">
                        <span class="sr-only">toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                        <ul class="nav navbar-nav navbar-left panel-group no-margin alt-font font-weight-800">
                            <li class="{{ (strpos(Route::currentRouteName(), 'home.index') === 0) ? 'active' : '' }}">
                                <a href="{{ route('homepage.index') }}">Home</a>
                            </li>
                            <li
                                class="{{ (strpos(Route::currentRouteName(), 'tentang.index') === 0) ? 'active' : '' }}">
                                <a href=" {{ route('tentang.index') }}">About Us</a>
                            </li>

                            <li
                                class="{{ (strpos(Route::currentRouteName(), 'webstore.index') === 0) ? 'active' : '' }}">
                                <a href=" {{ route('webstore.index') }}">Web Store</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- end navigation -->
</header>
<!-- end header -->
