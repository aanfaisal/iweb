<!-- animation -->
<link rel="stylesheet" href="{{ asset('main/css/animate.css') }}" />
<!-- bootstrap -->
<link rel="stylesheet" href="{{ asset('main/css/bootstrap.min.css') }}" />
<!-- font-awesome icon -->
<link rel="stylesheet" href="{{ asset('main/css/font-awesome.css') }}" />
<link rel="stylesheet" href="{{ asset('main/css/font-awesome.min.css') }}" />
<!-- magnific popup -->
<link rel="stylesheet" href="{{ asset('main/css/magnific-popup.css') }}" />
<!-- cube Portfolio -->
<link rel="stylesheet" href="{{ asset('main/css/cubeportfolio.min.css') }}" />
<!-- cube Portfolio -->
<link rel="stylesheet" href="{{ asset('main/css/lity.min.css') }}" />
<!-- owl carousel -->
<link href="{{ asset('main/css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('main/css/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<!-- bootsnav -->
<link rel="stylesheet" href="{{ asset('main/css/bootsnav.css') }}">
<!-- bundle css -->
<link rel="stylesheet" href="{{ asset('main/css/packed.css') }}" />
<!-- style -->
<link rel="stylesheet" href="{{ asset('main/css/style.css') }}" />
<!-- Custom Style -->
<link rel="stylesheet" href="{{ asset('main/css/custom.css') }}" />

@yield('css')
