<!-- about -->
<section class="white-bg xboot-section xs-text-center btn-version" id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-7 wow fadeInLeft" data-wow-duration=".5s">
                <div class="owl-xbox owl-carousel owl-theme">
                    <div class="intro-image item">
                        <img src="{{ asset('main/img/xbox.png') }}" alt="xbox" class="img-responsive">
                    </div>
                    <div class="intro-image item">
                        <img src="{{ asset('main/img/xbox.png') }}" alt="xbox" class="img-responsive">
                    </div>
                    <div class="intro-image item">
                        <img src="{{ asset('main/img/xbox.png') }}" alt="xbox" class="img-responsive">
                    </div>
                </div>
            </div>
            <div class="col-sm-5 wow fadeInRight" data-wow-duration=".5s">
                <div class="intro-content">
                    <div class="intro-heading">
                        <div class="text-medium xs-margin-20px-top">What is Finza ?</div>
                        <h5
                            class="text-blue text-capitalize alt-font margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                            You Need To Know</h5>
                    </div>
                    <div class="intro-description">
                        <p>Maecenas ut lectus a ipsum gravida ornare. Cras scelerisque in mauris id pretium.
                            Praesent
                            convallis neque quis erat lobortis gravida id et dolor sagittis vel
                            convallis quis.</p>
                        <p>Curabitur egestas ipsum ut iaculis sodales. Nunc tincidunt molestie sapien, ut mattis
                            lectus
                            aliquet non. id faucibus.</p>
                        <a class="btn btn-transparent-blue btn-large xs-margin-two-all margin-20px-top" href="#.">Learn
                            More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about end -->

<!-- parallax -->
<section class="parallax bg-1">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container position-relative">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
                <h5
                    class="text-capitalize alt-font text-white margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                    Friendly Support</h5>
                <p class="width-75 margin-lr-auto text-very-light-gray md-width-90 xs-width-100 xs-margin-30px-bottom">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo
                    tristique, tempor sagittis dolor.
                </p>
                <a href="#." class="btn btn-green btn-large text-extra-small margin-30px-top xs-no-margin-top">Get
                    Support</a>
            </div>
        </div>
    </div>
</section>
<!-- parallax -->


<!-- feature one -->
<section class="our-services" id="services">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
                <div class="area-heading text-center margin-70px-bottom">
                    <h5
                        class="area-title text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                        Why Choose Finza</h5>
                    <p class="width-75 margin-lr-auto md-width-90 xs-width-100 xs-margin-30px-bottom">Maecenas ut
                        lectus
                        a ipsum gravida ornare. Cras scelerisque in mauris id pretium.
                        Praesent convallis neque quis erat lobortis gravida id et dolor.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="single-feature margin-lr-auto margin-50px-bottom wow fadeInLeft" data-wow-duration=".5s">
                    <div class="feature-icon text-green">
                        <i class="fa fa-bar-chart" aria-hidden="true"></i>
                    </div>
                    <div class="feature-content">
                        <div class="text-large text-extra-dark-gray margin-20px-bottom hvr-green">Live Analyze</div>
                        <p>Lorem ipsum dolor sit amet, adipisicing elit. Impedit consequuntur expedita</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-feature margin-lr-auto margin-50px-bottom wow fadeInUp" data-wow-duration=".5s">
                    <div class="feature-icon text-blue">
                        <i class="fa fa-diamond" aria-hidden="true"></i>
                    </div>
                    <div class="feature-content">
                        <div class="text-large text-extra-dark-gray margin-20px-bottom hvr-blue">Online Marketing
                        </div>
                        <p>Lorem ipsum dolor sit amet, adipisicing elit. Impedit consequuntur expedita</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-feature margin-lr-auto margin-50px-bottom wow fadeInRight" data-wow-duration=".5s">
                    <div class="feature-icon text-green">
                        <i class="fa fa-pie-chart" aria-hidden="true"></i>
                    </div>
                    <div class="feature-content">
                        <div class="text-large text-extra-dark-gray margin-20px-bottom hvr-green">Smart Plaining
                        </div>
                        <p>Lorem ipsum dolor sit amet, adipisicing elit. Impedit consequuntur expedita</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-feature margin-lr-auto md-margin-50px-bottom xs-margin-50px-bottom wow fadeInLeft"
                    data-wow-duration=".5s">
                    <div class="feature-icon text-blue">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                    </div>
                    <div class="feature-content margin-lr-auto xs-margin-30px-bottom">
                        <div class="text-large text-extra-dark-gray margin-20px-bottom hvr-blue">Secure Web</div>
                        <p>Lorem ipsum dolor sit amet, adipisicing elit. Impedit consequuntur expedita</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-feature margin-lr-auto xs-margin-50px-bottom  wow fadeInUp" data-wow-duration=".5s">
                    <div class="feature-icon text-green">
                        <i class="fa fa-institution" aria-hidden="true"></i>
                    </div>
                    <div class="feature-content">
                        <div class="text-large text-extra-dark-gray margin-20px-bottom hvr-green">Best Idea</div>
                        <p>Lorem ipsum dolor sit amet, adipisicing elit. Impedit consequuntur expedita</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="single-feature margin-lr-auto wow fadeInRight" data-wow-duration=".5s">
                    <div class="feature-icon text-blue">
                        <i class="fa fa-send" aria-hidden="true"></i>
                    </div>
                    <div class="feature-content">
                        <div class="text-large text-extra-dark-gray margin-20px-bottom hvr-blue">Instant Exchange
                        </div>
                        <p>Lorem ipsum dolor sit amet, adipisicing elit. Impedit consequuntur expedita</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- feature end -->
