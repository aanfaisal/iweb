<!-- map -->
<section class="no-padding">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="row">
        <div class="map-horizontal">
            <div class="map" id="map"></div>
        </div>
    </div>
</section>
<!-- map end -->
