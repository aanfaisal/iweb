<!-- how it works -->
<section class="how-it-work text-center bg-light-gray">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
            <div class="sec-title margin-100px-bottom">
                <h5
                    class="text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                    How it Works</h5>
                <p class="width-75 margin-lr-auto md-width-90 xs-width-100 xs-margin-30px-bottom">Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                    laborum. Perspiciatis unde omnis iste natus error sit.</p>
            </div>
        </div>
        <div class="how-one-container">
            <!--how it work Box-->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how-box-one inner-box xs-margin-100px-bottom wow fadeInLeft" data-wow-duration=".5s">
                    <div class="icon-box-blue">
                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                    </div>
                    <div class="text-large text-extra-dark-gray margin-20px-bottom">Smart Plaining</div>
                    <p>Capitalise on low hanging fruit to identify a ballpark value added activity.
                    </p>
                </div>
            </div>

            <!--how it work Box-->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="how-box-one inner-box sm-margin-100px-bottom xs-margin-100px-bottom wow fadeInUp"
                    data-wow-duration=".5s">
                    <div class="icon-box-green">
                        <i class="fa fa-group" aria-hidden="true"></i>
                    </div>
                    <div class="text-large text-extra-dark-gray margin-20px-bottom">Business Consulting</div>
                    <p>Capitalise on low hanging fruit to identify a ballpark value added activity.
                    </p>
                </div>
            </div>

            <!--how it work Box-->
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="how-box-one inner-box wow fadeInRight" data-wow-duration=".5s">
                    <div class="icon-box-blue">
                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                    </div>
                    <div class="text-large text-extra-dark-gray margin-20px-bottom">Appointment</div>
                    <p>Capitalise on low hanging fruit to identify a ballpark value added activity.
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- how its work end -->
