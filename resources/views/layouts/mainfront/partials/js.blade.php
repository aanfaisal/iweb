<!-- javascript libraries -->
<script src="{{ asset('main/js/jquery.js') }}"></script>
<script src="{{ asset('main/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('main/js/jquery.appear.js') }}"></script>

<!-- owl carousel -->
<script src="{{ asset('main/js/owl.carousel.min.js') }}"></script>
<!-- counter -->
<script src="{{ asset('main/js/jquery.count-to.js') }}"></script>
<!-- magnific popup -->
<script src="{{ asset('main/js/jquery.magnific-popup.min.js') }}"></script>
<!-- cube portfolio -->
<script src="{{ asset('main/js/jquery.cubeportfolio.min.js') }}"></script>
<!-- fancybox -->
<script src="{{ asset('main/js/lity.min.js') }}"></script>
<!-- wow -->
<script src="{{ asset('main/js/wow.min.js') }}"></script>
<!-- setting -->
<script src="{{ asset('main/js/main.js') }}"></script>

@yield('js')
