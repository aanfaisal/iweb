<!-- testimonial -->
<section class="testmonial bg-light-gray xs-text-center">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">IWEB</h2>
    <div class="container">
        <div class="row equalize sm-equalize-auto">
            <div class="col-sm-6 col-xs-12">
                <div class="testimonial-column item wow fadeInLeft" data-wow-duration=".5s">
                    <div class="testimonial-title">
                        <div class="text-medium text-blue">Testimonial</div>
                        <h5
                            class="text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                        </h5>
                        <div class="back-quote"><i class="fa fa-quote-right"></i></div>
                    </div>
                    <div class="owl-testimonial owl-carousel owl-theme">
                        <div class="item">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam earum, provident ad,
                                porro
                                aperiam dolore, blanditiis, nihil pariatur eius adipisci consequuntur officiis.</p>
                            <div class="text-medium margin-10px-top">
                                Andra
                            </div>
                            <p class="text-medium text-blue">Owner He</p>
                        </div>
                        <div class="item">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam earum, provident ad,
                                porro
                                aperiam dolore, blanditiis, nihil pariatur eius adipisci consequuntur officiis.</p>
                            <div class="text-medium margin-10px-top">
                                Hendra
                            </div>
                            <p class="text-medium text-blue">Owner Ho</p>
                        </div>
                        <div class="item">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam earum, provident ad,
                                porro
                                aperiam dolore, blanditiis, nihil pariatur eius adipisci consequuntur officiis.</p>
                            <div class="text-medium margin-10px-top">
                                Luodra
                            </div>
                            <p class="text-medium text-blue">Owner Hi</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-xs-12 hidden-xs" aria-hidden="true">
                <div class="woa-image wow fadeInUp" data-wow-duration=".5s">
                    <img src="{{ asset('main/img/wao-img.png') }}" alt="woa">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- testimonial end -->
