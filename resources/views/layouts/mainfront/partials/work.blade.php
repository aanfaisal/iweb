<!-- Recent  Work -->
<section id="work" class="recent-work xs-text-center btn-version no-padding-bottom no-transition">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">Finza</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
                <div class="area-heading text-center margin-20px-bottom">
                    <h5
                        class="alt-font text-capitalize alt-font text-extra-dark-gray margin-20px-bottom font-weight-500 sm-width-100 xs-width-100">
                        Latest Events</h5>
                </div>
            </div>
        </div>
    </div>
    <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter margin-70px-bottom">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
            All
            <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".print" class="cbp-filter-item">
            Print
            <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".web-design" class="cbp-filter-item">
            Web Design
            <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".graphic" class="cbp-filter-item">
            Graphic
            <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".motion" class="cbp-filter-item">
            Motion
            <div class="cbp-filter-counter"></div>
        </div>
    </div>
    <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">
        <div class="cbp-item web-design graphic">
            <a href="{{ asset('main/img/project-s1.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="Bolt UI<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-s1.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Bolt UI</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print motion">
            <a href="{{ asset('main/img/project-s2.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="World Clock<br>by Paul Flavius Nechita">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-s2.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">World Clock</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print motion">
            <a href="{{ asset('main/img/project-l2.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="WhereTO App<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-l2.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">WhereTO App</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item motion graphic">
            <a href="{{ asset('main/img/project-m1.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="Seemple* Music<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-m1.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Seemple Music</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item web-design print">
            <a href="{{ asset('main/img/project-l1.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="iDevices<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-l1.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">iDevices</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print motion">
            <a href="{{ asset('main/img/project-m2.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="Remind~Me Widget<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-m2.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Remind~Me Widget</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item web-design print">
            <a href="{{ asset('main/img/project-s3.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="Workout Buddy<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-s3.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Workout Buddy</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print graphic">
            <a href="{{ asset('main/img/project-s4.jpg') }}" class="cbp-caption cbp-lightbox"
                data-title="Digital Menu<br>by Cosmin Capitanu">
                <div class="cbp-caption-defaultWrap">
                    <img src="img/project-s4.jpg" alt="project">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Digital Menu</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</section>
<!-- Reacent End -->
