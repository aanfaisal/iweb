<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <meta name="author" content="aanfaisal">
    <!-- description -->
    <meta name="description" content="@yield('description')">
    <!-- keywords -->
    <meta name="keywords" content="@yield('keywords')">
    <!-- title -->
    <title>@yield('title')</title>
    <!-- favicon -->
    <link rel="icon" href="{{ asset('img/favicon/favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">

    @include('layouts.mainfront.partials.css')

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="90" class="particles_special_id blue-version">

    <!-- start loader -->
    <div class="preloader">
        <div class="loader">
            <div class="loader-outter"></div>
            <div class="loader-inner"></div>

            <div class="indicator">
                <svg width="16px" height="12px">
                    <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                </svg>
            </div>
        </div>
    </div>
    <!-- end loader -->

    @include('layouts.mainfront.partials.menu')

    @yield('content')

    @include('layouts.mainfront.partials.footer')

    <!-- start scroll to top -->
    <a class="scroll-top-arrow" href="javascript:void(0);"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top  -->

    @include('layouts.mainfront.partials.js')
</body>

</html>
