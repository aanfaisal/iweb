<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>@yield('title') | IWEB </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="{{ asset('img/favicon/favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">

    <!-- Fonts and icons -->
    <script src="{{ asset('lantis/js/plugin/webfont/webfont.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {"families":["Lato:300,400,700,900"]},
            custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('lantis/css/fonts.min.css') }}']},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('lantis/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lantis/css/atlantis.css') }}">

    @yield('css')
</head>

<body class="login">

    @include('sweetalert::alert')

    <div class="wrapper wrapper-login wrapper-login-full p-0">

        <div class="login-aside w-50 d-flex align-items-center justify-content-center bg-white">

            @yield('content')

        </div>
        <div
            class="login-aside w-50 d-flex flex-column align-items-center justify-content-center text-center bg-primary-gradient">
            <h1 class="title fw-bold text-white mb-3">
                <img src="{{ asset('img/logo.png') }}" style="height: 100px;" alt="brand IWEB"
                    class="rounded mx-auto d-block">
            </h1>

        </div>
    </div>
    <script src="{{ asset('lantis/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('lantis/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('lantis/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('lantis/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('lantis/js/atlantis.min.js') }}"></script>


    @yield('js')
</body>

</html>
