<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>@yield('title') | IWEB</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <meta name="keywords" content="Website Promosi, Website Mudah, Cepat" />
    <meta name="description" content="App IWEB Indonesia">

    <!-- Control the behavior of search engine crawling and indexing -->
    <meta name="robots" content="index,follow"><!-- All Search Engines -->
    <meta name="googlebot" content="index,follow"><!-- Google Specific -->


    <link rel="icon" href="{{ asset('img/favicon/favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.main.partials.css')

</head>

<body data-background-color="bg3">

    <div class="wrapper">

        @include('sweetalert::alert')

        <div class="main-panel">
            <div class="container">

                @yield('content')

            </div>

        </div>

    </div>

    @include('layouts.main.partials.js')

</body>

</html>
