<!--   Core JS Files   -->
<script src="{{ asset('lantis/js/core/jquery.3.2.1.min.js') }}"></script>
<script src="{{ asset('lantis/js/core/popper.min.js') }}"></script>
<script src="{{ asset('lantis/js/core/bootstrap.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('lantis/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
<script src="{{ asset('lantis/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

<!-- jQuery Scrollbar -->
<script src="{{ asset('lantis/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

<!-- Chart JS -->
<script src="{{ asset('lantis/js/plugin/chart.js/chart.min.js') }}"></script>

<!-- jQuery Sparkline -->
<script src="{{ asset('lantis/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Chart Circle -->
<script src="{{ asset('lantis/js/plugin/chart-circle/circles.min.js') }}"></script>

<!-- Bootstrap Notify -->
<script src="{{ asset('lantis/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<!-- jQuery Vector Maps -->
<script src="{{ asset('lantis/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('lantis/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script>

<!-- Sweet Alert -->
<script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>

<!-- Atlantis JS -->
<script src="{{ asset('lantis/js/atlantis.min.js') }}"></script>

<!-- Page JS -->
@yield('js')
