<!-- Navbar Header -->
<nav class="navbar navbar-header navbar-header-transparent navbar-expand-lg" data-background-color="white">

    <div class="container-fluid">

        <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
            <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item toggle-nav-search hidden-caret">
                    <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false"
                        aria-controls="search-nav">
                        <i class="fa fa-search"></i>
                    </a>
                </li>

                <li class="nav-item dropdown hidden-caret">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                        <div class="avatar-sm">
                            <img src="@if(is_null(auth()->user()->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.auth()->user()->image) }}@endif"
                                alt="{{ auth()->user()->name }}" class="avatar-img rounded-circle">
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-user animated fadeIn">
                        <div class="scroll-wrapper dropdown-user-scroll scrollbar-outer" style="position: relative;">
                            <div class="dropdown-user-scroll scrollbar-outer scroll-content"
                                style="height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 0px;">
                                <li>
                                    <div class="user-box">
                                        <div class="avatar-lg">
                                            <img src="@if(is_null(auth()->user()->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.auth()->user()->image) }}@endif"
                                                alt="{{ auth()->user()->name }}" class="avatar-img rounded">
                                        </div>
                                        <div class="u-text">
                                            <h4>{{ auth()->user()->name }}</h4>
                                            <p class="text-muted">{{ auth()->user()->email }}</p>
                                            <a href="{{ route('user.profil', auth()->user()->id) }}" class=" btn btn-xs
                                                btn-secondary btn-sm">
                                                Profile</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                        <i class="fas fa-power-off"></i>&nbsp; Logout
                                    </a>

                                </li>
                            </div>
                            <div class="scroll-element scroll-x" style="">
                                <div class="scroll-element_outer">
                                    <div class="scroll-element_size"></div>
                                    <div class="scroll-element_track"></div>
                                    <div class="scroll-bar ui-draggable ui-draggable-handle"></div>
                                </div>
                            </div>
                            <div class="scroll-element scroll-y" style="">
                                <div class="scroll-element_outer">
                                    <div class="scroll-element_size"></div>
                                    <div class="scroll-element_track"></div>
                                    <div class="scroll-bar ui-draggable ui-draggable-handle"></div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </li>
            </ul>

    </div>
</nav>

<!-- End Navbar -->
