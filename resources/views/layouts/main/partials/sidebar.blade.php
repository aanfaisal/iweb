<!-- Sidebar -->
<div class="sidebar sidebar-style-2" data-background-color="blue">
    <div class="sidebar-wrapper scrollbar scrollbar-inner" style="position: relative;">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="@if(is_null(auth()->user()->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.auth()->user()->image) }}@endif"
                        alt="{{ auth()->user()->name }}" class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#logoutButton" aria-expanded="true">
                        <span>
                            {{ auth()->user()->name }}
                            <span class="user-level">{{ auth()->user()->roles->first()->name }}</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="in collapse" id="logoutButton" style="">
                        <ul class="nav">
                            <li>
                                <a href="{{ route('logout') }}" class="collapsed"
                                    onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                    <span class="link"> Logout</span>
                                </a>

                                <form id="frm-logout" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

            <ul class="nav nav-primary">
                <li class="nav-item {{ (strpos(Route::currentRouteName(), 'home') === 0) ? 'active' : '' }}">
                    <a href="{{ route('dashboard.index') }}" class="collapsed">
                        <i class="la flaticon-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>


                @role('admin')
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Menu Admin</h4>
                </li>
                <li
                    class="nav-item {{ (strpos(Route::currentRouteName(), 'setting-homepage') === 0) ? 'active' : '' }}">
                    <a href="{{ route('setting-homepage.index') }}">
                        <i class="fas fa-desktop"></i>
                        <p>Home Page</p>
                    </a>
                </li>
                <li class="nav-item {{ (strpos(Route::currentRouteName(), 'setting-biaya') === 0) ? 'active' : '' }}">
                    <a href="{{ route('setting-biaya.index') }}">
                        <i class="fas fa-dollar-sign"></i>
                        <p>Biaya</p>
                    </a>
                </li>
                <li class="nav-item {{ (strpos(Route::currentRouteName(), 'setting-tentang') === 0) ? 'active' : '' }}">
                    <a href="{{ route('setting-tentang.index') }}">
                        <i class="fas fa-industry"></i>
                        <p>Tentang Kami</p>
                    </a>
                </li>

                <li
                    class="nav-item {{ (strpos(Route::currentRouteName(), 'setting-testimoni') === 0) ? 'active' : '' }}">
                    <a href="{{ route('setting-testimoni.index') }}">
                        <i class="fas fa-coffee"></i>
                        <p>Testimoni</p>
                    </a>
                </li>
                @endrole


                @role('admin')
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Menu Settings</h4>
                </li>

                <li class="nav-item {{ (strpos(Route::currentRouteName(), 'user.index') === 0) ? 'active' : '' }}">
                    <a data-toggle="collapse" href="#subsettings" class="collapsed" aria-expanded="false">
                        <i class="fas fa-cogs"></i>
                        <p>Settings</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ (strpos(Route::currentRouteName(), 'user.index') === 0) ? 'active' : '' }}"
                        id="subsettings" style="">
                        <ul class="nav nav-collapse">
                            <li
                                class="nav-item {{ (strpos(Route::currentRouteName(), 'user.index') === 0) ? 'active' : '' }}">
                                <a href="{{ route('user.index') }}">
                                    <i class="fas fa-users"></i>
                                    <p> Data User </p>
                                </a>
                            </li>


                        </ul>
                    </div>
                </li>
                @endrole

            </ul>

        </div>
    </div>
</div>
<!-- End Sidebar -->
