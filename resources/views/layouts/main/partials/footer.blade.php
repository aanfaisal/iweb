<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="https://www.iweb.co.id/">
                        IWEB
                    </a>
                </li>

            </ul>
        </nav>
        <div class="copyright ml-auto">
            © 2022 - {{ \Carbon\Carbon::now()->format('Y') }} <b>IWEB</b> All
            Right Reserved</b>.
        </div>
    </div>
</footer>
