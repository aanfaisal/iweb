<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <meta name="keywords" content="Website Promosi, Website Mudah, Cepat" />
    <meta name="description" content="App IWEB Indonesia">

    <!-- Control the behavior of search engine crawling and indexing -->
    <meta name="robots" content="index,follow"><!-- All Search Engines -->
    <meta name="googlebot" content="index,follow"><!-- Google Specific -->

    @yield('favicon')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.userfront.partials.csshome')
</head>

<body class="animsition">

    @yield('menu-desktop')

    @yield('menu-mobile')

    @yield('slider')

    @yield('banner')

    @yield('footer')

    {{-- @include('layouts.userfront.partials.footer') --}}

    <!-- Back to top -->
    <div class="btn-back-to-top" id="myBtn">
        <span class="symbol-btn-back-to-top">
            <i class="zmdi zmdi-chevron-up"></i>
        </span>
    </div>


    @include('layouts.userfront.partials.jshome')

</body>

</html>
