<!-- Footer -->
@if((new \Jenssegers\Agent\Agent())->isMobile())
<footer class="bg3 p-t-40 p-b-32">
    <div class="container">

        @yield('bannerfooter')

    </div>

    <div class="p-t-40">
        @yield('socialmedia')

        <br>
        <p class="stext-107 cl6 txt-center">
            Copyright &copy;
            <script>
                document.write(new Date().getFullYear());
            </script> All rights reserved <i class="fa fa-heart-o" aria-hidden="true"></i> by <a
                href="https://iweb.co.id" target="_blank"> @yield('copyright')</a>.
        </p>
    </div>
    </div>
</footer>

@elseif((new \Jenssegers\Agent\Agent())->isDesktop())
<footer class="bg3 p-t-75 p-b-32">
    <div class="container">

        @yield('bannerfooter')

    </div>

    <div class="p-t-40">
        @yield('socialmedia')

        <br>
        <p class="stext-107 cl6 txt-center">
            Copyright &copy;
            <script>
                document.write(new Date().getFullYear());
            </script> All rights reserved <i class="fa fa-heart-o" aria-hidden="true"></i> by <a
                href="https://iweb.co.id" target="_blank"> @yield('copyright')</a>.
        </p>
    </div>
    </div>
</footer>
@endif
