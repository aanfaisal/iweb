<div class="form-group {{ $errors->has('judul') ? 'has-error' : ''}}">
    <label for="judul" class="control-label">{{ 'Judul' }}</label>
    <input class="form-control" name="judul" type="text" id="judul" placeholder="Isikan Judul Tentang Kami. . ."
        value="{{ isset($settingtentang->judul) ? $settingtentang->judul : ''}}" required>

    <div class="invalid-feedback">
        Judul Wajib Diisi
    </div>
    {!! $errors->first('judul', '<p class="help-block">:message</p>') !!}

</div>

<div class="form-group {{ $errors->has('keterangan') ? 'has-error' : ''}}">
    <label for="keterangan" class="control-label">{{ 'Keterangan' }}</label>
    <textarea class="form-control" rows="5" name="keterangan" type="text" id="keterangan"
        placeholder="Isikan Perihal Tentang Kami. . ."
        required>{{ isset($settingtentang->keterangan) ? $settingtentang->keterangan : ''}}</textarea>

    <div class="invalid-feedback">
        Keterangan Wajib Diisi
    </div>

    {!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value={{ $formMode==='edit' ? 'Update' : 'Simpan' }}>
</div>
