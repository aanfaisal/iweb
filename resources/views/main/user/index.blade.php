@extends('layouts.main.app')

@section('title')
Data User
@endsection

@section('content')
<div class="panel-header">
    <div class="page-inner">
        <div class="d-flex align-items-left flex-column align-items-md-center flex-column flex-md-row">
            <h2 class="pb-2 fw-bold">@yield('title')</h2>
            <h5 class="op-7 mb-4"></h5>
        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="btn-group">
                        <a href="{{ route('user.create') }}" class="btn btn-success btn-md" title="Add Data User">
                            <i class="fas fa-plus" aria-hidden="true"></i> &nbsp;Tambah User
                        </a>
                    </div>
                </div>
                <div class="col-md-4 ml-auto">
                    <form method="GET" action="{{ route('user.index') }}" accept-charset="UTF-8" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..."
                                value="{{ request('search') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        @foreach($user as $item)
        <div class="col-md-3 col-lg-3">
            <div class="card card-profile">
                <div class="card-header"
                    style="background-image: url('@if(is_null($item->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.$item->image) }}@endif')">
                    <div class="profile-picture">
                        <div class="avatar avatar-xl">
                            <img src="@if(is_null($item->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.$item->image) }}@endif"
                                class="avatar-img rounded-circle">
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="user-profile text-center">
                        <div class="name">{{ $item->name }}</div>
                        {{-- <div class="job">Role : {{ ucfirst($item->roles->first()->name) }}</div> --}}
                        <div class="desc"></div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="d-flex justify-content-center">
                        <div class="btn-group" role="group">
                            <a href="{{ route('user.profil', $item->id)  }}"><button type="button"
                                    class="btn btn-md btn-default"><i class="fas fa-cloud"></i>&nbsp;</button></a>
                            <a href="{{ route('user.edit', $item->id) }}" title="Edit User"><button
                                    class="btn btn-primary btn-md"><i class="fas fa-edit" aria-hidden="true"></i>
                                </button></a>

                            <form method="POST" action="{{ route('user.destroy', $item->id) }}" accept-charset="UTF-8"
                                style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-md delete-confirm"><i
                                        class="fas fa-trash-alt" aria-hidden="true"></i>
                                </button>
                            </form>

                        </div>
                    </div>
                    <hr>

                    @if($item->paket_aktif == 1)
                    <form action="{{ route('user-admin.active', $item->id) }}" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success btn-block" name="active" value="0"
                            disabled>Aktif</button>
                    </form>
                    <span>Sejak : {{ $item->paket_tanggal }} </span>
                    @else
                    <form action="{{ route('user-admin.active', $item->id) }}" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-default btn-block" name="active"
                            value="1">Inactive</button>
                    </form>
                    @endif

                </div>
            </div>
        </div>
        @endforeach


    </div>
    <div class="card">
        <div class="card-body">
            <div class="pagination-wrapper">
                {{ $user->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('vendor/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-buttons/css/buttons.bootstrap4.min.css') }}">

@endsection

@section('js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('vendor/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('vendor/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- Page specific script -->

<script>
    $(function () {
    if ( $.fn.dataTable.isDataTable( '#settingbiaya' ) ) {
            table = $('#settingbiaya').DataTable();
        }
        else {
            table = $("#settingbiaya").DataTable({
                        responsive: true,
                        lengthChange: false,
                        autoWidth: false,
                        searching: false,
                        paging: true,
                        ordering: false,
                        buttons: ["copy", "csv", "excel", "pdf", "print"]
            }).buttons().container().appendTo('#settingbiaya_wrapper .col-md-6:eq(0)');

        }
    });

    $('.delete-confirm').on('click', function (event) {
        var form =  $(this).closest("form");
        var name = $(this).data("name");
        event.preventDefault();
        swal({
            title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
            text: "Jika dihapus data tidak akan ditampilkan lagi",
            icon: "warning",
            buttons: ["Tidak", "Ya!"],
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            form.submit();
        }
        });
    });
</script>
<script>
    $('.pagination li').addClass('page-item');
      $('.pagination li a').addClass('page-link');
      $('.pagination span').addClass('page-link');
</script>

@endsection
