@extends('layouts.main.app')

@section('title')
Tambah User
@endsection

@section('content')

<div class="page-inner">
    <div class="card">
        <div class="card-header">
            <div class="card-title">@yield('title')</div>
        </div>
        <div class="card-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form method="POST" action="{{ route('user.store' ) }}" accept-charset="UTF-8"
                class="form-horizontal form-bordered" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('nama', 'Nama ' . ':*') !!}
                            {!! Form::text('nama', null, ['class' => 'form-control', 'required', 'placeholder' =>
                            'Nama user' ]); !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('username', 'Username Login' . ':') !!}
                            {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' =>
                            'Username (tanpa spasi)' ]); !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('role', 'role' . ':*') !!}
                            {!! Form::select('role', $roles, null, ['class' => 'form-control ']); !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('birthday', 'Birthday' . ':') !!}
                            {!! Form::date('birthday', null, ['class' => 'form-control', 'required', 'placeholder'
                            => 'Birthday User' ]); !!}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('email', 'Alamat Email' . ':*') !!}
                            {!! Form::text('email', null, ['class' => 'form-control', 'required', 'placeholder' =>
                            'Email User' ]); !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('no_whatapps', 'Nomor Whatapps' . ':*') !!}
                            <input class="form-control" required="" placeholder="Nomor Whatapps . . ."
                                name="no_whatapps" type="number" id="no_whatapps" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('paket', 'Pilih Paket' . ':*') !!}
                            <select name="paket" class="form-control" id="paket" required>
                                @foreach ($paket as $value)
                                <option value="{{ $value->namapaket }}" {{ (isset($user->paket) &&
                                    $user->paket == $value->namapaket) ? 'selected' : ''}}> {{ $value->namapaket }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('password', 'Password' . ':*') !!}
                            {!! Form::password('password', ['class' => 'form-control', 'required', 'placeholder' =>
                            'password' ]); !!}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            {!! Form::label('confirm_password', 'Konfirmasi Password' . ':*') !!}
                            {!! Form::password('confirm_password', ['class' => 'form-control', 'required',
                            'placeholder' => 'Konfirmasi Password' ]); !!}
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('webaddress', 'Web Address ' . ':') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-address-card"></i></span>
                                </div>
                                <input id="webaddress" type="text" class="form-control " name="webaddress" value=""
                                    placeholder="Web Link" required="">
                                <div class="input-group-append">
                                    <span class="input-group-text">.iweb.co.id</span>
                                </div>
                            </div>

                            <span class="invalid-feedback">
                                Web Link Wajib Diisi
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('keterangan', 'Keterangan Web Site' . ':') !!}

                            <textarea name="keterangan" id="keterangan" cols="30" rows="5"
                                class="form-control"></textarea>
                        </div>
                    </div>


                </div>

                <hr>
                @role('admin')

                @endrole

                <div class="row h-100 justify-content-center align-items-center">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection

@section('css')
<!-- iCheck -->
<link rel="stylesheet" href="{{ asset('vendor/iCheck/square/blue.css') }}">

@endsection
@section('js')
<!-- iCheck -->
<script src="{{ asset('vendor/iCheck/icheck.min.js') }}"></script>

<script class="text/javascript">
    $(document).ready(function(){
    //initialize iCheck
        $('input[type="checkbox"].input-icheck, input[type="radio"].input-icheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
        });
        $(document).on( 'ifChecked', '.check_all', function(){
            $(this).closest('.check_group').find('.input-icheck').each( function(){
                $(this).iCheck('check');
            });
        });
        $(document).on( 'ifUnchecked', '.check_all', function(){
            $(this).closest('.check_group').find('.input-icheck').each( function(){
                $(this).iCheck('uncheck');
            });
        });
        $('.check_all').each( function(){
            var length = 0;
            var checked_length = 0;
            $(this).closest('.check_group').find('.input-icheck').each( function(){
                length += 1;
                if($( this ).iCheck('update')[0].checked){
                    checked_length += 1;
                }
            });
            length = length - 1;
            if( checked_length != 0 && length == checked_length){
                $(this).iCheck('check');
            }
    });
});

</script>
@endsection
