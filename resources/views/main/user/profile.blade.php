@extends('layouts.main.app')

@section('title')
Profil User #{{ $user->username }} #USERID : {{ $user->id }}
@endsection

@section('content')

<div class="page-inner">
    <div class="card">
        <div class="card-header">
            <div class="card-title">@yield('title')</div>
        </div>
        <div class="card-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>

    <!-- start: page -->
    <div class="row">
        <div class="col-md-3 col-lg-3">
            <div class="card card-profile">
                <div class="card-header"
                    style="background-image: url('@if(is_null($user->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.$user->image) }}@endif')">
                    <div class="profile-picture">
                        <div class="avatar avatar-xl">
                            <img src="@if(is_null($user->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.$user->image) }}@endif"
                                class="avatar-img rounded-circle">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="user-profile text-center">
                        <div class="name">{{ $user->nama }}</div>
                        <div class="job">User</div>
                        <div class="desc"></div>
                        <div class="view-profile">

                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row user-stats text-center">
                        <form enctype="multipart/form-data" action="{{ route('user.uploadfoto', $user->id ) }}"
                            method="POST">
                            {{ csrf_field() }}
                            <input type="file" class="form-control-file" id="image" name="image" />
                            <input type="submit" class="btn btn-block btn-sm btn-warning" name="image"
                                value="Upload Image" />
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-lg-9">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">@yield('title')</div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('user.profil', $user->id ) }}" accept-charset="UTF-8"
                        class="form-horizontal form-bordered" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('nama', 'nama' . ':*') !!}
                                        {!! Form::text('nama', $user->name, ['class' => 'form-control', 'required',
                                        'placeholder' => 'Nama User'
                                        ]); !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('username', 'username' . ':') !!}
                                        <input class="form-control" required="" placeholder="username" name="username"
                                            type="text" value="{{ $user->username }}" id="username" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('role', 'role' . ':*') !!}
                                        <input class="form-control" required="" placeholder="role" name="role"
                                            type="text" value="{{ $role->name }}" id="role" required readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('birthday', 'Birthday' . ':') !!}
                                        {!! Form::date('birthday', $user->birthday, ['class' => 'form-control',
                                        'required',
                                        'placeholder'
                                        => 'Birthday User' ]); !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('email', 'Alamat Email' . ':*') !!}
                                        {!! Form::text('email', $user->email , ['class' => 'form-control', 'required',
                                        'placeholder' => 'Email
                                        User' ]); !!}

                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('no_whatapps', 'Nomor Whatapps' . ':*') !!}
                                        <input class="form-control" required="" placeholder="Nomor Whatapps . . ."
                                            name="no_whatapps" type="text" value="{{ $user->no_whatapps }}"
                                            id="username" required>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('password', 'Password' . ':*') !!}
                                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' =>
                                        'password' ]); !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        {!! Form::label('confirm_password', 'Konfirmasi Password' . ':*') !!}
                                        {!! Form::password('confirm_password', ['class' => 'form-control', 'placeholder'
                                        => 'Konfirmasi Password' ]); !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('webaddress', 'Web Address ' . ':') !!}
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="fas fa-address-card"></i></span>
                                            </div>
                                            <input id="webaddress" type="text" class="form-control " name="webaddress"
                                                value="" placeholder="Web Link" required=""
                                                value="{{ $user->webaddress }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.iweb.co.id</span>
                                            </div>
                                        </div>

                                        <span class="invalid-feedback">
                                            Web Link Wajib Diisi
                                        </span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('keterangan', 'Keterangan Web Site' . ':') !!}

                                        <textarea name="keterangan" id="keterangan" cols="30" rows="5"
                                            class="form-control">{{ $user->keterangan }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>

                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                    </div>
                </div>

            </div>
        </div>
    </div>


    </form>

</div>
@endsection

@section('css')
<!-- iCheck -->
<link rel="stylesheet" href="{{ asset('vendor/iCheck/square/blue.css') }}">

@endsection
@section('js')
<!-- iCheck -->
<script src="{{ asset('vendor/iCheck/icheck.min.js') }}"></script>

<script class="text/javascript">
    $(document).ready(function(){
        //initialize iCheck
        $('input[type="checkbox"].input-icheck, input[type="radio"].input-icheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
        $(document).on( 'ifChecked', '.check_all', function(){
            $(this).closest('.check_group').find('.input-icheck').each( function(){
            $(this).iCheck('check');
            });
        });
        $(document).on( 'ifUnchecked', '.check_all', function(){
            $(this).closest('.check_group').find('.input-icheck').each( function(){
            $(this).iCheck('uncheck');
            });
        });
        $('.check_all').each( function(){
            var length = 0;
            var checked_length = 0;
            $(this).closest('.check_group').find('.input-icheck').each( function(){
            length += 1;
            if($( this ).iCheck('update')[0].checked){
                checked_length += 1;
            }
            });
            length = length - 1;
            if( checked_length != 0 && length == checked_length){
            $(this).iCheck('check');
            }
    });
});

</script>
@endsection
