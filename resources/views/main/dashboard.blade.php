@extends('layouts.main.app')

@section('title')Dashboard @endsection

@section('content')
<div class="panel-header">
    <div class="page-inner border-bottom pb-0 mb-3">
        <div class="d-flex align-items-left flex-column">
            <h2 class="pb-2 fw-bold">Selamat Datang, @if(is_null(auth()->user()->username)) @else {{
                auth()->user()->username }}
                @endif</h2>
            <h5 class="op-7 mb-4">{!! $quote !!}</h5>
        </div>
    </div>
</div>
<div class="page-inner">
    <div class="row">
        @role('admin')
        <div class="col-sm-6 col-md-4">
            <div class="card card-stats card-primary card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="flaticon-users"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Main Visitors</p>
                                <h4 class="card-title">{{ $visitors }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="card card-stats card-info card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="flaticon-interface-6"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Jumlah User</p>
                                <h4 class="card-title">{{ $usercount }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="card card-stats card-success card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="flaticon-analytics"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Paket Aktif</p>
                                <h4 class="card-title"> {{ $paket_count }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endrole

    </div>




</div>


@endsection

@section('css')

@endsection

@section('js')

@endsection
