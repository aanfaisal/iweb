@extends('layouts.main.app')
@section('title')
Lihat Data SettingTestimoni #{{ $settingtestimoni->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Data settingtestimoni</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <a href="{{ route('setting-testimoni.index') }}" title="Kembali"><button
                        class="btn btn-warning btn-sm"><i class="fas fa-arrow-left" aria-hidden="true"></i>
                        Kembali</button></a>

                <a href="{{ route('setting-testimoni.edit', $settingtestimoni->id) }}"
                    title="Edit SettingTestimoni"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"
                            aria-hidden="true">&nbsp; Edit</i>
                    </button></a>

                <form method="POST" action="{{ route('setting-testimoni.destroy', $settingtestimoni->id) }}"
                    accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm delete-confirm"
                        title="Delete SettingTestimoni"><i class="fas fa-trash" aria-hidden="true"></i>
                        &nbsp; Hapus</button>
                </form>
                <br />
                <br />

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $settingtestimoni->id }}</td>
                            </tr>
                            <tr>
                                <th> Judul </th>
                                <td> {{ $settingtestimoni->judul }} </td>
                            </tr>
                            <tr>
                                <th> Keterangan </th>
                                <td> {{ $settingtestimoni->keterangan }} </td>
                            </tr>
                            <tr>
                                <th> Foto </th>
                                <td> {{ $settingtestimoni->foto }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')

@endsection

@section('js')
<script>
    $('.delete-confirm').on('click', function (event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
              text: "Jika dihapus data tidak akan ditampilkan lagi",
              icon: "warning",
              buttons: ["Tidak", "Ya!"],
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
</script>
@endsection
