<div class="form-group {{ $errors->has('judul') ? 'has-error' : ''}}">
    <label for="judul" class="control-label">{{ 'Nama' }}</label>
    <input class="form-control" name="judul" type="text" id="judul"
        value="{{ isset($settingtestimoni->judul) ? $settingtestimoni->judul : ''}}"
        placeholder="Isikan Nama Orang . . ." required>

    <div class="invalid-feedback">
        Nama Wajib Diisi
    </div>
    {!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('keterangan') ? 'has-error' : ''}}">
    <label for="keterangan" class="control-label">{{ 'Keterangan' }}</label>
    <textarea class="form-control" rows="10" name="keterangan" type="text" id="keterangan"
        placeholder="Isikan Perihal Testimoni . . ."
        required>{{ isset($settingtestimoni->keterangan) ? $settingtestimoni->keterangan : ''}}</textarea>

    <div class="invalid-feedback">
        Keterangan Wajib Diisi
    </div>

    {!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <label for="foto" class="control-label">{{ 'Foto Orangnya' }}</label>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file">
                Pilih Foto <input type="file" name="foto" id="foto">
            </span>
        </span>
        <input type="text" class="form-control"
            value="{{ isset($settingtestimoni->foto) ? $settingtestimoni->foto : ''}}" readonly>
    </div>
    <br>
    <center><img id='img-upload' class="img-fluid rounded"
            src="@if(isset($settingtestimoni->foto)){{ asset('img/testimoni/'.$settingtestimoni->foto) }}@else{{ asset('img/testimoni/testimoni.jpg') }}@endif" />
    </center>

</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value={{ $formMode==='edit' ? 'Update' : 'Simpan' }}>
</div>
