@extends('layouts.main.app')
@section('title')
Lihat Data Setting HomePage #{{ $settinghomepage->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="btn-group" role="group">
                    <a href="{{ route('setting-homepage.index') }}" title="Kembali"><button
                            class="btn btn-warning btn-sm"><i class="fas fa-arrow-left" aria-hidden="true"></i>
                            Kembali</button></a>

                    <a href="{{ route('setting-homepage.edit', $settinghomepage->id) }}"
                        title="Edit SettingHomePage"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"
                                aria-hidden="true">&nbsp; Edit</i>
                        </button></a>

                    <form method="POST" action="{{ route('setting-homepage.destroy', $settinghomepage->id) }}"
                        accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm delete-confirm"
                            title="Delete SettingHomePage"><i class="fas fa-trash" aria-hidden="true"></i>
                            &nbsp; Hapus</button>
                    </form>
                </div>
                <br />
                <br />

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $settinghomepage->id }}</td>
                            </tr>
                            <tr>
                                <th> Slider Background </th>
                                <td>
                                    <center>
                                        <img id='img-upload' style="width: 40%"
                                            src="{{ asset('img/mainslider/'.$settinghomepage->sliderbackground) }}" />
                                        <br><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <th> Slider Mobile </th>
                                <td>
                                    <center>
                                        <img id='img-uploads' style="width: 40%"
                                            src="{{ asset('img/mainslider/'.$settinghomepage->slidermobile) }}" />
                                        <br><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <th> Theme </th>
                                <td> {{ $settinghomepage->theme }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>


                <h3 class="card-title">Data settinghomepage</h3>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            @if($settinghomepagedetail->isEmpty())
                            <p>Tidak Ada Detail</p>
                            @else
                            @foreach ($settinghomepagedetail as $item)
                            <tr>
                                <th> Slidernumber </th>
                                <td> {{ $item->slidernumber }} </td>
                            </tr>
                            <tr>
                                <th> Keterangan1 </th>
                                <td> {{ $item->keterangan1 }} </td>
                            </tr>
                            <tr>
                                <th> Keterangan2 </th>
                                <td> {{ $item->keterangan2 }} </td>
                            </tr>
                            <tr>
                                <th> Keterangan3 </th>
                                <td> {{ $item->keterangan3 }} </td>
                            </tr>
                            <hr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>




            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')

@endsection

@section('js')
<script>
    $('.delete-confirm').on('click', function (event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
              text: "Jika dihapus data tidak akan ditampilkan lagi",
              icon: "warning",
              buttons: ["Tidak", "Ya!"],
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
</script>
@endsection
