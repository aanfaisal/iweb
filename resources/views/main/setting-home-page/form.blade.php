<div class="form-group {{ $errors->has('sliderbackground') ? 'has-error' : ''}}">
    <label for="sliderbackground" class="control-label">{{ 'Pilih Foto Slider (Desktop)' }}</label>
    <span class="required-label">*</span>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file" data-toggle="tooltip" data-placement="top"
                title="Resolusi Rekomendasi: 1280 x 720">
                Pilih Foto Slider <input type="file" name="sliderbackground" id="sliderbackground">
            </span>
        </span>
        <input type="text" class="form-control"
            value="{{ isset($settinghomepage->sliderbackground) ? $settinghomepage->sliderbackground : ''}}" readonly>
    </div>
    <br>
    <center>
        <img id='img-upload' class="img-fluid rounded"
            src="@if(isset($settinghomepage->sliderbackground)){{ asset('img/mainslider/'.$settinghomepage->sliderbackground) }}@else{{ asset('img/userslider/slider.jpg') }}@endif" />
    </center>

    {!! $errors->first('sliderbackground', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('slidermobile') ? 'has-error' : ''}}">
    <label for="slidermobile" class="control-label">{{ 'Pilih Foto Slider (Mobile)' }}</label>
    <span class="required-label">*</span>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file" data-toggle="tooltip" data-placement="top"
                title="Resolusi Rekomendasi: 461 x 1024">
                Pilih Foto Slider <input type="file" name="slidermobile" id="slidermobile">
            </span>
        </span>
        <input type="text" class="form-control"
            value="{{ isset($settinghomepage->slidermobile) ? $settinghomepage->slidermobile : ''}}" readonly>
    </div>
    <br>
    <center>
        <img id='img-uploads' class="img-fluid rounded"
            src="@if(isset($settinghomepage->slidermobile)){{ asset('img/mainslider/'.$settinghomepage->slidermobile) }}@else{{ asset('img/userslider/mobile.jpg') }}@endif" />
    </center>

    {!! $errors->first('slidermobile', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('caption') ? 'has-error' : ''}}">
    <label for="caption" class="control-label">{{ 'Keterangan Foto' }}</label>
    <input class="form-control" name="caption" type="text" id="caption"
        value="{{ isset($settinghomepage->caption) ? $settinghomepage->caption : ''}}" required>

    <div class="invalid-feedback">
        Caption Wajib Diisi
    </div>
    {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('theme') ? 'has-error' : ''}}">
    <label for="theme" class="control-label">{{ 'Warna' }}</label>
    <select name="theme" id="theme" class="form-control select" required>

        <option value="white" {{ (isset($settinghomepage->theme) && $settinghomepage->theme == 'white') ? 'selected' :
            ''}} > White Mode
        </option>
        <option value="dark" {{ (isset($settinghomepage->theme) && $settinghomepage->theme == 'dark') ? 'selected'
            :
            ''}} > Dark Mode
        </option>

    </select>

    <div class="invalid-feedback">
        Tema Wajib Dipilih
    </div>

    {!! $errors->first('theme', '<p class="help-block">:message</p>') !!}
</div>

<label for="kodenegara" class="form-group control-label"><strong> {{ 'Kode Negara + No Whatapps' }} </strong></label>
<div class="form-group form-inline">
    <div class="col-md-3">
        <input class="form-control " name="kodenegara" type="text" id="kodenegara"
            value="{{ isset($settinghomepage->kodenegara) ? $settinghomepage->kodenegara : ''}}"
            placeholder="Kode Negara" required>
    </div>
    <div class="col-md-9">
        <input class="form-control" name="no_whatapps" type="number" id="no_whatapps"
            value="{{ isset($settinghomepage->no_whatapps) ? $settinghomepage->no_whatapps : ''}}"
            placeholder="No Whatapp" required>
    </div>

    <div class="invalid-feedback">
        Kode Negara & No Whatapps Wajib Diisi
    </div>
    {!! $errors->first('kodenegara', '<p class="help-block">:message</p>') !!}
</div>

<label class="form-group" for="Add Detail"><strong>{{ 'Detail Lainnya :' }}</strong></label>

<div class="form-group fieldGroup">
    <label for="detail" class="control-label">{{ 'Item Detail' }}</label>
    <div class="input-group-addon ml-3">
        <a href="javascript:void(0)" class="btn btn-success btn-block addMore"><i class="fas fa-plus"></i> Tambah Item
            Detail</a>
    </div>
</div>

@if(empty($settinghomepagedetail))

@else

@foreach ($settinghomepagedetail as $item)
<div class="form-group fieldGroup">
    <div class="form-group {{ $errors->has('slidernumber') ? 'has-error' : ''}}">
        <label for="slidernumber[]" class="control-label">{{ 'Slider No.' }}</label>
        <input class="form-control" name="slidernumber[]" type="number" id="slidernumber[]" min="0"
            value="{{ $item->slidernumber }}">
        <div class="invalid-feedback">
            Slide Number Wajib Diisi
        </div>

        {!! $errors->first('slidernumber', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('keterangan1') ? 'has-error' : ''}}">
        <label for="keterangan1[]" class="control-label">{{ 'Keterangan 1' }}</label>
        <textarea class="form-control summernote" rows="3" name="keterangan1[]" type="text" id="keterangan1[]"
            placeholder="Keterangan 1 => cth : Apapun Bisnis Anda. . .">{{ $item->keterangan1 }}</textarea>

        <div class="invalid-feedback">
            Keterangan 1 Wajib Diisi
        </div>

        {!! $errors->first('keterangan1', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('keterangan2') ? 'has-error' : ''}}">
        <label for="keterangan2[]" class="control-label">{{ 'Keterangan 2' }}</label>
        <textarea class="form-control summernote" rows="3" name="keterangan2[]" type="text" id="keterangan2[]"
            placeholder="Keterangan 2 => cth : IWEB Solusi Efektif . . .">{{  $item->keterangan2 }}</textarea>
        <div class="invalid-feedback">
            Keterangan 2 Wajib Diisi
        </div>

        {!! $errors->first('keterangan2', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('keterangan3') ? 'has-error' : ''}}">
        <label for="keterangan3[]" class="control-label">{{ 'Keterangan 3' }}</label>
        <textarea class="form-control summernote" rows="3" name="keterangan3[]" type="text" id="keterangan3[]"
            placeholder="Keterangan 2 => cth : Tanpa Harus Coding . . .">{{ $item->keterangan3 }}</textarea>
        <div class="invalid-feedback">
            Keterangan 3 Wajib Diisi
        </div>

        {!! $errors->first('keterangan3', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group mr-auto ml-auto">
        <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"> HAPUS</i></a>
    </div>
</div>

@endforeach
@endif

<div class="form-group form-group-default fieldGroupCopy" style="display: none;">
    <div class="form-group {{ $errors->has('slidernumber') ? 'has-error' : ''}}">
        <label for="slidernumber[]" class="control-label">{{ 'Slider No.' }}</label>
        <input class="form-control" name="slidernumber[]" type="number" id="slidernumber[]" min="0"
            value="{{ isset($settinghomepagedetail->slidernumber) ? $settinghomepagedetail->slidernumber : ''}}">
        <div class="invalid-feedback">
            Slide Number Wajib Diisi
        </div>

        {!! $errors->first('slidernumber', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('keterangan1') ? 'has-error' : ''}}">
        <label for="keterangan1[]" class="control-label">{{ 'Keterangan 1' }}</label>
        <textarea class="form-control summernote" rows="3" name="keterangan1[]" type="text" id="keterangan1[]"
            placeholder="Keterangan 1 => cth : Apapun Bisnis Anda. . .">{{ isset($settinghomepagedetail->keterangan1) ? $settinghomepagedetail->keterangan1 : ''}}</textarea>

        <div class="invalid-feedback">
            Keterangan 1 Wajib Diisi
        </div>

        {!! $errors->first('keterangan1', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('keterangan2') ? 'has-error' : ''}}">
        <label for="keterangan2[]" class="control-label">{{ 'Keterangan 2' }}</label>
        <textarea class="form-control summernote" rows="3" name="keterangan2[]" type="text" id="keterangan2[]"
            placeholder="Keterangan 2 => cth : IWEB Solusi Efektif . . .">{{ isset($settinghomepagedetail->keterangan2) ? $settinghomepagedetail->keterangan2 : ''}}</textarea>
        <div class="invalid-feedback">
            Keterangan 2 Wajib Diisi
        </div>

        {!! $errors->first('keterangan2', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group {{ $errors->has('keterangan3') ? 'has-error' : ''}}">
        <label for="keterangan3[]" class="control-label">{{ 'Keterangan 3' }}</label>
        <textarea class="form-control summernote" rows="3" name="keterangan3[]" type="text" id="keterangan3[]"
            placeholder="Keterangan 2 => cth : Tanpa Harus Coding . . .">{{ isset($settinghomepagedetail->keterangan3) ? $settinghomepagedetail->keterangan3 : ''}}</textarea>
        <div class="invalid-feedback">
            Keterangan 3 Wajib Diisi
        </div>

        {!! $errors->first('keterangan3', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group mr-auto ml-auto">
        <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"> HAPUS</i></a>
    </div>
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value={{ $formMode==='edit' ? 'Update' : 'Simpan' }}>
</div>
