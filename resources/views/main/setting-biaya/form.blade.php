<div class="form-group form-group-default {{ $errors->has('namapaket') ? 'has-error' : ''}}">
    <label for="namapaket" class="control-label">{{ 'Nama Paket' }}</label>
    <input class="form-control" name="namapaket" type="text" id="namapaket"
        value="{{ isset($settingbiaya->namapaket) ? $settingbiaya->namapaket : ''}}" placeholder="Nama Paket . . ."
        required>
    <div class="invalid-feedback">
        Nama Paket Wajib Diisi
    </div>

    {!! $errors->first('namapaket', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group form-group-default {{ $errors->has('warna') ? 'has-error' : ''}}">
    <label for="warna" class="control-label">{{ 'Warna' }}</label>
    <select name="warna" id="warna" class="form-control select" required>
        <option value="blue" {{ (isset($settingbiaya->warna) && $settingbiaya->warna == 'blue') ? 'selected' :
            ''}}> Biru
        </option>
        <option value="green" {{ (isset($settingbiaya->warna) && $settingbiaya->warna == 'green') ? 'selected' :
            ''}}> Hijau
        </option>
        <option value="default" {{ (isset($settingbiaya->warna) && $settingbiaya->warna == 'default') ? 'selected' :
            ''}}> Kuning
        </option>
        <option value="red" {{ (isset($settingbiaya->warna) && $settingbiaya->warna == 'red') ? 'selected' :
            ''}} > Merah
        </option>
        <option value="secondary" {{ (isset($settingbiaya->warna) && $settingbiaya->warna == 'secondary') ? 'selected' :
            ''}} > Ungu
        </option>

    </select>

    <div class="invalid-feedback">
        Nama Paket Wajib Diisi
    </div>

    {!! $errors->first('warna', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group form-group-default {{ $errors->has('hargaasli') ? 'has-error' : ''}}">
    <label for="hargaasli" class="control-label">{{ 'Harga Asli' }}</label>
    <input class="form-control" name="hargaasli" type="number" id="hargaasli"
        value="{{ isset($settingbiaya->hargaasli) ? $settingbiaya->hargaasli : ''}}" placeholder="Harga Asli . . ."
        required>
    <div class="invalid-feedback">
        Harga Asli Paket Wajib Diisi
    </div>

    {!! $errors->first('hargaasli', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group form-group-default {{ $errors->has('hargadiskon') ? 'has-error' : ''}}">
    <label for="hargadiskon" class="control-label">{{ 'Harga Diskon' }}</label>
    <input class="form-control" name="hargadiskon" type="number" id="hargadiskon"
        value="{{ isset($settingbiaya->hargadiskon) ? $settingbiaya->hargadiskon : ''}}"
        placeholder="Harga Diskon . . ." required>
    <div class="invalid-feedback">
        Harga Diskon Paket Wajib Diisi
    </div>

    {!! $errors->first('hargadiskon', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group form-group-default {{ $errors->has('jumlahmenu') ? 'has-error' : ''}}">
    <label for="jumlahmenu" class="control-label">{{ 'Jumlah Menu' }}</label>
    <input class="form-control" name="jumlahmenu" type="number" id="jumlahmenu"
        value="{{ isset($settingbiaya->jumlahmenu) ? $settingbiaya->jumlahmenu : ''}}" placeholder="Jumlah Menu . . ."
        required>
    <div class="invalid-feedback">
        Jumlah Menu Wajib Diisi
    </div>

    {!! $errors->first('jumlahmenu', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group form-group-default {{ $errors->has('ket') ? 'has-error' : ''}}">
    <label for="ket" class="control-label">{{ 'Keterangan' }}</label>
    <textarea class="form-control" rows="3" name="ket" type="text" id="ket"
        placeholder="Keterangan . . .">{{ isset($settingbiaya->ket) ? $settingbiaya->ket : ''}}</textarea>
    <div class="invalid-feedback">
        Keterangan Diisi
    </div>
</div>

<div class="form-group fieldGroup">
    <label for="detail" class="control-label">{{ 'Item Detail' }}</label>
    <div class="input-group-addon ml-3">
        <a href="javascript:void(0)" class="btn btn-success btn-block addMore"><i class="fas fa-plus"></i> Tambah Item
            Detail</a>
    </div>
</div>

<div class="form-group form-group-default fieldGroupCopy" style="display: none;">
    <div class="form-group">
        <div class="input-group">
            <input type="text" name="detail[]" class="form-control"
                placeholder="Input Detail (cth : Layanan Bantuan (CS))" aria-label="" aria-describedby="basic-addon1">

            <div class="input-group-prepend">
                <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"></i></a>
            </div>
        </div>
    </div>
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value={{ $formMode==='edit' ? 'Update' : 'Simpan' }}>
</div>
