@extends('layouts.main.app')
@section('title')
Edit Data SettingBiaya #{{ $settingbiaya->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <a href="{{ route('setting-biaya.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                            class="fas fa-arrow-left" aria-hidden="true"></i>
                        Kembali</button></a>
                <br />
                <br />

                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif

                <form method="POST" action="{{ route('setting-biaya.update', $settingbiaya->id) }}"
                    accept-charset="UTF-8" class="form-horizontal" id="needs-validation" enctype="multipart/form-data"
                    novalidate>
                    @csrf
                    @method('PUT')
                    <div class="form-group {{ $errors->has('namapaket') ? 'has-error' : ''}}">
                        <label for="namapaket" class="control-label">{{ 'Nama Paket' }}</label>
                        <input class="form-control" name="namapaket" type="text" id="namapaket"
                            value="{{ isset($settingbiaya->namapaket) ? $settingbiaya->namapaket : ''}}"
                            placeholder="Nama Paket . . ." required>
                        <div class="invalid-feedback">
                            Nama Paket Wajib Diisi
                        </div>

                        {!! $errors->first('namapaket', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ $errors->has('warna') ? 'has-error' : ''}}">
                        <label for="warna" class="control-label">{{ 'Warna' }}</label>
                        <select name="warna" id="warna" class="form-control select" required>
                            <option {{ isset($settingbiaya->warna)=='blue' ? 'selected' : '' }} value="blue"> Blue
                            </option>
                            <option {{ isset($settingbiaya->warna)=='green' ? 'selected' : '' }} value="green"> Hijau
                            </option>
                            <option {{ isset($settingbiaya->warna)=='default' ? 'selected' : '' }} value="default">
                                Kuning
                            </option>
                            <option {{ isset($settingbiaya->warna)=='red' ? 'selected' : '' }} value="red"> Merah
                            </option>
                            <option {{ isset($settingbiaya->warna)=='secondary' ? 'selected' : '' }} value="secondary">
                                Ungu
                            </option>
                        </select>

                        <div class="invalid-feedback">
                            Nama Paket Wajib Diisi
                        </div>

                        {!! $errors->first('warna', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('hargaasli') ? 'has-error' : ''}}">
                        <label for="hargaasli" class="control-label">{{ 'Harga Asli' }}</label>
                        <input class="form-control" name="hargaasli" type="number" id="hargaasli"
                            value="{{ isset($settingbiaya->hargaasli) ? $settingbiaya->hargaasli : ''}}"
                            placeholder="Harga Asli . . ." required>
                        <div class="invalid-feedback">
                            Harga Asli Paket Wajib Diisi
                        </div>

                        {!! $errors->first('hargaasli', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('hargadiskon') ? 'has-error' : ''}}">
                        <label for="hargadiskon" class="control-label">{{ 'Harga Diskon' }}</label>
                        <input class="form-control" name="hargadiskon" type="number" id="hargadiskon"
                            value="{{ isset($settingbiaya->hargadiskon) ? $settingbiaya->hargadiskon : ''}}"
                            placeholder="Harga Diskon . . ." required>
                        <div class="invalid-feedback">
                            Harga Diskon Paket Wajib Diisi
                        </div>

                        {!! $errors->first('hargadiskon', '<p class="help-block">:message</p>') !!}
                    </div>


                    <div class="form-group {{ $errors->has('jumlahmenu') ? 'has-error' : ''}}">
                        <label for="jumlahmenu" class="control-label">{{ 'Jumlah Menu' }}</label>
                        <input class="form-control" name="jumlahmenu" type="number" id="jumlahmenu"
                            value="{{ isset($settingbiaya->jumlahmenu) ? $settingbiaya->jumlahmenu : ''}}"
                            placeholder="Jumlah Menu . . ." required>
                        <div class="invalid-feedback">
                            Jumlah Menu Wajib Diisi
                        </div>

                        {!! $errors->first('jumlahmenu', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('ket') ? 'has-error' : ''}}">
                        <label for="ket" class="control-label">{{ 'Keterangan' }}</label>
                        <textarea class="form-control" rows="3" name="ket" type="text" id="ket"
                            placeholder="Keterangan . . .">{{ isset($settingbiaya->ket) ? $settingbiaya->ket : ''}}</textarea>
                        <div class="invalid-feedback">
                            Keterangan Diisi
                        </div>
                    </div>

                    <div class="form-group fieldGroup">
                        <label for="detail" class="control-label">{{ 'Item Detail' }}</label>
                        <div class="input-group-addon ml-3">
                            <a href="javascript:void(0)" class="btn btn-success btn-block addMore"><i
                                    class="fas fa-plus"></i> Tambah Item
                                Detail</a>
                        </div>
                    </div>

                    @foreach ($settingbiayadetail as $item )
                    <div class="form-group fieldGroup">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="detail[]" class="form-control"
                                    placeholder="Input Detail (cth : Layanan Bantuan (CS))" aria-label=""
                                    aria-describedby="basic-addon1" value="{{ $item->detail }}">

                                <div class="input-group-prepend">
                                    <a href="javascript:void(0)" class="btn btn-danger remove"><i
                                            class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <div class="form-group fieldGroupCopy" style="display: none;">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="detail[]" class="form-control"
                                    placeholder="Input Detail (cth : Layanan Bantuan (CS))" aria-label=""
                                    aria-describedby="basic-addon1">

                                <div class="input-group-prepend">
                                    <a href="javascript:void(0)" class="btn btn-danger remove"><i
                                            class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="form-group">
                        <input class="btn btn-primary btn-block" type="submit" value={{ 'Update' }}>
                    </div>

                </form>


            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('js')
<!-- Select2 -->
<script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>
<!-- Page specific script -->
<script>
    (function() {
    'use strict';
        window.addEventListener('load', function() {
            var form = document.getElementById('needs-validation');
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        }, false);
    })();

    $(function () {
        //Initialize Select2 Elements
        $('#warna').select2({
            theme: 'bootstrap4'
        })
    });
</script>

<script>
    $(document).ready(function(){
        // membatasi jumlah inputan
        var maxGroup = 10;

        //melakukan proses multiple input
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length <= maxGroup){
                var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';

                $('body').find('.fieldGroup:last').after(fieldHTML);

            } else {

                swal({
                    title: "Mohon Maaf",
                    text:  'Anda Sudah Limit Penambahan Item ! '+maxGroup+'.',
                    icon: "error",
                    buttons: ["Tidak", "Ya!"],
                    dangerMode: true,
                })
            }
        });

        //remove fields group
        $("body").on("click",".remove",function(){
            $(this).parents(".fieldGroup").remove();
        });
    });
</script>
@endsection
