@extends('layouts.main.app')
@section('title')
Tambah Data Biaya
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Data Biaya</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <a href="{{ route('setting-biaya.index') }}" title="Kembali"><button class="btn btn-warning btn-sm"><i
                            class="fas fa-arrow-left" aria-hidden="true"></i>
                        Kembali</button></a>
                <br />
                <br />

                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif

                <form method="POST" action="{{ route('setting-biaya.store') }}" accept-charset="UTF-8"
                    class="form-horizontal" id="needs-validation" enctype="multipart/form-data" novalidate>
                    @csrf

                    @include ('main.setting-biaya.form', ['formMode' => 'create'])

                </form>


            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection

@section('js')
<!-- Select2 -->
<script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>
<!-- Page specific script -->
<script>
    (function() {
    'use strict';
        window.addEventListener('load', function() {
            var form = document.getElementById('needs-validation');
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        }, false);
    })();

    $(function () {
        //Initialize Select2 Elements
        $('#warna').select2({
            theme: 'bootstrap4'
        })
    });
</script>

<script>
    $(document).ready(function(){
        // membatasi jumlah inputan
        var maxGroup = 10;

        //melakukan proses multiple input
        $(".addMore").click(function(){
            if($('body').find('.fieldGroup').length <= maxGroup){
                var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';

                $('body').find('.fieldGroup:last').after(fieldHTML);

            } else {

                swal({
                    title: "Mohon Maaf",
                    text:  'Anda Sudah Limit Penambahan Item ! '+maxGroup+'.',
                    icon: "error",
                    buttons: ["Tidak", "Ya!"],
                    dangerMode: true,
                })
            }
        });

        //remove fields group
        $("body").on("click",".remove",function(){
            $(this).parents(".fieldGroup").remove();
        });
    });
</script>
@endsection
