@extends('layouts.main.app')
@section('title')
Lihat Data Biaya #{{ $settingbiaya->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Data settingbiaya</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <a href="{{ route('setting-biaya.index') }}" title="Kembali"><button class="btn btn-warning btn-sm"><i
                            class="fas fa-arrow-left" aria-hidden="true"></i>
                        Kembali</button></a>

                <a href="{{ route('setting-biaya.edit', $settingbiaya->id) }}" title="Edit Setting Biaya"><button
                        class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true">&nbsp; Edit</i>
                    </button></a>

                <form method="POST" action="{{ route('setting-biaya.destroy', $settingbiaya->id) }}"
                    accept-charset="UTF-8" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-sm delete-confirm" title="Delete SettingBiaya"><i
                            class="fas fa-trash" aria-hidden="true"></i>
                        &nbsp; Hapus</button>
                </form>
                <br />
                <br />

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $settingbiaya->id }}</td>
                            </tr>

                            <tr>
                                <th> Nama </th>
                                <td> {{ $settingbiaya->namapaket }} </td>
                            </tr>

                            <tr>
                                <th> Harga Asli</th>
                                <td> {{ $settingbiaya->hargaasli }} </td>
                            </tr>

                            <tr>
                                <th> Harga Diskon </th>
                                <td> {{ $settingbiaya->hargadiskon }} </td>
                            </tr>

                            <tr>
                                <th> Jumlah Menu</th>
                                <td> {{ $settingbiaya->jumlahmenu }} </td>
                            </tr>
                            <tr>
                                <th> Ket </th>
                                <td> {{ $settingbiaya->ket }} </td>
                            </tr>
                            <tr>
                                <th> Warna </th>
                                <td> {{ $settingbiaya->warna }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="card-title">Data Detail Paket Biaya</h3>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            @foreach ($settingbiayadetail as $item)
                            <tr>
                                <th> # </th>
                                <td> {{ $item->detail }} </td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>


            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')

@endsection

@section('js')
<script>
    $('.delete-confirm').on('click', function (event) {
        var form =  $(this).closest("form");
        var name = $(this).data("name");
        event.preventDefault();
        swal({
            title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
            text: "Jika dihapus data tidak akan ditampilkan lagi",
            icon: "warning",
            buttons: ["Tidak", "Ya!"],
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            form.submit();
        }
        });
    });
</script>
@endsection
