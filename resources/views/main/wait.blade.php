@extends('layouts.main.app')

@section('title')Dashboard @endsection

@section('content')
<div class="panel-header">
    <div class="page-inner border-bottom pb-0 mb-3">
        <div class="d-flex align-items-left flex-column">
            <h2 class="pb-2 fw-bold">Selamat Datang, @if(is_null(auth()->user()->username)) @else {{
                auth()->user()->username }}
                @endif</h2>
            <h5 class="op-7 mb-4">{!! $quote !!}</h5>
        </div>
    </div>
</div>
<div class="page-inner">

    <div class="card">

        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <i class="fas fa-birthday-cake"></i>
                    We will notify you via whatapps/email
                    when your website ready to use
                </div>
            </div>
        </div>
    </div>




</div>


@endsection

@section('css')

@endsection

@section('js')

@endsection
