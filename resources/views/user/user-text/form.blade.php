<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($usertext->user_id) ? $usertext->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($usertext->username) ? $usertext->username : auth()->user()->username }}" required>

<div class="form-group {{ $errors->has('menu_id') ? 'has-error' : ''}}">
    <label for="menu_id" class="control-label">{{ 'Pilih Menu' }}</label>
    <span class="required-label">*</span>

    <select name="menu_id" class="form-control" id="menu_id" required>
        @foreach ($menu as $value)
        <option value="{{ $value->id }}" {{ (isset($usertext->menu_id) && $usertext->menu_id ==
            $value->id) ? 'selected' : ''}}> {{ $value->title }}
        </option>
        @endforeach
    </select>

    <div class="invalid-feedback">
        Menu Wajib Diisi
    </div>

    {!! $errors->first('menu_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('page_id') ? 'has-error' : ''}}">
    <label for="page_id" class="control-label">{{ 'Pilih Page' }}</label>
    <span class="required-label">*</span>
    <select name="page_id" class="form-control" id="page_id" required>
        @foreach ($page as $value)
        <option value="{{ $value->id }}" {{ (isset($usertext->page_id) && $usertext->page_id ==
            $value->id) ? 'selected' : ''}}> {{ $value->title }}
        </option>
        @endforeach
    </select>

    <div class="invalid-feedback">
        Page Wajib Diisi
    </div>

    {!! $errors->first('page_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('title1') ? 'has-error' : ''}}">
    <label for="title1" class="control-label">{{ 'Judul' }}</label>
    <span class="required-label">*</span>
    <input class="form-control" name="title1" type="text" id="title1"
        value="{{ isset($usertext->title1) ? $usertext->title1 : ''}}" placeholder="Misal : Tentang Kami . . ."
        required>
    <div class="invalid-feedback">
        Judul Wajib Diisi
    </div>

    {!! $errors->first('title1', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('detail1') ? 'has-error' : ''}}">
    <label for="detail1" class="control-label">{{ 'Text 1' }}</label>
    <textarea class="form-control" rows="5" name="detail1" type="text" id="detail1"
        placeholder="Isikan Perihal Detail Text . . .">{{ isset($usertext->detail1) ? $usertext->detail1 : ''}}</textarea>

    <div class="invalid-feedback">
        Detail 1 Wajib Diisi
    </div>

    {!! $errors->first('detail1', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('foto1') ? 'has-error' : ''}}">
    <label for="foto1" class="control-label">{{ 'Foto 1' }}</label>
    <span class="required-label">*</span>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file" data-toggle="tooltip" data-placement="top"
                title="Resolusi Rekomendasi: 600x600">
                Pilih Foto <input type="file" name="foto1" id="foto1">
            </span>
        </span>
        <input type="text" class="form-control" value="{{ isset($usertext->foto1) ? $usertext->foto1 : ''}}" readonly
            required>
    </div>
    <br>
    <center>
        <img id='img-upload' class="img-fluid rounded"
            src="@if(isset($usertext->foto1)){{ asset('img/usertext/'.$usertext->foto1) }}@else{{ asset('img/usertext/text.jpg') }}@endif" />
    </center>


    {!! $errors->first('foto1', '<p class="help-block">:message</p>') !!}
</div>
<hr>

<div class="form-group {{ $errors->has('title2') ? 'has-error' : ''}}">
    <label for="title2" class="control-label">{{ 'Judul 2' }}</label>
    <input class="form-control" name="title2" type="text" id="title2"
        value="{{ isset($usertext->title2) ? $usertext->title2 : ''}}"
        placeholder="Misal : Visi Misi / Alamat / Kontak Kami . . . ">
    <div class="invalid-feedback">
        Judul Wajib Diisi
    </div>

    {!! $errors->first('title2', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('detail2') ? 'has-error' : ''}}">
    <label for="detail2" class="control-label">{{ 'Text 2' }}</label>
    <textarea class="form-control" rows="5" name="detail2" type="text" id="detail2"
        placeholder="Isikan Perihal Detail Text . . .">{{ isset($usertext->detail2) ? $usertext->detail2 : ''}}</textarea>

    <div class="invalid-feedback">
        Detail 2 Wajib Diisi
    </div>

    {!! $errors->first('detail2', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('foto2') ? 'has-error' : ''}}">
    <label for="foto2" class="control-label">{{ 'Foto 2' }}</label>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file" data-toggle="tooltip" data-placement="top"
                title="Resolusi Rekomendasi: 600x600">
                Pilih Foto <input type="file" name="foto2" id="foto2">
            </span>
        </span>
        <input type="text" class="form-control" value="{{ isset($usertext->foto2) ? $usertext->foto2 : ''}}" readonly>
    </div>
    <br>
    <center>
        <img id='img-uploads' class="img-fluid rounded"
            src="@if(isset($usertext->foto2)){{ asset('img/usertext/'.$usertext->foto2) }}@else{{ asset('img/usertext/text.jpg') }}@endif" />
    </center>


    {!! $errors->first('foto2', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
