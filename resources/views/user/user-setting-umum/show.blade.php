@extends('layouts.main.app')
@section('title')
Lihat User Setting Umum #{{ $usersettingumum->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="btn-group" role="group">
                    <a href="{{ route('user-setting-umum.index') }}" title="Kembali"><button
                            class="btn btn-warning btn-sm"><i class="fas fa-arrow-left" aria-hidden="true"></i>
                            Kembali</button></a>

                    <a href="{{ route('user-setting-umum.edit', $usersettingumum->id) }}"
                        title="Edit User Setting Umum"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"
                                aria-hidden="true">&nbsp; Edit</i>
                        </button></a>

                    <form method="POST" action="{{ route('user-setting-umum.destroy', $usersettingumum->id) }}"
                        accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm delete-confirm"
                            title="Delete User Setting Umum"><i class="fas fa-trash" aria-hidden="true"></i>
                            &nbsp; Hapus</button>
                    </form>
                </div>
                <br />
                <br />

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>

                            <tr>
                                <th> Username </th>
                                <td> {{ $usersettingumum->username }} </td>
                            </tr>
                            <tr>
                                <th> Logo </th>
                                <td>
                                    <center>
                                        <img id='img-upload' style="width: 50%"
                                            src="{{ asset('img/logouser/'.$usersettingumum->logo) }}" /> <br><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <th> Namaweb </th>
                                <td> {{ $usersettingumum->namaweb }} </td>
                            </tr>
                            <tr>
                                <th> Tema </th>
                                <td> {{ $usersettingumum->tema }} </td>
                            </tr>
                            <tr>
                                <th> Webaddress </th>
                                <td> {{ $usersettingumum->webaddress }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')

@endsection

@section('js')
<script>
    $('.delete-confirm').on('click', function (event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
              text: "Jika dihapus data tidak akan ditampilkan lagi",
              icon: "warning",
              buttons: ["Tidak", "Ya!"],
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
</script>
@endsection
