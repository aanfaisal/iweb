<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($usersettingumum->user_id) ? $usersettingumum->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($usersettingumum->username) ? $usersettingumum->username : auth()->user()->username }}" required>


<div class="form-group {{ $errors->has('namaweb') ? 'has-error' : ''}}">
    <label for="namaweb" class="control-label">{{ 'Namaweb' }}</label>
    <input class="form-control" name="namaweb" type="text" id="namaweb"
        value="{{ isset($usersettingumum->namaweb) ? $usersettingumum->namaweb : ''}}" required>
    <div class="invalid-feedback">
        namaweb Wajib Diisi
    </div>

    {!! $errors->first('namaweb', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('tema') ? 'has-error' : ''}}">
    <label for="tema" class="control-label">{{ 'Tema' }}</label>
    <select name="tema" id="tema" class="form-control select" required>

        <option value="light" {{ (isset($usersettingumum->tema) && $usersettingumum->tema == 'light') ? 'selected' :
            ''}}> Light Mode
        </option>
        <option value="dark" {{ (isset($usersettingumum->tema) && $usersettingumum->tema == 'dark') ? 'selected' :
            ''}} > Dark Mode
        </option>
    </select>

    <div class="invalid-feedback">
        Tema Wajib Dipilih
    </div>

    {!! $errors->first('tema', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('webaddress') ? 'has-error' : ''}}">
    <label for="webaddress" class="control-label">{{ 'Webaddress' }}</label>
    <input class="form-control" name="webaddress" type="text" id="webaddress"
        value="{{ isset($usersettingumum->webaddress) ? $usersettingumum->webaddress : ''}}" required>
    <div class="invalid-feedback">
        webaddress Wajib Diisi
    </div>

    {!! $errors->first('webaddress', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('logo') ? 'has-error' : ''}}">
    <label for="logo" class="control-label">{{ 'Logo' }}</label>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file">
                Pilih Logo <input type="file" name="logo" id="logo">
            </span>
        </span>
        <input type="text" class="form-control" value="{{ isset($usersettingumum->logo) ? $usersettingumum->logo : ''}}"
            readonly>
    </div>
    <br>
    <center>
        <img id='img-upload' class="img-fluid rounded"
            src="@if(isset($usersettingumum->logo)){{ asset('img/logouser/'.$usersettingumum->logo) }}@else{{ asset('img/logouser/logo.png') }}@endif" />
    </center>


    {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
