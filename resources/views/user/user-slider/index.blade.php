@extends('layouts.main.app')

@section('title')
User Slider
@endsection

@section('content')
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">@yield('title')</h2>
                <h5 class="text-white op-7 mb-3"></h5>
            </div>
        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="container-fluid">
        <!-- CONTENT -->
        <a href="{{ route('user-slider.create') }}" class="btn btn-success btn-sm" title="Tambah User Slider">
            <i class="fa fa-plus" aria-hidden="true"></i> Tambah Data
        </a>

        <br />
        <br />
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Filter Data</h3>


            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="GET" action="{{ route('user-slider.index') }}" accept-charset="UTF-8"
                            role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Cari..."
                                    value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->

            <div class="card-body">

                <div class="table-responsive">
                    <table id="userslider" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu</th>
                                <th>Page</th>
                                <th>Foto</th>
                                <th>Thumbnail</th>
                                <th>Caption</th>
                                <th>SliderTextType</th>
                                <th>Aktif ?</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($userslider as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->menu_id }}</td>
                                <td>{{ $item->page_id }}</td>
                                <td>
                                    <center>
                                        <img id='img-upload' style="width: 40%"
                                            src="{{ asset('img/userslider/'.$item->foto) }}" /> <br><br>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <img id='img-upload' style="width: 40%"
                                            src="{{ asset('img/userslider/'.$item->thumbnail) }}" /> <br><br>
                                    </center>
                                </td>
                                <td>{{ $item->caption }}</td>
                                <td>{{ $item->sliderTextType }}</td>
                                <td>
                                    @if($item->active == 1)
                                    <form action="{{ route('user-slider.active', $item->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-success" name="active"
                                            value="0">Active</button>
                                    </form>
                                    @else
                                    <form action="{{ route('user-slider.active', $item->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-default" name="active"
                                            value="1">Inactive</button>
                                    </form>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <a href="{{ route('user-slider.show', $item->id) }}"
                                            title="Lihat User Slider"><button class="btn btn-info btn-sm"><i
                                                    class="fas fa-eye" aria-hidden="true"></i> </button></a>
                                        <a href="{{ route('user-slider.edit', $item->id) }}"
                                            title="Edit User Slider"><button class="btn btn-primary btn-sm"><i
                                                    class="fas fa-edit" aria-hidden="true"></i>
                                            </button></a>

                                        <form method="POST" action="{{ route('user-slider.destroy', $item->id) }}"
                                            accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm delete-confirm"
                                                title="Hapus User Slider"><i class="fas fa-trash"
                                                    aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- /.card-body -->

            <div class="card-footer">

            </div><!-- /.card-footer -->


        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('vendor/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/datatables-buttons/css/buttons.bootstrap4.min.css') }}">

@endsection

@section('js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('vendor/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('vendor/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('vendor/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- Page specific script -->

<script>
    $(function () {
    if ( $.fn.dataTable.isDataTable( '#userslider' ) ) {
            table = $('#userslider').DataTable();
        }
        else {
            table = $("#userslider").DataTable({
                    responsive: true,
                    lengthChange: false,
                    autoWidth: false,
                    searching: false,
                    paging: true,
                    ordering: false
            }).buttons().container().appendTo('#userslider_wrapper .col-md-6:eq(0)');

        }
    });
    $('.delete-confirm').on('click', function (event) {
        var form =  $(this).closest("form");
        var name = $(this).data("name");
        event.preventDefault();
        swal({
            title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
            text: "Jika dihapus data tidak akan ditampilkan lagi",
            icon: "warning",
            buttons: ["Tidak", "Ya!"],
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            form.submit();
        }
        });
    });
</script>

@endsection
