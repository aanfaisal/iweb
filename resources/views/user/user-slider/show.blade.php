@extends('layouts.main.app')
@section('title')
Lihat User Slider #{{ $userslider->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="btn-group" role="group">
                    <a href="{{ route('user-slider.index') }}" title="Kembali"><button class="btn btn-warning btn-sm"><i
                                class="fas fa-arrow-left" aria-hidden="true"></i>
                            Kembali</button></a>

                    <a href="{{ route('user-slider.edit', $userslider->id) }}" title="Edit User Slider"><button
                            class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true">&nbsp; Edit</i>
                        </button></a>

                    <form method="POST" action="{{ route('user-slider.destroy', $userslider->id) }}"
                        accept-charset="UTF-8" style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm delete-confirm" title="Delete User Slider"><i
                                class="fas fa-trash" aria-hidden="true"></i>
                            &nbsp; Hapus</button>
                    </form>
                </div>
                <br />
                <br />

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $userslider->id }}</td>
                            </tr>

                            <tr>
                                <th> Menu Id </th>
                                <td> {{ $userslider->menu_id }} </td>
                            </tr>
                            <tr>
                                <th> Page Id </th>
                                <td> {{ $userslider->page_id }} </td>
                            </tr>
                            <tr>
                                <th> Foto </th>
                                <td>
                                    <center>
                                        <img id='img-upload' style="width: 40%"
                                            src="{{ asset('img/userslider/'.$userslider->foto) }}" /> <br><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <th> Thumbnail </th>
                                <td>
                                    <center>
                                        <img id='img-upload' style="width: 40%"
                                            src="{{ asset('img/userslider/'.$userslider->thumbnail) }}" /> <br><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <th> Caption </th>
                                <td> {{ $userslider->caption }} </td>
                            </tr>
                            <tr>
                                <th> SliderTextType </th>
                                <td> {{ $userslider->sliderTextType }} </td>
                            </tr>
                            <tr>
                                <th> SliderType </th>
                                <td> {{ $userslider->sliderType }} </td>
                            </tr>
                            <tr>
                                <th> Order </th>
                                <td> {{ $userslider->order }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')

@endsection

@section('js')
<script>
    $('.delete-confirm').on('click', function (event) {
        var form =  $(this).closest("form");
        var name = $(this).data("name");
        event.preventDefault();
        swal({
            title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
            text: "Jika dihapus data tidak akan ditampilkan lagi",
            icon: "warning",
            buttons: ["Tidak", "Ya!"],
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            form.submit();
        }
        });
    });
</script>
@endsection
