<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($userslider->user_id) ? $userslider->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($userslider->username) ? $userslider->username : auth()->user()->username }}" required>

<div class="form-group {{ $errors->has('menu_id') ? 'has-error' : ''}}">
    <label for="menu_id" class="control-label">{{ 'Pilih Menu' }}</label>

    <select name="menu_id" class="form-control" id="menu_id" required>
        @foreach ($menu as $value)
        <option value="{{ $value->id }}" {{ (isset($userslider->menu_id) && $userslider->menu_id ==
            $value->id) ? 'selected' : ''}}> {{ $value->username }} => {{ $value->title }}
        </option>
        @endforeach
    </select>
    <div class="invalid-feedback">
        Menu Wajib Diisi
    </div>

    {!! $errors->first('menu_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('page_id') ? 'has-error' : ''}}">
    <label for="page_id" class="control-label">{{ 'Pilih Page' }}</label>

    <select name="page_id" class="form-control" id="page_id" required>
        @foreach ($page as $value)
        <option value="{{ $value->id }}" {{ (isset($userslider->page_id) && $userslider->page_id ==
            $value->id) ? 'selected' : ''}}> {{ $value->username }} => {{ $value->title }}
        </option>
        @endforeach
    </select>

    <div class="invalid-feedback">
        Page Wajib Diisi
    </div>

    {!! $errors->first('page_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('foto') ? 'has-error' : ''}}">
    <label for="foto" class="control-label">{{ 'Pilih Foto Slider' }}</label>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file">
                Pilih Foto Slider <input type="file" name="foto" id="foto">
            </span>
        </span>
        <input type="text" class="form-control" value="{{ isset($userslider->foto) ? $userslider->foto : ''}}" readonly>
    </div>
    <br>
    <center>
        <img id='img-upload' class="img-fluid rounded"
            src="@if(isset($userslider->foto)){{ asset('img/userslider/'.$userslider->foto) }}@else{{ asset('img/userslider/slider.jpg') }}@endif" />
    </center>

    {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('caption') ? 'has-error' : ''}}">
    <label for="caption" class="control-label">{{ 'Text Foto' }}</label>
    <input class="form-control" name="caption" type="text" id="caption"
        value="{{ isset($userslider->caption) ? $userslider->caption : ''}}" required>
    <div class="invalid-feedback">
        caption Wajib Diisi
    </div>

    {!! $errors->first('caption', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('sliderTextType') ? 'has-error' : ''}}">
    <label for="sliderTextType" class="control-label">{{ 'Animasi Text' }}</label>

    <select name="sliderTextType" id="sliderTextType" class="form-control select" required>
        <option value="fadeInDown" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'fadeInDown') ? 'selected' : ''}}> fadeInDown
        </option>
        <option value="fadeInUp" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'fadeInUp') ? 'selected' : ''}}> fadeInUp
        </option>
        <option value="zoomIn" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'zoomIn') ? 'selected' : ''}}> zoomIn
        </option>
        <option value="rollIn" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'rollIn') ? 'selected' : ''}}> rollIn
        </option>
        <option value="lightSpeedIn" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'lightSpeedIn') ? 'selected' : ''}}> lightSpeedIn
        </option>

        <option value="slideInUp" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'slideInUp') ? 'selected' : ''}}> slideInUp
        </option>

        <option value="rotateInDownLeft" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'rotateInDownLeft') ? 'selected' : ''}}> rotateInDownLeft
        </option>

        <option value="rotateInUpRight" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'rotateInUpRight') ? 'selected' : ''}}> rotateInUpRight
        </option>
        <option value="rotateIn" {{ (isset($userslider->sliderTextType) && $userslider->sliderTextType ==
            'rotateIn') ? 'selected' : ''}}> rotateIn
        </option>
    </select>

    <div class="invalid-feedback">
        sliderTextType Wajib Diisi
    </div>

    {!! $errors->first('sliderTextType', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('sliderType') ? 'has-error' : ''}}">
    <label for="sliderType" class="control-label">{{ 'sliderType' }}</label>
    <input class="form-control" name="sliderType" type="text" id="sliderType"
        value="{{ isset($userslider->sliderType) ? $userslider->sliderType : ''}}">
    <div class="invalid-feedback">
        sliderType Wajib Diisi
    </div>

    {!! $errors->first('sliderType', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('order') ? 'has-error' : ''}}">
    <label for="order" class="control-label">{{ 'Urutan Slide' }}</label>
    <input class="form-control" name="order" type="text" id="order"
        value="{{ isset($userslider->order) ? $userslider->order : ''}}" required>
    <div class="invalid-feedback">
        order Wajib Diisi
    </div>

    {!! $errors->first('order', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('link') ? 'has-error' : ''}}">
    <label for="link" class="control-label">{{ 'Link Text ( Jika Ada )' }}</label>
    <input class="form-control" name="link" type="text" id="link"
        value="{{ isset($userslider->link) ? $userslider->link : ''}}">
    <div class="invalid-feedback">
        Link Wajib Diisi
    </div>

    {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
