<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($userstore->user_id) ? $userstore->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($userstore->username) ? $userstore->username : auth()->user()->username }}" required>

<div class="form-group {{ $errors->has('menu_id') ? 'has-error' : ''}}">
    <label for="menu_id" class="control-label">{{ 'Pilih Menu' }}</label>
    <span class="required-label">*</span>

    <select name="menu_id" class="form-control" id="menu_id" required>
        @foreach ($menu as $value)
        <option value="{{ $value->id }}" {{ (isset($userstore->menu_id) && $userstore->menu_id ==
            $value->id) ? 'selected' : ''}}> {{ $value->title }}
        </option>
        @endforeach
    </select>

    <div class="invalid-feedback">
        Menu Wajib Diisi
    </div>

    {!! $errors->first('menu_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('page_id') ? 'has-error' : ''}}">
    <label for="page_id" class="control-label">{{ 'Pilih Page' }}</label>
    <span class="required-label">*</span>
    <select name="page_id" class="form-control" id="page_id" required>
        @foreach ($page as $value)
        <option value="{{ $value->id }}" {{ (isset($userstore->page_id) && $userstore->page_id ==
            $value->id) ? 'selected' : ''}}> {{ $value->title }}
        </option>
        @endforeach
    </select>

    <div class="invalid-feedback">
        Page Wajib Diisi
    </div>

    {!! $errors->first('page_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Nama Produk' }}</label>
    <span class="required-label">*</span>
    <input class="form-control" name="title" type="text" id="title"
        value="{{ isset($userstore->title) ? $userstore->title : ''}}" placeholder="Misal : Produk A . . ." required>
    <div class="invalid-feedback">
        Judul Wajib Diisi
    </div>

    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('detail') ? 'has-error' : ''}}">
    <label for="detail" class="control-label">{{ 'Detail Singkat' }}</label>
    <textarea class="form-control" rows="5" name="detail" type="text" id="detail"
        placeholder="Isikan Perihal Detail Produk . . .">{{ isset($userstore->detail) ? $userstore->detail : ''}}</textarea>

    <div class="invalid-feedback">
        Detail 1 Wajib Diisi
    </div>

    {!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('foto') ? 'has-error' : ''}}">
    <label for="foto" class="control-label">{{ 'Foto Produk' }}</label>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file" data-toggle="tooltip" data-placement="top"
                title="Resolusi Rekomendasi: 600x600">
                Pilih Foto <input type="file" name="foto" id="foto">
            </span>
        </span>
        <input type="text" class="form-control" value="{{ isset($userstore->foto) ? $userstore->foto : ''}}" readonly
            required>
    </div>
    <br>
    <center>
        <img id='img-upload' class="img-fluid rounded"
            src="@if(isset($userstore->foto)){{ asset('img/userstore/'.$userstore->foto) }}@else{{ asset('img/userstore/product.jpg') }}@endif" />
    </center>


    {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
