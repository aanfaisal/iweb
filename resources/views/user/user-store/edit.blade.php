@extends('layouts.main.app')
@section('title')
Edit Data User Store #{{ $usertext->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <a href="{{ route('user-store.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                            class="fas fa-arrow-left" aria-hidden="true"></i>
                        Kembali</button></a>
                <br />
                <br />

                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif

                <form method="POST" action="{{ route('user-store.update', $usertext->id) }}" accept-charset="UTF-8"
                    class="form-horizontal" id="needs-validation" enctype="multipart/form-data" novalidate>
                    @csrf
                    @method('PUT')

                    @include ('user.user-store.form', ['formMode' => 'edit'])

                </form>


            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.min.css') }}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@1.2.4/themes/blue/pace-theme-corner-indicator.css">

<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }

    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload {
        width: 50%;
    }

    #img-uploads {
        width: 50%;
    }
</style>
@endsection

@section('js')
<!-- Select2 -->
<script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>
<!-- Summernote JS -->
<script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
<!-- Pace JS -->
<script src="https://cdn.jsdelivr.net/npm/pace-js@1.2.4/pace.min.js"></script>

<!-- Page specific script -->
<script>
    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#foto").change(function(){
            readURL(this);
        });
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".btn-success").click(function(){
            var html = $(".clone").html();
            $(".increment").after(html);
        });
        $("body").on("click",".btn-danger",function(){
            $(this).parents(".control-group").remove();
        });
    });
</script>
<!-- Page specific script -->
<script>
    (function() {
    'use strict';
        window.addEventListener('load', function() {
            var form = document.getElementById('needs-validation');
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        }, false);
    })();

$(function () {

    $('#menu_id').select2({
        theme: 'bootstrap4'
    })

    $('#page_id').select2({
        theme: 'bootstrap4'
    })

});

</script>

@endsection
