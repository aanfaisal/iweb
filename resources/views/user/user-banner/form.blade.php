<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($userbanner->user_id) ? $userbanner->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($userbanner->username) ? $userbanner->username : auth()->user()->username }}" required>

<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Nama Banner' }}</label>
    <input class="form-control" name="title" type="text" id="title"
        value="{{ isset($userbanner->title) ? $userbanner->title : ''}}" placeholder="Isikan Nama Banner . . .">
    <div class="invalid-feedback">
        Nama Banner Wajib Diisi
    </div>

    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('order') ? 'has-error' : ''}}">
    <label for="order" class="control-label">{{ 'Urutan' }}</label>
    <input class="form-control" name="order" type="number" id="order"
        value="{{ isset($userbanner->order) ? $userbanner->order : ''}}" placeholder="Isikan Urutan Banner . . .">
    <div class="invalid-feedback">
        Urutan Wajib Diisi
    </div>

    {!! $errors->first('order', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('foto') ? 'has-error' : ''}}">
    <label for="foto" class="control-label">{{ 'Pilih Foto' }}</label>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file">
                Pilih Foto <input type="file" name="foto" id="foto">
            </span>
        </span>
        <input type="text" class="form-control" value="{{ isset($userbanner->foto) ? $userbanner->foto : ''}}" readonly>
    </div>
    <br>
    <center>
        <img id='img-upload' class="img-fluid rounded"
            src="@if(isset($userbanner->foto)){{ asset('img/userbanner/'.$userbanner->foto) }}@else{{ asset('img/userbanner/banner.jpg') }}@endif" />
    </center>


    {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('link') ? 'has-error' : ''}}">
    <label for="link" class="control-label">{{ 'Link Video / Web' }}</label>
    <input class="form-control" name="link" type="text" id="link"
        value="{{ isset($userbanner->link) ? $userbanner->link : ''}}" placeholder="https:// . . ." required>
    <div class="invalid-feedback">
        Link Wajib Diisi
    </div>

    {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
