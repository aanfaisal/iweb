<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($userfooter->user_id) ? $userfooter->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($userfooter->username) ? $userfooter->username : auth()->user()->username }}" required>

<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Judul' }}</label>
    <input class="form-control" name="title" type="text" id="title"
        value="{{ isset($userfooter->title) ? $userfooter->title : ''}}" placeholder="Cth : ALAMAT, TENTANG KAMI">
    <div class="invalid-feedback">
        Judul Wajib Diisi
    </div>

    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('detail') ? 'has-error' : ''}}">
    <label for="detail" class="control-label">{{ 'Detail Footer' }}</label>
    <textarea class="form-control" rows="3" name="detail" type="text" id="detail"
        required>{{ isset($userfooter->detail) ? $userfooter->detail : ''}}</textarea>

    <div class="invalid-feedback">
        Detail Wajib Diisi
    </div>

    {!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
</div>

<hr>

<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
