<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($usermenu->user_id) ? $usermenu->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($usermenu->username) ? $usermenu->username : auth()->user()->username }}" required>


<div class="form-group {{ $errors->has('icon') ? 'has-error' : ''}}">
    <label for="icon" class="control-label">{{ 'Icon' }}</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <button class="btn btn-primary btn-border" type="button" id="GetIconPicker"
                data-iconpicker-input="input#icon" data-iconpicker-preview="i#IconPreview">Pilih Icon</button>
        </div>

        <input class="form-control" name="icon" type="text" id="icon"
            value="{{ isset($usermenu->icon) ? $usermenu->icon : ''}}" autocomplete="off" spellcheck="false" readonly>
    </div>
    {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group get-and-preview">
    <div class="icon-preview" data-toggle="tooltip" title="" data-original-title="Preview of selected Icon">
        <i id="IconPreview" class="{{ isset($usermenu->icon) ? $usermenu->icon : 'fab fa-github fa-2x' }}"></i>
    </div>
</div>

<br>
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Nama Menu' }}</label>
    <input class="form-control" name="title" type="`" id="title"
        value="{{ isset($usermenu->title) ? $usermenu->title : ''}}" placeholder="Isikan Nama Menu . . ." required>
    <div class="invalid-feedback">
        Nama Menu Wajib Diisi
    </div>

    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('detail') ? 'has-error' : ''}}">
    <label for="detail" class="control-label">{{ 'Keterangan Menu' }}</label>
    <textarea class="form-control" rows="5" name="detail" type="text" id="detail"
        placeholder="Isikan Perihal Detail Menu . . ."
        required>{{ isset($usermenu->detail) ? $usermenu->detail : ''}}</textarea>

    <div class="invalid-feedback">
        Detail Wajib Diisi
    </div>

    {!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('typepage') ? 'has-error' : ''}}">
    <label for="typepage" class="control-label">{{ 'Tipe Halaman' }}</label>
    <select name="typepage" id="typepage" class="form-control select" required>
        <option value="homepage" {{ (isset($usermenu->typepage) && $usermenu->typepage == 'homepage') ? 'selected' :
            ''}}>Home Page
        </option>
        <option value="promopage" {{ (isset($usermenu->typepage) && $usermenu->typepage == 'promopage') ? 'selected' :
            ''}}> Promo Page
        </option>
        <option value="postpage" {{ (isset($usermenu->typepage) && $usermenu->typepage == 'postpage') ? 'selected' :
            ''}}> Post Page
        </option>
        <option value="pdfpage" {{ (isset($usermenu->typepage) && $usermenu->typepage == 'pdfpage') ? 'selected' :
            ''}} > PDF Page
        </option>

    </select>
    <div class="invalid-feedback">
        Tipe Page Wajib Diisi
    </div>

    {!! $errors->first('typepage', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('order') ? 'has-error' : ''}}">
    <label for="order" class="control-label">{{ 'Urutan Menu' }}</label>
    <input class="form-control" name="order" type="number" id="order"
        value="{{ isset($usermenu->order) ? $usermenu->order : ''}}" min="0" placeholder="Isikan Urutan Menu . . .">
    <div class="invalid-feedback">
        Urutan Wajib Diisi
    </div>

    {!! $errors->first('order', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
