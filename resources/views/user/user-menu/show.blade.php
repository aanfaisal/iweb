@extends('layouts.main.app')
@section('title')
Lihat User Menu #{{ $usermenu->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <div class="btn-group" role="group">
                    <a href="{{ route('user-menu.index') }}" title="Kembali"><button class="btn btn-warning btn-sm"><i
                                class="fas fa-arrow-left" aria-hidden="true"></i>
                            Kembali</button></a>

                    <a href="{{ route('user-menu.edit', $usermenu->id) }}" title="Edit User Menu"><button
                            class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true">&nbsp; Edit</i>
                        </button></a>

                    <form method="POST" action="{{ route('user-menu.destroy', $usermenu->id) }}" accept-charset="UTF-8"
                        style="display:inline">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger btn-sm delete-confirm" title="Delete User Menu"><i
                                class="fas fa-trash" aria-hidden="true"></i>
                            &nbsp; Hapus</button>
                    </form>
                </div>
                <br />
                <br />

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th> Username </th>
                                <td> {{ $usermenu->username }} </td>
                            </tr>
                            <tr>
                                <th> Icon </th>
                                <td>
                                    <i class="{{ $usermenu->icon }}"></i>
                                </td>
                            </tr>
                            <tr>
                                <th> Nama Menu </th>
                                <td> {{ $usermenu->title }} </td>
                            </tr>
                            <tr>
                                <th> Detail </th>
                                <td> {{ $usermenu->detail }} </td>
                            </tr>
                            <tr>
                                <th> Tipe Halaman : </th>
                                <td> {{ $usermenu->typepage }} </td>
                            </tr>
                            <tr>
                                <th> Urutan : </th>
                                <td> {{ $usermenu->order }} </td>
                            </tr>
                            <tr>
                                <th> Link </th>
                                <td> {{ $usermenu->link }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')

@endsection

@section('js')
<script>
    $('.delete-confirm').on('click', function (event) {
        var form =  $(this).closest("form");
        var name = $(this).data("name");
        event.preventDefault();
        swal({
            title: `Apakah Anda Yakin Untuk Menghapus Data Ini ?`,
            text: "Jika dihapus data tidak akan ditampilkan lagi",
            icon: "warning",
            buttons: ["Tidak", "Ya!"],
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            form.submit();
        }
        });
    });
</script>
@endsection
