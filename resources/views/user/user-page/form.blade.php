<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($userpage->user_id) ? $userpage->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($userpage->username) ? $userpage->username : auth()->user()->username }}" required>


<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Nama' }}</label>
    <input class="form-control" name="title" type="text" id="title" placeholder="Isikan Nama Page Ini . . ."
        value="{{ isset($userpage->title) ? $userpage->title : ''}}" required>
    <div class="invalid-feedback">
        Nama Halaman Wajib Diisi
    </div>

    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('detail') ? 'has-error' : ''}}">
    <label for="detail" class="control-label">{{ 'Detail Page' }}</label>
    <textarea class="form-control" rows="5" name="detail" type="text" id="detail"
        placeholder="Isikan Perihal Detail Page Ini . . ."
        required>{{ isset($userpage->detail) ? $userpage->detail : ''}}</textarea>

    <div class="invalid-feedback">
        Detail Wajib Diisi
    </div>

    {!! $errors->first('keterangan', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('menu_id') ? 'has-error' : ''}}">
    <label for="menu_id" class="control-label">{{ 'Pilih Menu' }}</label>

    <select name="menu_id" class="form-control" id="menu_id" required>
        @foreach ($menu as $value)
        <option value="{{ $value->id }}" {{ (isset($userpage->menu_id) && $userpage->menu_id ==
            $value->id) ? 'selected' : ''}}> {{ $value->username }} => {{ $value->title }}
        </option>
        @endforeach
    </select>

    <div class="invalid-feedback">
        Menu Wajib Dipilih
    </div>

    {!! $errors->first('menu_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('foto') ? 'has-error' : ''}}">
    <label for="foto" class="control-label">{{ 'Pilih Foto Default' }}</label>
    <div class="input-group">
        <span class="input-group-btn">
            <span class="btn btn-primary btn-file">
                Pilih Foto Default <input type="file" name="foto" id="foto">
            </span>
        </span>
        <input type="text" class="form-control" value="{{ isset($userpage->foto) ? $userpage->foto : ''}}" readonly>
    </div>
    <br>
    <center>
        <img id='img-upload' class="img-fluid rounded"
            src="@if(isset($userpage->foto)){{ asset('img/pageuser/'.$userpage->foto) }}@else{{ asset('img/pageuser/page.jpg') }}@endif" />
    </center>


    {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
