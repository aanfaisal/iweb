<input class="form-control" name="user_id" type="hidden" id="user_id"
    value="{{ isset($usersocialmedia->user_id) ? $usersocialmedia->user_id : auth()->user()->id }}" required>

<input class="form-control" name="username" type="hidden" id="username"
    value="{{ isset($usersocialmedia->username) ? $usersocialmedia->username : auth()->user()->username }}" required>

<div class="form-group {{ $errors->has('icon') ? 'has-error' : ''}}">
    <label for="icon" class="control-label">{{ 'Icon' }}</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <button class="btn btn-primary btn-border" type="button" id="GetIconPicker"
                data-iconpicker-input="input#icon" data-iconpicker-preview="i#IconPreview">Pilih Icon</button>
        </div>

        <input class="form-control" name="icon" type="text" id="icon"
            value="{{ isset($usersocialmedia->icon) ? $usersocialmedia->icon : ''}}" autocomplete="off"
            spellcheck="false" readonly>
    </div>
    {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group get-and-preview">
    <div class="icon-preview" data-toggle="tooltip" title="" data-original-title="Preview of selected Icon">
        <i id="IconPreview"
            class="{{ isset($usersocialmedia->icon) ? $usersocialmedia->icon : 'fab fa-github fa-2x' }}"></i>
    </div>
</div>

<br><br><br>
<hr>
<div class="form-group {{ $errors->has('detail') ? 'has-error' : ''}}">
    <label for="detail" class="control-label">{{ 'Detail' }}</label>
    <textarea class="form-control" rows="3" name="detail" type="text" id="detail"
        placeholder="Isikan Link Social Mediamu . . ."
        required>{{ isset($usersocialmedia->detail) ? $usersocialmedia->detail : ''}}</textarea>

    <div class="invalid-feedback">
        Detail Link Wajib Diisi
    </div>

    {!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
</div>

<hr>
<div class="form-group">
    <input class="btn btn-primary btn-block" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Simpan' }}">
</div>
