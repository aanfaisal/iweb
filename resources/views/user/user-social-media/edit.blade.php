@extends('layouts.main.app')
@section('title')
Edit UserSocialMedia #{{ $usersocialmedia->id }}
@endsection

@section('content')
<div class="page-inner">
    <div class="container-fluid">
        <!-- CONTENT -->
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">@yield('title')</h3>

            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <a href="{{ route('user-social-media.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                            class="fas fa-arrow-left" aria-hidden="true"></i>
                        Kembali</button></a>
                <br />
                <br />

                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif

                <form method="POST" action="{{ route('user-social-media.update', $usersocialmedia->id) }}"
                    accept-charset="UTF-8" class="form-horizontal" id="needs-validation" enctype="multipart/form-data"
                    novalidate>
                    @csrf
                    @method('PUT')

                    @include ('user.user-social-media.form', ['formMode' => 'edit'])

                </form>


            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/IconPicker/fontawesome-5.11.2/css/all.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/IconPicker/iconpicker-1.5.0.css') }}" />

<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }

    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload {
        width: 70%;
    }

    .get-and-preview {
        width: 100%;
        margin: 0 0 10px;
        float: left;
        clear: both;
    }

    .get-and-preview .icon-preview {
        float: left;
        width: 55px;
        height: 55px;
        margin: 0 15px 0 0;
        border-radius: 5px;
        background: #fff;
        text-align: center;
        font-size: 30px;
        line-height: 55px;
        color: #1e1e1e;
    }

    .get-and-preview button {
        position: relative;
        transition: all .2s ease-in-out;
        cursor: pointer;
        float: left;
        color: #fff;
        background: #a94158;
        background: linear-gradient(to right bottom, #a94158, #99394e, #883144, #78293a, #692131);
        box-shadow: 0 2px 14px -6px #000;
        padding: 12px 18px;
        font-size: 16px;
        line-height: 16px;
        border: none;
        border-radius: 20px;
        margin: 7px 0 0;
    }

    .get-and-preview button:hover {
        transform: scale(.97);
        box-shadow: 0 0 18px -6px #000;
    }


    /* Form Area off */
</style>
@endsection

@section('js')
<!-- Select2 -->
<script src="{{ asset('vendor/select2/js/select2.full.min.js') }}"></script>
<!-- Icon Picker -->
<script src="{{ asset('vendor/IconPicker/iconpicker-1.5.0.js') }}"></script>

<!-- Page specific script -->
<script>
    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#foto").change(function(){
            readURL(this);
        });
    });
</script>
<!-- Page specific script -->
<script>
    (function() {
    'use strict';
        window.addEventListener('load', function() {
            var form = document.getElementById('needs-validation');
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        }, false);
    })();

$(function () {

    IconPicker.Init({
        // Required: You have to set the path of IconPicker JSON file to "jsonUrl" option. e.g. '/content/plugins/IconPicker/dist/iconpicker-1.5.0.json'
        jsonUrl: '{{ asset('vendor/IconPicker/iconpicker-1.5.0.json') }}',

        // Optional: Change the buttons or search placeholder text according to the language.
        searchPlaceholder: 'Search Icon',
        showAllButton: 'Show All',
        cancelButton: 'Cancel',
        noResultsFound: 'No results found.', // v1.5.0 and the next versionsborderRadius: '20px',
        // v1.5.0 and the next versions
    });

    IconPicker.Run('#GetIconPicker');

    //Initialize Select2 Elements
    $('#typepage').select2({
        theme: 'bootstrap4'
    })
});
</script>
@endsection
