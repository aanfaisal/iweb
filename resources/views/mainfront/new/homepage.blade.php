@extends('layouts.userfront.app')

@section('title')
IWEB
@endsection

@section('favicon')

<link rel="icon" href="{{ asset('img/favicon/favicon.ico') }}" type="image/x-icon" />
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">

@endsection


@section('menu-desktop')
<!-- Header -->
<header class="header-v1">
    <!-- Header desktop -->
    <div class="container-menu-desktop trans-03">
        <div class="wrap-menu-desktop">
            <nav class="limiter-menu-desktop p-l-45">

                <!-- Logo desktop -->
                <a href="{{ env('APP_URL') }}" class="logo">
                    <img src="{{ asset('img/logo.png') }}" alt="LOGO">
                </a>


            </nav>
        </div>
    </div>


</header>
@endsection


@section('slider')

@if((new \Jenssegers\Agent\Agent())->isDesktop())

<!-- Slider -->
<section class="section-slide {{ $warna }}">
    <div class="wrap-slick1 rs1-slick1">
        <div class="slick1">

            @foreach ($settinghomepage as $itemslider )

            <div class="item-slick1 {{ $warna }}"
                style="background-image: url(@if(is_null($itemslider->sliderbackground)){{ asset('istore/images/slide-01.jpg') }}@else{{ asset('img/mainslider/'.$itemslider->sliderbackground) }}@endif);"
                data-thumb="@if(is_null($itemslider->sliderbackground)){{ asset('istore/images/thumb-01.jpg') }}@else{{ asset('images/img/'.$itemslider->sliderbackground) }}@endif"
                data-caption="Slider">
                <div class="container h-full">

                    <div class="flex-col-c-m h-full p-t-100 p-b-60 respon5">

                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1000">
                            <a href="{{ route('register') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                REGISTER
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1500">
                            <a href="{{ route('login') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                LOGIN
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="2000">
                            <a href="{{ route('tentang.index') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                ABOUT US
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="2500">
                            <a href="{{ route('webstore.index') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                WEB STORE
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="3000">
                            <a href="{{ route('testimoni.index') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                TESTIMONI
                            </a>
                        </div>




                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>


@elseif((new \Jenssegers\Agent\Agent())->isMobile())

<!-- Slider MOBILE -->
<section class="section-slide {{ $warna }}">
    <div class="wrap-slick1 rs1-slick1">
        <div class="slick1">

            @foreach ($settinghomepage as $itemslider)

            <div class="item-slick1 {{ $warna }}"
                style="background-image: url(@if(is_null($itemslider->slidermobile)){{ asset('istore/images/slide-01.jpg') }}@else{{ asset('img/mainslider/'.$itemslider->slidermobile) }}@endif);"
                data-thumb="@if(is_null($itemslider->slidermobile)){{ asset('istore/images/thumb-01.jpg') }}@else{{ asset('images/img/'.$itemslider->slidermobile) }}@endif"
                data-caption="Slider">

                <div class="container h-full">

                    <div class="logo-mobiles">
                        <a href="{{ env('APP_URL') }}" class="logo">
                            <img src="@if(is_null($settingumum)){{ asset('img/logo.png') }}@else{{ asset('img/logouser/'.$settingumum->logo) }}@endif "
                                alt="LOGO">
                        </a>
                    </div>


                    <div class="flex-col-c-m p-t-240 p-b-60">

                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1000">
                            <a href="{{ route('register') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                REGISTER
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1500">
                            <a href="{{ route('login') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                LOGIN
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="2000">
                            <a href="{{ route('tentang.index') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                ABOUT US
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="2500">
                            <a href="{{ route('webstore.index') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                WEB STORE
                            </a>
                        </div>
                        <br>
                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="3000">
                            <a href="{{ route('testimoni.index') }}"
                                class="flex-c-m stext-101 cl0 size-115 bg1 bor8 hov-btn2 p-lr-15 trans-04">
                                TESTIMONI
                            </a>
                        </div>

                        <div class="fixed">
                            <div class="p-t-27">
                                <a href="#" class="fs-18 cl7 p-t-120 p-b-40 hov-cl1 trans-04 m-r-1">
                                    Cv. Pahlawan Situstama Nusantara
                                </a>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
@endif

@endsection


@section('footer')

@if((new \Jenssegers\Agent\Agent())->isDesktop())
<footer class="{{ $warna }} p-b-32">
    <div class="p-t-0">

        <div class="fixed">
            <div class="p-t-27 txt-center">
                <a href="#" class="fs-18 cl7 p-t-120 p-b-40 hov-cl1 trans-04 m-r-1">
                    Cv. Pahlawan Situstama Nusantara
                </a>
            </div>
        </div>

    </div>
</footer>
@endif
@endsection

@section('css')
<style>
    .overlay {
        height: 100%;
        width: 100%;
        display: none;
        position: fixed;
        z-index: 9999;
        top: 0;
        left: 0;
        background-color: rgb(0, 0, 0);
        background-color: rgba(0, 0, 0, 0.9);
    }

    .overlay-content {
        position: relative;
        top: 25%;
        width: 100%;
        text-align: center;
        margin-top: 30px;
    }

    .overlay a {
        padding: 8px;
        text-decoration: none;
        font-size: 36px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .overlay a:hover,
    .overlay a:focus {
        color: #f1f1f1;
    }

    .overlay .closebtn {
        position: absolute;
        top: 65px;
        right: 45px;
        font-size: 60px;
    }

    @media screen and (max-height: 450px) {
        .overlay a {
            font-size: 20px
        }

        .overlay .closebtn {
            font-size: 40px;
            top: 55px;
            right: 35px;
        }
    }

    .fixed {
        position: absolute;
        width: 100%;
        display: flex;
        justify-content: center;
        bottom: 10px;
    }

    .logo-mobiles {
        position: absolute;
        width: 151px;
        display: flex;
        justify-content: right;
        bottom: calc(100vh - 90px);
        padding-left: 15px;
    }

    @media screen and (orientation:landscape) {
        .logo-mobiles {
            position: absolute;
            width: 120px;
            display: flex;
            justify-content: right;
            top: 34px;
            padding-left: 15px;
        }
    }
</style>

@endsection

@section('js')

<script>
    function openNav() {
        document.getElementById("myNav").style.display = "block";
    }

    function closeNav() {
        document.getElementById("myNav").style.display = "none";
    }
</script>

@endsection
