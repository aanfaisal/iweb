@extends('layouts.mainfront.main')

@section('title')
IWEB Promotions Website
@endsection

@section('description')
IWEB Promotions Website
@endsection

@section('keywords')
IWEB, Promotions Website, Bikin Website, Website Murah
@endsection


@section('content')


<!-- testimonial -->
<section class=" standalone text-center">
    <div class="container">
        <div class="standalone-title margin-100px-bottom sm-margin-50px-bottom xs-margin-20px-bottom">
            <h5
                class="text-uppercase alt-font text-extra-dark-gray margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">
                Apa Kata <span class="text-blue"> Mereka ? </span>
            </h5>
            <div class="testimonials">
                <div class="container">
                    <div class="row">
                        @if($testimoni->isNotEmpty())
                        @foreach ($testimoni as $item)
                        <div class="col-md-4 col-centered">

                            <figure class="snip1390">
                                <img src="@if(is_null($item->foto)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/testimoni/'.$item->foto) }}@endif"
                                    alt="profile-sample3" class="profile" />
                                <figcaption>
                                    <h2>{{ $item->judul }}</h2>
                                    <h4></h4>
                                    <blockquote>{!! $item->keterangan !!}</blockquote>
                                </figcaption>
                            </figure>
                        </div>
                        @endforeach
                        @else
                        <p>Silahkan Input Data Testimoni </p>
                        @endif
                    </div>
                </div>

            </div>
            </p>
        </div>
    </div>
</section>
<!-- testimonial end -->


@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('istore/vendor/slick/slick.css') }}">

<style>
    /*----Testimonial-----*/

    @import url(https://fonts.googleapis.com/css?family=Roboto:300,400);

    figure.snip1390 {
        font-family: "Roboto", Arial, sans-serif;
        position: relative;
        overflow: hidden;
        margin: 10px;
        min-width: 230px;
        max-width: 315px;
        width: 100%;
        color: #000000;
        text-align: center;
        font-size: 16px;
        background-color: #2c3447;
        padding: 30px;
        background-image: linear-gradient(-25deg,
                rgba(0, 0, 0, 0.2) 0%,
                rgba(255, 255, 255, 0.1) 100%);
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    figure.snip1390 *,
    figure.snip1390 *:before,
    figure.snip1390 *:after {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-transition: all 0.35s ease;
        transition: all 0.35s ease;
    }

    figure.snip1390 figcaption {
        width: 100%;
    }

    figure.snip1390 h2,
    figure.snip1390 h4,
    figure.snip1390 blockquote {
        margin: 0;
    }

    figure.snip1390 h2,
    figure.snip1390 h4 {
        font-weight: 200;
    }

    figure.snip1390 h2 {
        color: #ffffff;
    }

    figure.snip1390 h4 {
        color: #a6a6a6;
    }

    figure.snip1390 blockquote {
        font-size: 1em;
        padding: 45px 20px 40px 50px;
        margin-top: 30px;
        background-color: #ffffff;
        border-radius: 5px;
        box-shadow: inset -1.4px -1.4px 2px rgba(0, 0, 0, 0.3);
        text-align: left;
        position: relative;
    }

    figure.snip1390 blockquote:before {
        font-family: "FontAwesome";
        content: "\201C";
        position: absolute;
        font-size: 70px;
        opacity: 0.25;
        font-style: normal;
        top: 0px;
        left: 20px;
    }

    figure.snip1390 .profile {
        width: 100px;
        border-radius: 50%;
        display: inline-block;
        box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.4);
        margin-bottom: 10px;
        border: solid 5px #a6a57a;
    }


    /*------------------------------------------------------------------
    [ Slick2 ]*/
    .slick-slide {
        outline: none !important;
    }


    /*//////////////////////////////////////////////////////////////////
[ Slick1 ]*/
    .wrap-slick1 {
        border-radius: 10px;
        position: relative;
    }

    .item-slick1 {
        height: calc(100vh - 40px);
        border-radius: 10px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
    }

    .rs1-slick1 .item-slick1 {
        border-radius: 10px;
        height: calc(100vh - 84px);
    }

    .rs2-slick1 .item-slick1 {
        border-radius: 10px;
        height: 100vh;
    }

    @media (max-width: 991px) {
        .item-slick1 {
            border-radius: 10px;
            height: calc(100vh - 70px) !important;
        }
    }

    .arrow-slick1 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
        align-items: center;
        width: auto;
        height: auto;
        font-size: 80px;
        color: rgba(0, 0, 0, 0.3);
        position: absolute;
        opacity: 0;

        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);

        z-index: 200;
        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
    }

    .wrap-slick1:hover .arrow-slick1 {
        opacity: 1;
    }

    .arrow-slick1:hover {
        color: #7280e0;
    }

    .next-slick1 {
        right: 50px;
        left: auto;
    }

    .prev-slick1 {
        left: 50px;
        right: auto;
    }

    @media (max-width: 991px) {
        .next-slick1 {
            right: 15px;
        }

        .prev-slick1 {
            left: 15px;
        }
    }

    /*---------------------------------------------*/
    .rs2-slick1 .arrow-slick1 {
        color: rgba(255, 255, 255, 0.3);
    }

    .rs2-slick1 .arrow-slick1:hover {
        color: #7280e0;
    }

    .wrap-slick1-dots {
        position: absolute;
        width: 100%;
        left: 0;
        bottom: 60px;
    }

    .slick1-dots {
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
    }

    .slick1-dots li {
        max-width: 190px;
        position: relative;
        cursor: pointer;
        margin-right: 1px;
    }

    .slick1-dots li img {
        width: 100%;
    }

    .caption-dots-slick1 {
        font-family: Poppins-ExtraLight;
        font-size: 16px;
        line-height: 1.3;
        color: #fff;
        text-align: center;

        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
        align-items: center;

        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, 0.5);
        padding: 5px;

        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
        opacity: 0;
    }

    .slick1-dots li:hover .caption-dots-slick1 {
        opacity: 1;
    }

    .slick1-dots li.slick-active .caption-dots-slick1 {
        opacity: 1;
    }

    @media (max-width: 575px) {
        .caption-dots-slick1 {
            font-size: 13px;
        }

        .wrap-slick1-dots {
            bottom: 25px;
        }
    }

    /*------------------------------------------------------------------
    [ Slick4 ]*/
    .wrap-slick4 {
        position: relative;
    }

    .item-slick4 {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
    }

    .rs1-slick4 .item-slick4 {
        height: 210px;
    }

    .rs2-slick4 .item-slick4 {
        height: 210px;
    }

    @media (max-width: 991px) {
        .item-slick4 {
            width: 100%;
            height: 100%;
        }
    }

    .arrow-slick4 {
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
        align-items: center;
        width: auto;
        height: auto;
        font-size: 80px;
        color: rgba(0, 0, 0, 0.3);
        position: absolute;
        opacity: 0;

        top: 50%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);

        z-index: 200;
        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
    }

    .wrap-slick4:hover .arrow-slick4 {
        opacity: 1;
    }

    .arrow-slick4:hover {
        color: #7280e0;
    }

    .next-slick4 {
        right: 50px;
        left: auto;
    }

    .prev-slick4 {
        left: 50px;
        right: auto;
    }

    @media (max-width: 991px) {
        .next-slick4 {
            right: 15px;
        }

        .prev-slick4 {
            left: 15px;
        }
    }

    /*---------------------------------------------*/
    .rs2-slick4 .arrow-slick4 {
        color: rgba(255, 255, 255, 0.3);
    }

    .rs2-slick4 .arrow-slick4:hover {
        color: #7280e0;
    }

    .wrap-slick4-dots {
        position: absolute;
        width: 100%;
        left: 0;
        bottom: 60px;
    }

    .slick4-dots {
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
    }

    .slick4-dots li {
        max-width: 190px;
        position: relative;
        cursor: pointer;
        margin-right: 1px;
    }

    .slick4-dots li img {
        width: 100%;
    }

    .caption-dots-slick4 {
        font-family: Poppins-ExtraLight;
        font-size: 16px;
        line-height: 1.3;
        color: #fff;
        text-align: center;

        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
        align-items: center;

        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, 0.5);
        padding: 5px;

        -webkit-transition: all 0.4s;
        -o-transition: all 0.4s;
        -moz-transition: all 0.4s;
        transition: all 0.4s;
        opacity: 0;
    }

    .slick4-dots li:hover .caption-dots-slick4 {
        opacity: 1;
    }

    .slick4-dots li.slick-active .caption-dots-slick4 {
        opacity: 1;
    }

    @media (max-width: 575px) {
        .caption-dots-slick4 {
            font-size: 13px;
        }

        .wrap-slick4-dots {
            bottom: 25px;
        }
    }
</style>
@endsection

@section('js')
<script src="{{ asset('istore/vendor/slick/slick.min.js') }}"></script>
<script src="{{ asset('istore/js/slick-custom.js') }}"></script>

@endsection
