@extends('layouts.mainfront.main')

@section('title')
WEB Store | iWEB
@endsection

@section('description')
Web Store IWEB Promotions Website
@endsection

@section('keyword')
IWEB, Promotions Website, Bikin Website, Website Murah
@endsection

@section('slider')
@endsection

@section('content')

<section class="standalone_header gradient-background bg-grd-blue">
    <h2 class="display-none" aria-hidden="true">IWEB</h2>
    <div class="row">
        <div class="col-md-8 col-sm-12 center-col text-center margin-80px-top">
            <h5 class="text-uppercase alt-font text-white margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">
                WEB Store</h5>
            <p class="text-white margin-5px-bottom xs-padding-10px-left"></p>
            <div class="page_nav xs-padding-10px-left">
                <span class="text-white">Anda disini:</span> <a href="{{ route('homepage.index') }}"
                    class="text-black">Home</a> <span class="text-white"><i class="fa fa-angle-double-right"></i>
                    Web Store</span>
            </div>
        </div>
    </div>
</section>

<section class="how-it-work text-center bg-light-gray">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">IWEB</h2>
    <div class="container">

        <div class="how-one-container">

            @foreach ($webstore as $itemweb)

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <img src="@if(is_null($itemweb->image)){{ asset('img/!logged-user.jpg') }}@else{{ asset('img/imageuser/'.$itemweb->image) }}@endif"
                        class="rounded-circle">
                    <div class="details">
                        <div class="text-large text-extra-dark-gray margin-20px-bottom">{{ $itemweb->username }}</div>
                    </div>
                    <p id="info">
                        {{ $itemweb->webaddress }}
                    </p>
                    <p>
                        {{ substr($itemweb->keterangan, 80) }}
                    </p>
                </div>
            </div>

            @endforeach

        </div>


    </div>

    <div class="pagination-wrapper">
        {{ $webstore->links() }}
    </div>
</section>

@endsection

@section('css')
<style>
    @import url("https://fonts.googleapis.com/css2?family=Red+Rose:wght@300;400;600&display=swap");



    .card {
        width: 350px;
        height: 450px;
        display: flex;
        flex-direction: column;
        align-items: center;
        text-align: center;
        padding: 50px;
        border-radius: 10px;
        overflow: hidden;
        box-shadow: rgba(50, 50, 93, 0.25) 0px 6px 12px -2px,
            rgba(0, 0, 0, 0.3) 0px 3px 7px -3px;
    }

    .card img {
        width: 180px;
        height: 180px;
        border-radius: 50%;
        margin: 5px;
    }

    .card .details {
        margin: 10px;
    }

    .card .details h2 {
        font-weight: 600;
    }

    .card .details p {
        text-transform: uppercase;
        font-weight: 300;
    }
</style>

@endsection

@section('js')

@endsection
