@extends('layouts.mainfront.main')

@section('title')
Tentang Kami | iWEB
@endsection

@section('description')
Tentang IWEB Promotions Website
@endsection

@section('keyword')
IWEB, Promotions Website, Bikin Website, Website Murah
@endsection

@section('slider')
@endsection

@section('content')

<section class="standalone_header gradient-background bg-grd-blue">
    <h2 class="display-none" aria-hidden="true">IWEB</h2>
    <div class="row">
        <div class="col-md-8 col-sm-12 center-col text-center margin-80px-top">
            <h5 class="text-uppercase alt-font text-white margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">
                Tentang Kami</h5>
            <p class="text-white margin-5px-bottom xs-padding-10px-left"></p>
            <div class="page_nav xs-padding-10px-left">
                <span class="text-white">Anda disini:</span> <a href="{{ route('homepage.index') }}"
                    class="text-black">Home</a> <span class="text-white"><i class="fa fa-angle-double-right"></i>
                    Tentang Kami</span>
            </div>
        </div>
    </div>
</section>

<section class="standalone text-center">
    <h2 class="display-none" aria-hidden="true">IWEB</h2>
    <div class="container">
        <div class="standalone-title margin-100px-bottom sm-margin-50px-bottom xs-margin-20px-bottom">
            <h5
                class="text-uppercase alt-font text-extra-dark-gray margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">
                Tentang <span class="text-blue">Kami </span>
            </h5>
            <p class="margin-lr-auto md-width-100 xs-width-100 xs-margin-30px-bottom">
                @if(!empty($tentang))
                {!! $tentang->keterangan !!}
                @else
                Silahkan Input Data Tentang Kami di ADMINPANEL
                @endif
            </p>
        </div>
    </div>
</section>

@endsection

@section('css')

@endsection

@section('js')

@endsection
