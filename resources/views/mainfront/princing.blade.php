@extends('layouts.mainfront.main')

@section('title')
Biaya Kami | iWEB
@endsection

@section('description')
Biaya IWEB Promotions Website
@endsection

@section('keyword')
IWEB, Promotions Website, Bikin Website, Website Murah
@endsection

@section('content')

<section class="standalone_header gradient-background bg-grd-blue">
    <h2 class="display-none" aria-hidden="true">IWEB</h2>
    <div class="row">
        <div class="col-md-8 col-sm-12 center-col text-center margin-80px-top">
            <h5 class="text-uppercase alt-font text-white margin-20px-bottom font-weight-700 sm-width-100 xs-width-100">
                Biaya Kami</h5>
            <p class="text-white margin-5px-bottom xs-padding-10px-left"></p>
            <div class="page_nav xs-padding-10px-left">
                <span class="text-white">Anda disini:</span> <a href="{{ route('homepage.index') }}">Home</a> <span
                    class="text-white"><i class="fa fa-angle-double-right"></i> Biaya </span>
            </div>
        </div>
    </div>
</section>

<!-- Price -->
<section id="price" class="text-center btn-version bg-light-gray">
    <h2 class="display-none no-padding no-margin" aria-hidden="true">IWEB</h2>
    <div class="container">
        <div class="col-lg-9 col-md-10 col-sm-12 text-center center-col last-paragraph-no-margin">
            <div class="sec-title margin-70px-bottom">

                <p class="width-75 margin-lr-auto md-width-90 xs-width-100 xs-margin-30px-bottom">
                    Solusi dengan harga terjangkau untuk mengenalkan
                    Profile Anda / Bisnis Anda secara luas dengan ekstra
                    benefit yang Kami berikan.
                    Miliki iWebmu sekarang dan dapatkan penawaran
                    spesial dari Kami
                </p>
            </div>
        </div>
        <div class="row">

            @if($biaya->isNotEmpty())

            @foreach ($biaya as $item)
            <div class="col-md-3 col-sm-6">
                <div class="pricingTable {{ $item->warna }}">
                    <div class="pricingTable-header">

                        <div class="price">Rp. <s>{{ $item->hargaasli }} </s></div>
                        <br>
                        <div class="price-value"> Rp. {{ $item->hargadiskon }} </div>
                    </div>
                    <h3 class="heading">{{ $item->namapaket }}</h3>
                    <div class="pricing-content">
                        <ul>
                            @php $detail = App\Http\Controllers\MainFront\BiayaController::cari_detail($item->id);
                            @endphp

                            @foreach ($detail as $lel)
                            <li>{{ $lel->detail }}</li>
                            <hr>
                            @endforeach

                        </ul>
                    </div>
                    <div class="pricingTable-signup">
                        <a href="{{ route('register') }}">register</a>
                    </div>
                </div>
            </div>

            @endforeach
            @else
            <p>Silahkan Input Data Biaya </p>
            @endif
            {{--

            <div class="col-md-3 col-sm-6">
                <div class="pricingTable green">
                    <div class="pricingTable-header">

                        <div class="price-value"> $20.00 <span class="month">per tahun</span> </div>
                    </div>
                    <h3 class="heading">Business</h3>
                    <div class="pricing-content">
                        <ul>
                            <li><b>60GB</b> Disk Space</li>
                            <li><b>60</b> Email Accounts</li>
                            <li><b>60GB</b> Monthly Bandwidth</li>
                            <li><b>15</b> subdomains</li>
                            <li><b>20</b> Domains</li>
                        </ul>
                    </div>
                    <div class="pricingTable-signup">
                        <a href="{{ route('register') }}">register</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="pricingTable blue">
                    <div class="pricingTable-header">

                        <div class="price-value"> $30.00 <span class="month">per tahun</span> </div>
                    </div>
                    <h3 class="heading">Premium</h3>
                    <div class="pricing-content">
                        <ul>
                            <li><b>70GB</b> Disk Space</li>
                            <li><b>70</b> Email Accounts</li>
                            <li><b>70GB</b> Monthly Bandwidth</li>
                            <li><b>20</b> subdomains</li>
                            <li><b>25</b> Domains</li>
                        </ul>
                    </div>
                    <div class="pricingTable-signup">
                        <a href="{{ route('register') }}">register</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="pricingTable red">
                    <div class="pricingTable-header">

                        <div class="price-value"> $40.00 <span class="month">per tahun</span> </div>
                    </div>
                    <h3 class="heading">Extra</h3>
                    <div class="pricing-content">
                        <ul>
                            <li><b>80GB</b> Disk Space</li>
                            <li><b>80</b> Email Accounts</li>
                            <li><b>80GB</b> Monthly Bandwidth</li>
                            <li><b>25</b> subdomains</li>
                            <li><b>30</b> Domains</li>
                        </ul>
                    </div>
                    <div class="pricingTable-signup">
                        <a href="{{ route('register') }}">register</a>
                    </div>
                </div>
            </div> --}}

        </div>
    </div>
</section>
<!-- price end -->

@endsection

@section('css')

<style>
    .pricingTable {
        text-align: center;
        background: #fff;
        margin: 0 -15px;
        box-shadow: 0 0 10px #ababab;
        padding-bottom: 40px;
        border-radius: 10px;
        color: #cad0de;
        transform: scale(1);
        transition: all .5s ease 0s
    }

    .pricingTable:hover {
        transform: scale(1.05);
        z-index: 1
    }

    .pricingTable .pricingTable-header {
        padding: 40px 0;
        background: #f5f6f9;
        border-radius: 10px 10px 50% 50%;
        transition: all .5s ease 0s
    }

    .pricingTable:hover .pricingTable-header {
        background: #ff9624
    }

    .pricingTable .pricingTable-header i {
        font-size: 50px;
        color: #858c9a;
        margin-bottom: 10px;
        transition: all .5s ease 0s
    }

    .pricingTable .price-value {
        font-size: 35px;
        color: #ff9624;
        transition: all .5s ease 0s
    }

    .pricingTable .month {
        display: block;
        font-size: 14px;
        color: #cad0de
    }

    .pricingTable:hover .month,
    .pricingTable:hover .price-value,
    .pricingTable:hover .pricingTable-header i {
        color: #fff
    }

    .pricingTable .heading {
        font-size: 24px;
        color: #ff9624;
        margin-bottom: 20px;
        text-transform: uppercase
    }

    .pricingTable .pricing-content ul {
        list-style: none;
        padding: 0;
        margin-bottom: 30px
    }

    .pricingTable .pricing-content ul li {
        line-height: 30px;
        color: #a7a8aa
    }

    .pricingTable .pricingTable-signup a {
        display: inline-block;
        font-size: 15px;
        color: #fff;
        padding: 10px 35px;
        border-radius: 20px;
        background: #ffa442;
        text-transform: uppercase;
        transition: all .3s ease 0s
    }

    .pricingTable .pricingTable-signup a:hover {
        box-shadow: 0 0 10px #ffa442
    }

    .pricingTable.blue .heading,
    .pricingTable.blue .price-value {
        color: #4b64ff
    }

    .pricingTable.blue .pricingTable-signup a,
    .pricingTable.blue:hover .pricingTable-header {
        background: #4b64ff
    }

    .pricingTable.blue .pricingTable-signup a:hover {
        box-shadow: 0 0 10px #4b64ff
    }

    .pricingTable.red .heading,
    .pricingTable.red .price-value {
        color: #ff4b4b
    }

    .pricingTable.red .pricingTable-signup a,
    .pricingTable.red:hover .pricingTable-header {
        background: #ff4b4b
    }

    .pricingTable.red .pricingTable-signup a:hover {
        box-shadow: 0 0 10px #ff4b4b
    }

    .pricingTable.green .heading,
    .pricingTable.green .price-value {
        color: #40c952
    }

    .pricingTable.green .pricingTable-signup a,
    .pricingTable.green:hover .pricingTable-header {
        background: #40c952
    }

    .pricingTable.green .pricingTable-signup a:hover {
        box-shadow: 0 0 10px #40c952
    }

    .pricingTable.blue:hover .price-value,
    .pricingTable.green:hover .price-value,
    .pricingTable.red:hover .price-value {
        color: #fff
    }

    @media screen and (max-width:990px) {
        .pricingTable {
            margin: 0 0 20px
        }
    }
</style>
@endsection

@section('js')

<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js'></script>

@endsection
