@extends('layouts.userfront.app')

@section('title')
@if(is_null($settingumum))
Namaweb
@else
{{ $settingumum->namaweb }}
@endif
@endsection

@section('menu-desktop')
<!-- Logo desktop -->
<a href="#" class="logo">
    <img src="@if(is_null($settingumum)){{ asset('img/logo.png') }}@else{{ asset('img/logouser/'.$settingumum->logo) }}@endif "
        alt="IMG-LOGO">
</a>

<!-- Menu desktop -->
<div class="menu-desktop">
    <ul class="main-menu">

        @foreach ($menu as $menuitem)
        <li>
            <a href="{{ URL::current() }}/{{ $menuitem->title }}">{{ $menuitem->title }}</a>
        </li>
        @endforeach

    </ul>
</div>
@endsection

@section('menu-mobile')
<ul class="navbar-nav nav-justified w-100">

    @foreach ($menu as $menuitem)
    <li class="nav-item">
        <a href="{{ URL::current() }}/{{ $menuitem->title }}" class="nav-link text-center">
            <i class="{{ $menuitem->icon }}" style="font-size: 2rem;"></i><br />

            <span class="small d-block">{{ $menuitem->title }}</span>
        </a>
    </li>
    @endforeach

</ul>
@endsection

@section('slider')

<!-- Slider -->
<section class="section-slide">
    <div class="wrap-slick1 rs1-slick1">
        <div class="slick1">

            @foreach ($slider as $itemslider)

            <div class="item-slick1"
                style="background-image: url(@if(is_null($itemslider->foto)){{ asset('istore/images/slide-02.jpg') }}@else{{ asset('img/userslider/'.$itemslider->foto) }}@endif);">

                <div class="container h-full">
                    <div class="flex-col-l-m h-full p-t-100 p-b-30">
                        <div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
                            <span class="ltext-202 cl2 respon2">
                                {{ $itemslider->caption }}
                            </span>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="{{ $itemslider->sliderTextType }}"
                            data-delay="800">
                            <h2 class="ltext-104 cl2 p-t-19 p-b-43 respon1">
                                {{ $itemslider->caption }}
                            </h2>
                        </div>

                        <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
                            <a href="{{ $itemslider->link }}"
                                class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                                Detail
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>


@endsection

@section('content')

@endsection

@section('socialmedia')
<div class="p-t-27 txt-center">
    @foreach ($socialmedia as $itemsocialmedia)
    <a href="{{ $itemsocialmedia->detail }}" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
        <i class="{{ $itemsocialmedia->icon }} fa-2x"></i>
    </a>
    @endforeach

</div>
@endsection

@section('bannerfooter')
<div class="row">
    @foreach ($footer as $itemfooter)

    <div class="col-sm-6 col-lg-6 p-b-50">
        <h4 class="stext-301 cl0 p-b-30">
            {{ $itemfooter->title }}
        </h4>

        <p class="stext-120 cl7 size-204">
            {!! $itemfooter->detail !!}
        </p>

    </div>
    @endforeach

</div>
@endsection

@section('css')

@endsection

@section('js')

@endsection
