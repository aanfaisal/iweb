@extends('layouts.userfront.app')

@section('title')
{{ $settingumum->namaweb }}
@endsection

@section('menu-desktop')

<!-- Logo desktop -->
<a href="#" class="logo">
    <img src="@if(is_null($settingumum->logo)){{ asset('img/logo.png') }}@else{{ asset('img/logouser/'.$settingumum->logo) }}@endif "
        alt="IMG-LOGO">
</a>

<!-- Menu desktop -->
<div class="menu-desktop">
    <ul class="main-menu">

        @foreach ($menu as $menuitem)
        <li>
            <a href="{{ URL::current() }}/{{ $menuitem->title }}">{{ $menuitem->title }}</a>
        </li>
        @endforeach

    </ul>
</div>
@endsection

@section('menu-mobile')
<ul class="navbar-nav nav-justified w-100">

    @foreach ($menu as $menuitem)
    <li class="nav-item">
        <a href="{{ URL::current() }}/{{ $menuitem->title }}" class="nav-link text-center">
            <i class="{{ $menuitem->icon }}" style="font-size: 2rem;"></i><br />

            <span class="small d-block">{{ $menuitem->title }}</span>
        </a>
    </li>
    @endforeach

</ul>
@endsection


@section('content')

@include('layouts.userfront.partials.product')

@endsection

@section('socialmedia')
<div class="p-t-27 txt-center">
    @foreach ($socialmedia as $itemsocialmedia)
    <a href="{{ $itemsocialmedia->detail }}" class="fs-18 cl7 hov-cl1 trans-04 m-r-16">
        <i class="{{ $itemsocialmedia->icon }} fa-2x"></i>
    </a>
    @endforeach

</div>
@endsection

@section('bannerfooter')
<div class="row">
    @foreach ($footer as $itemfooter)

    <div class="col-sm-6 col-lg-6 p-b-50">
        <h4 class="stext-301 cl0 p-b-30">
            {{ $itemfooter->title }}
        </h4>

        <p class="stext-120 cl7 size-204">
            {!! $itemfooter->detail !!}
        </p>

    </div>
    @endforeach

</div>
@endsection

@section('css')

@endsection

@section('js')

@endsection
