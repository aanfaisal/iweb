<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\HomeController;

use App\Http\Controllers\Main\Manage\UserController;
use App\Http\Controllers\Main\Manage\SettingController;
use App\Http\Controllers\Main\Manage\AksesMenuController;

use App\Http\Controllers\Main\Setting\SettingHomePageController;
use App\Http\Controllers\Main\Setting\SettingBiayaController;
use App\Http\Controllers\Main\Setting\SettingTentangController;
use App\Http\Controllers\Main\Setting\SettingTestimoniController;

use App\Http\Controllers\MainFront\IndexController;
use App\Http\Controllers\MainFront\BiayaController;
use App\Http\Controllers\MainFront\TentangController;


use App\Http\Controllers\User\UserSettingUmumController;
use App\Http\Controllers\User\UserSocialMediaController;
use App\Http\Controllers\User\UserFooterController;
use App\Http\Controllers\User\UserMenuController;
use App\Http\Controllers\User\UserPageController;
use App\Http\Controllers\User\UserSliderController;
use App\Http\Controllers\User\UserBannerController;


use App\Http\Controllers\UserFront\UserFrontendController;
use App\Http\Controllers\UserFront\UserFrontendMenuController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['web']], function () {

    /*
    * Routes Untuk Halaman Awal Website Utama
    */

    Route::get('/reload-captcha', [App\Http\Controllers\Auth\RegisterController::class, 'reloadCaptcha'])->name('reloadCaptcha');

    Route::get('/', [IndexController::class, 'index'])->name('homepage.index');

    Route::get('biaya', [BiayaController::class, 'index'])->name('biaya.index');

    Route::get('tentang', [TentangController::class, 'index'])->name('tentang.index');
});


Route::group(['middleware' => ['web']], function () {

    /*
    * Routes Untuk Fitur2 Halaman User
    */
    Route::prefix('web')->group(function () {

        Route::get('{username}', [UserFrontendController::class, 'index']);

        Route::get('{username}/{menuname}', [UserFrontendMenuController::class, 'index']);
    });
});


/*
* Routes Setelah Login
*/
Route::group(['middleware' => ['auth']], function () {
    /*
    * Routes Untuk Fitur2 Dashboard
    */
    Route::get('home', [HomeController::class, 'index'])->name('dashboard.index');

    /*
    * Routes Untuk Fitur2 User
    */
    Route::prefix('user')->group(function () {
        Route::resource('user-setting-umum', UserSettingUmumController::class);

        Route::resource('user-social-media', UserSocialMediaController::class);

        Route::resource('user-footer', UserFooterController::class);

        Route::resource('user-menu', UserMenuController::class);

        Route::resource('user-page', UserPageController::class);

        Route::resource('user-slider', UserSliderController::class);

        Route::resource('user-banner', UserBannerController::class);
    });
});
