<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;

use App\Http\Controllers\Main\Manage\UserController;
use App\Http\Controllers\Main\Manage\SettingController;
use App\Http\Controllers\Main\Manage\AksesMenuController;

use App\Http\Controllers\Main\Setting\SettingHomePageController;
use App\Http\Controllers\Main\Setting\SettingBiayaController;
use App\Http\Controllers\Main\Setting\SettingTentangController;
use App\Http\Controllers\Main\Setting\SettingTestimoniController;

use App\Http\Controllers\MainFront\IndexController;
use App\Http\Controllers\MainFront\BiayaController;
use App\Http\Controllers\MainFront\TentangController;
use App\Http\Controllers\MainFront\WebStoreController;
use App\Http\Controllers\MainFront\TestimoniController;


use App\Http\Controllers\User\UserSettingUmumController;
use App\Http\Controllers\User\UserSocialMediaController;
use App\Http\Controllers\User\UserFooterController;
use App\Http\Controllers\User\UserMenuController;
use App\Http\Controllers\User\UserPageController;
use App\Http\Controllers\User\UserSliderController;
use App\Http\Controllers\User\UserBannerController;
use App\Http\Controllers\User\UserPostController;
use App\Http\Controllers\User\UserTextController;
use App\Http\Controllers\User\UserStoreController;


use App\Http\Controllers\UserFront\UserFrontendController;
use App\Http\Controllers\UserFront\UserFrontendMenuController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['web', 'visitors']], function () {

    /*
    * Routes Untuk Halaman Awal Website Utama
    */

    Route::get('/reload-captcha', [RegisterController::class, 'reloadCaptcha'])->name('reloadCaptcha');

    Route::get('/', [IndexController::class, 'index'])->name('homepage.index');

    Route::get('biaya', [BiayaController::class, 'index'])->name('biaya.index');

    Route::get('webstore', [WebStoreController::class, 'index'])->name('webstore.index');

    Route::get('tentang', [TentangController::class, 'index'])->name('tentang.index');

    Route::get('testimoni', [TestimoniController::class, 'index'])->name('testimoni.index');
});



/*
* Routes Setelah Login ( ADMIN )
*/
Route::group(['middleware' => ['auth']], function () {
    /*
    * Routes Untuk Fitur2 Dashboard
    */
    Route::get('home', [HomeController::class, 'index'])->name('dashboard.index');

    Route::get('wait', [HomeController::class, 'wait'])->name('wait.index');

    /*
    * Routes Untuk Fitur2 Setting
    */
    Route::prefix('setting')->group(function () {
        /*
        * Routes Untuk Fitur2 User Management
        */
        Route::resource('user', UserController::class);
        Route::get('user/{id}/profil', [UserController::class, 'getProfile'])->name('user.profil');
        Route::patch('user/{id}/profil', [UserController::class, 'updateProfile']);
        Route::post('user/{id}/uploadfoto',  [UserController::class, 'storefotoUpload'])->name('user.uploadfoto');

        Route::post('active-useradmin/{id}', [UserController::class, 'active'])->name('user-admin.active');
    });

    /*
    * Routes Untuk Fitur2 Admin
    */
    Route::prefix('admin')->group(function () {

        Route::resource('setting-homepage', SettingHomePageController::class);
        Route::post('active-homepage/{id}', [SettingHomePageController::class, 'active'])->name('setting-homepage.active');

        Route::resource('setting-biaya', SettingBiayaController::class);
        Route::post('active-biaya/{id}', [SettingBiayaController::class, 'active'])->name('setting-biaya.active');

        Route::resource('setting-tentang', SettingTentangController::class);
        Route::post('active-tentang/{id}', [SettingTentangController::class, 'active'])->name('setting-tentang.active');

        Route::resource('setting-testimoni', SettingTestimoniController::class);
        Route::post('active-testimoni/{id}', [SettingTestimoniController::class, 'active'])->name('setting-testimoni.active');
    });


    /*
    * Routes Untuk Fitur2 User Administration
    */
    Route::prefix('user')->group(function () {
        Route::resource('user-setting-umum', UserSettingUmumController::class);
        Route::post('active-settingumum/{id}', [UserSettingUmumController::class, 'active'])->name('user-settingumum.active');

        Route::resource('user-social-media', UserSocialMediaController::class);
        Route::post('active-socialmedia/{id}', [UserSocialMediaController::class, 'active'])->name('user-socialmedia.active');

        Route::resource('user-footer', UserFooterController::class);
        Route::post('active-userfooter/{id}', [UserFooterController::class, 'active'])->name('user-footer.active');

        Route::resource('user-menu', UserMenuController::class);
        Route::post('active-usermenu/{id}', [UserMenuController::class, 'active'])->name('user-menu.active');

        Route::resource('user-page', UserPageController::class);
        Route::post('active-userpage/{id}', [UserPageController::class, 'active'])->name('user-page.active');


        Route::resource('user-slider', UserSliderController::class);
        Route::post('active-userslider/{id}', [UserSliderController::class, 'active'])->name('user-slider.active');

        Route::resource('user-banner', UserBannerController::class);
        Route::post('active-userbanner/{id}', [UserBannerController::class, 'active'])->name('user-banner.active');

        Route::resource('user-post', UserPostController::class);
        Route::post('active-userpost/{id}', [UserPostController::class, 'active'])->name('user-post.active');

        Route::resource('user-text', UserTextController::class);
        Route::post('active-usertext/{id}', [UserTextController::class, 'active'])->name('user-text.active');


        Route::resource('user-store', UserStoreController::class);
        Route::post('active-userstore/{id}', [UserStoreController::class, 'active'])->name('user-store.active');

        //
    });
});
