<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = \DB::table('users')->get();
        $usercount = $user->count();

        $quote = Inspiring::quote();

        $url = env('APP_URL');

        $paket_aktif = \DB::table('users')->where('paket_aktif', 1)->get();
        $paket_count = $paket_aktif->count();

        $user_aktif = \DB::table('users')->get();
        $useraktif_count = $user_aktif->count();

        // Alert::success('SUCCESS', 'We will notify you via whatapps/email when your website ready to use');

        $visitors = \DB::table('shetabit_visits')->where('url', 'LIKE', "%$url%")->distinct('ip')->count();

        // Alert::success('Selamat Datang', 'App Iweb');

        return view('main.dashboard', compact('usercount', 'quote', 'paket_count', 'useraktif_count', 'visitors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wait()
    {

        $quote = Inspiring::quote();

        Alert::success('SUCCESS', 'We will notify you via whatapps/email when your website ready to use');

        // Alert::success('Selamat Datang', 'App Iweb');

        return view('main.wait', compact('quote'));
    }
}
