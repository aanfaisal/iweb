<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserText;
use App\Models\UserMenu;
use App\Models\UserPage;

class UserTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('UserText.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $usertext = UserText::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('title1', 'LIKE', "%$keyword%")
                ->orWhere('detail1', 'LIKE', "%$keyword%")
                ->orWhere('title2', 'LIKE', "%$keyword%")
                ->orWhere('detail2', 'LIKE', "%$keyword%")
                ->orWhere('link', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $usertext = UserText::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-text.index', compact('usertext'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('UserText.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $menu = UserMenu::where('user_id', env('USERID'))->where('active', 1)->get();
        $page = UserPage::where('user_id', env('USERID'))->where('active', 1)->get();

        return view('user.user-text.create', compact('menu', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('UserText.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'user_id' => 'required',
            'menu_id' => 'required',
            'page_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            if (!empty($request->get('detail1'))) {

                $data1 = $request->get('detail1');
                //loading the html data from the summernote editor and select the img tags from it
                $dom1 = new \DomDocument();
                @$dom1->loadHtml($data1, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom1->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data1 = $dom1->saveHTML();
            } else {
                $data1 = "";
            }


            if (!empty($request->get('detail2'))) {

                $data2 = $request->get('detail2');
                //loading the html data from the summernote editor and select the img tags from it
                $dom2 = new \DomDocument();
                @$dom2->loadHtml($data2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom2->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data2 = $dom2->saveHTML();
            } else {
                $data2 = "";
            }

            $UserText = new UserText();
            $UserText->user_id = $user_id;
            $UserText->username = $username;
            $UserText->menu_id = $request->get('menu_id');
            $UserText->page_id = $request->get('page_id');
            $UserText->detail1 = $data1;
            $UserText->detail2 = $data2;
            $UserText->title1 = $request->get('title1');
            $UserText->title2 = $request->get('title2');
            $UserText->active = '0';


            if (!empty($request->file('foto1'))) {
                $imageData = $request->file('foto1');

                $fileName = 'Text_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 600)->save(public_path('img/usertext/') . $fileName);

                $UserText->foto1 = $fileName;
            }

            if (!empty($request->file('foto2'))) {
                $imageData = $request->file('foto2');

                $fileName = 'Text_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 600)->save(public_path('img/usertext/') . $fileName);

                $UserText->foto2 = $fileName;
            }

            $UserText->save();
        }

        Alert::success('Sukses', 'Simpan Data User Text');

        return redirect()->route('user-text.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('UserText.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usertext = UserText::findOrFail($id);

        return view('user.user-text.show', compact('usertext'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('UserText.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usertext = UserText::findOrFail($id);
        $menu = UserMenu::where('user_id', env('USERID'))->where('active', 1)->get();
        $page = UserPage::where('user_id', env('USERID'))->where('active', 1)->get();

        return view('user.user-text.edit', compact('usertext', 'menu', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'user_id' => 'required',
            'menu_id' => 'required',
            'page_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            if (!empty($request->get('detail1'))) {

                $data1 = $request->get('detail1');
                //loading the html data from the summernote editor and select the img tags from it
                $dom1 = new \DomDocument();
                @$dom1->loadHtml($data1, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom1->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data1 = $dom1->saveHTML();
            } else {
                $data1 = "";
            }


            if (!empty($request->get('detail2'))) {

                $data2 = $request->get('detail2');
                //loading the html data from the summernote editor and select the img tags from it
                $dom2 = new \DomDocument();
                @$dom2->loadHtml($data2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom2->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data2 = $dom2->saveHTML();
            } else {
                $data2 = "";
            }

            $UserText = UserText::findOrFail($id);
            $UserText->user_id = $user_id;
            $UserText->username = $username;
            $UserText->menu_id = $request->get('menu_id');
            $UserText->page_id = $request->get('page_id');
            $UserText->detail1 = $data1;
            $UserText->detail2 = $data2;
            $UserText->title1 = $request->get('title1');
            $UserText->title2 = $request->get('title2');
            $UserText->active = '0';

            if (!empty($request->file('foto1'))) {
                $imageData = $request->file('foto1');

                $fileName = 'Text_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 600)->save(public_path('img/usertext/') . $fileName);

                $UserText->foto1 = $fileName;
            }

            if (!empty($request->file('foto2'))) {
                $imageData = $request->file('foto2');

                $fileName = 'Text_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 600)->save(public_path('img/usertext/') . $fileName);

                $UserText->foto2 = $fileName;
            }

            $UserText->save();
        }

        Alert::success('Sukses', 'Update Data User Text');

        return redirect()->route('user-text.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('UserText.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserText::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Text');

        return redirect()->route('user-text.index');
    }

    /**
     * Update Active Text
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // Set ALL records to a status of 0
        DB::table('user_texts')->where('username',  env('USERNAME'))
            ->where('user_id', env('USERID'))->where('active', 1)->update(['active' => 0]);

        $Setting = UserText::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update Aktifkan User Text');

        return redirect()->back();
    }
}
