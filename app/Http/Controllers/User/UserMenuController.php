<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;

use App\Models\UserMenu;
use App\Models\SettingBiaya;
use App\Models\User;

class UserMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('userMenu.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $usermenu = UserMenu::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('icon', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('detail', 'LIKE', "%$keyword%")
                ->orWhere('typepage', 'LIKE', "%$keyword%")
                ->orWhere('order', 'LIKE', "%$keyword%")
                ->orWhere('link', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $usermenu = UserMenu::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-menu.index', compact('usermenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('userMenu.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('user.user-menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('userMenu.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'typepage' => 'required',
            'order' => 'required',
        ]);

        $detailuser = User::where('id', $request->get('user_id'))->first();

        $biaya = SettingBiaya::where('namapaket', $detailuser->paket)->first();

        $menumaxinum = $biaya->jumlahmenu;

        $menuuser = UserMenu::where('user_id', $request->get('user_id'))->where('active', 1)->get();

        $jumlahmenu = $menuuser->count();

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } elseif ($jumlahmenu == $menumaxinum) {

            Alert::warning('Maaf', 'Anda Telah Mencapai Limit Jumlah Menu Aktif');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            $title = $request->get('title');

            $link = $username . "-" . Str::slug($title);

            $UserMenu = new UserMenu();
            $UserMenu->user_id = $user_id;
            $UserMenu->username = $username;
            $UserMenu->icon = $request->get('icon');
            $UserMenu->title = $request->get('title');
            $UserMenu->detail = $request->get('detail');
            $UserMenu->typepage = $request->get('typepage');
            $UserMenu->order = $request->get('order');
            $UserMenu->active = $request->get('active');
            $UserMenu->link = $link;

            $UserMenu->save();
        }


        Alert::success('Sukses', 'Simpan Data User Menu');

        return redirect()->route('user-menu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('userMenu.show')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usermenu = UserMenu::findOrFail($id);

        return view('user.user-menu.show', compact('usermenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('userMenu.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usermenu = UserMenu::findOrFail($id);

        return view('user.user-menu.edit', compact('usermenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('userMenu.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'typepage' => 'required',
            'order' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');
            $title = $request->get('title');

            $link = $username . "-" . Str::slug($title);

            $UserMenu = UserMenu::findOrFail($id);
            $UserMenu->user_id = $user_id;
            $UserMenu->username = $username;
            $UserMenu->icon = $request->get('icon');
            $UserMenu->title = $request->get('title');
            $UserMenu->detail = $request->get('detail');
            $UserMenu->typepage = $request->get('typepage');
            $UserMenu->order = $request->get('order');
            $UserMenu->active = $request->get('active');
            $UserMenu->link = $link;

            $UserMenu->save();
        }

        Alert::success('Sukses', 'Update Data User Menu');

        return redirect()->route('user-menu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('userMenu.delete')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserMenu::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Menu');

        return redirect()->route('user-menu.index');
    }

    /**
     * Update Active Menu
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // $counter = UserFooter::where('active', 1)->get();
        // $count = $counter->count();

        $detailuser = User::where('id', auth()->user()->id)->first();

        $biaya = SettingBiaya::where('namapaket', $detailuser->paket)->first();

        $menumaxinum = $biaya->jumlahmenu;

        $menuuser = UserMenu::where('user_id', auth()->user()->id)->where('active', 1)->get();

        $jumlahmenu = $menuuser->count();

        if ($jumlahmenu >= $menumaxinum) {

            Alert::warning('Maaf', 'Anda Telah Mencapai Limit Jumlah Menu Aktif');
            return redirect()->back();
        } else {
            $Setting = UserMenu::findOrFail($id);
            $Setting->active = $request->get('active');
            $Setting->save();
        }

        Alert::success('Sukses', 'Update Aktif User Menu');

        return redirect()->back();
    }
}
