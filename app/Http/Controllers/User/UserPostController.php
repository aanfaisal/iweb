<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserPost;
use App\Models\UserPostDetail;
use App\Models\UserMenu;
use App\Models\UserPage;

class UserPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('userPost.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $userpost = UserPost::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('menu_id', 'LIKE', "%$keyword%")
                ->orWhere('page_id', 'LIKE', "%$keyword%")
                ->orWhere('foto', 'LIKE', "%$keyword%")
                ->orWhere('thumbnail', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('detail', 'LIKE', "%$keyword%")
                ->orWhere('link', 'LIKE', "%$keyword%")
                ->orWhere('category', 'LIKE', "%$keyword%")
                ->orWhere('file', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $userpost = DB::table('user_posts')->where('user_posts.username', env('USERNAME'))->where('user_posts.user_id', env('USERID'))
                ->where('user_posts.deleted_at', NULL)
                ->join('user_pages', 'user_posts.page_id', '=', 'user_pages.id')
                ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
                ->select('user_posts.*', 'user_pages.title AS namapage', 'user_menus.typepage AS Tipe')->get();
        }

        return view('user.user-post.index', compact('userpost'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('userPost.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $menu = UserMenu::where('user_id', env('USERID'))->where('active', 1)->get();
        $page = UserPage::where('user_id', env('USERID'))->where('active', 1)->get();

        return view('user.user-post.create', compact('menu', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('userPost.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'menu_id' => 'required',
            'page_id' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            $title = $request->get('title');
            $link = $username . "-" . Str::slug($title);


            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }

            $UserPost = new UserPost();
            $UserPost->user_id = $user_id;
            $UserPost->username = $username;
            $UserPost->menu_id = $request->get('menu_id');
            $UserPost->page_id = $request->get('page_id');
            $UserPost->title = $request->get('title');
            $UserPost->detail = $data;
            $UserPost->link = $link;
            $UserPost->active = '1';

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Post_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1200, 600)->save(public_path('img/userpost/') . $fileName);

                $UserPost->foto = $fileName;

                $filenames = 'Post_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(640, 360)->save(public_path('img/userpost/') . $filenames);

                $UserPost->thumbnail = $filenames;
            }

            // if (!empty($request->file('file'))) {
            //     $fileData = $request->file('file');

            //     $fileName = 'Post_' . $username . '_' . $fileData->getClientOriginalName() . '.' . $fileData->getClientOriginalExtension();

            //     $request->file->move(public_path('file'), $fileName);

            //     $UserPost->file = $fileName;
            // }

            $UserPost->save();

            // $userpost_id = UserPost::orderBy('id', 'DESC')->first();

            // if (!empty($request->hasfile('fotodetail'))) {

            //     foreach ($request->file('fotodetail') as $image) {

            //         $imageData = $image;

            //         $name = $image->getClientOriginalName();

            //         Image::make($imageData)->resize(1200, 600)->save(public_path('img/userpost/') . $name);

            //         $userDetail = new UserPostDetail();
            //         $userDetail->userpost_id = $userpost_id->id;
            //         $userDetail->user_id = $user_id;
            //         $userDetail->username = $username;
            //         $userDetail->menu_id = $request->get('menu_id');
            //         $userDetail->page_id = $request->get('page_id');
            //         $userDetail->foto = $name;

            //         $userDetail->save();
            //     }
            // }
        }

        Alert::success('Sukses', 'Simpan Data User Posting');

        return redirect()->route('user-post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('userPost.list')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        // $userpost = UserPost::findOrFail($id);
        $userpost = DB::table('user_posts')->where('user_posts.username', env('USERNAME'))->where('user_posts.user_id', env('USERID'))
            ->where('user_posts.deleted_at', NULL)
            ->where('user_posts.id', $id)
            ->join('user_pages', 'user_posts.page_id', '=', 'user_pages.id')
            ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
            ->select('user_posts.*', 'user_pages.title AS namapage', 'user_menus.title AS namamenu',  'user_posts.id AS IDpost')->first();

        $userpostdetail = UserPostDetail::where('userpost_id', $id)->get();

        return view('user.user-post.show', compact('userpost', 'userpostdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('userPost.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userpost = UserPost::findOrFail($id);
        $menu = UserMenu::where('user_id', env('USERID'))->where('active', 1)->get();
        $page = UserPage::where('user_id', env('USERID'))->where('active', 1)->get();
        $userpostdetail = UserPostDetail::where('userpost_id', $id)->get();

        return view('user.user-post.edit', compact('userpost', 'menu', 'page', 'userpostdetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('userPost.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'menu_id' => 'required',
            'page_id' => 'required',
            'title' => 'required',
        ]);


        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            $title = $request->get('title');
            $link = $username . "-" . Str::slug($title);


            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }


            $UserPost = UserPost::findOrFail($id);
            $UserPost->user_id = $user_id;
            $UserPost->username = $username;
            $UserPost->menu_id = $request->get('menu_id');
            $UserPost->page_id = $request->get('page_id');
            $UserPost->title = $request->get('title');
            $UserPost->detail = $data;
            $UserPost->link = $link;
            $UserPost->active = '1';

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Post_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1200, 837)->save(public_path('img/userpost/') . $fileName);

                $UserPost->foto = $fileName;

                $filenames = 'Post_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 403)->save(public_path('img/userpost/') . $filenames);

                $UserPost->thumbnail = $filenames;
            }

            $UserPost->save();

            // if (!empty($request->file('file'))) {
            //     $fileData = $request->file('file');

            //     $fileName = 'Post_' . $username . '_' . $fileData->getClientOriginalName() . '.' . $fileData->getClientOriginalExtension();

            //     $request->file->move(public_path('file'), $fileName);

            //     $UserPost->file = $fileName;
            // }


            // $fotodetail = $request->file('fotodetail');

            // Hapus Data Detail Lama

            // if (!empty($request->hasfile('fotodetail'))) {
            //     for ($a = 0; $a < count($fotodetail); $a++) {
            //         if (!empty($fotodetail[$a])) {

            //             UserPostDetail::where('userpost_id', $id)->delete();
            //         }
            //     }
            // }

            // if (!empty($request->hasfile('fotodetail'))) {

            //     foreach ($request->file('fotodetail') as $image) {

            //         $imageData = $image;

            //         $name = $image->getClientOriginalName();

            //         Image::make($imageData)->resize(1200, 600)->save(public_path('img/userpost/') . $name);

            //         $userDetail = new UserPostDetail();
            //         $userDetail->userpost_id = $id;
            //         $userDetail->user_id = $user_id;
            //         $userDetail->username = $username;
            //         $userDetail->menu_id = $request->get('menu_id');
            //         $userDetail->page_id = $request->get('page_id');
            //         $userDetail->foto = $name;

            //         $userDetail->save();
            //     }
            // }
        }

        Alert::success('Sukses', 'Update Data User Posting');

        return redirect()->route('user-post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('userPost.delete')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserPost::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Posting');

        return redirect()->route('user-post.index');
    }

    /**
     * Update Active Page
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // $counter = UserFooter::where('active', 1)->get();
        // $count = $counter->count();

        $Setting = UserPost::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update Aktif User Posting');

        return redirect()->back();
    }
}
