<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserSlider;
use App\Models\UserMenu;
use App\Models\UserPage;

class UserSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('userSlider.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $userslider = UserSlider::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('menu_id', 'LIKE', "%$keyword%")
                ->orWhere('page_id', 'LIKE', "%$keyword%")
                ->orWhere('foto', 'LIKE', "%$keyword%")
                ->orWhere('thumbnail', 'LIKE', "%$keyword%")
                // ->orWhere('caption', 'LIKE', "%$keyword%")
                // ->orWhere('sliderTextType', 'LIKE', "%$keyword%")
                // ->orWhere('caption_2', 'LIKE', "%$keyword%")
                ->orWhere('order', 'LIKE', "%$keyword%")
                // ->orWhere('link', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $userslider = DB::table('user_sliders')->where('user_sliders.username',  env('USERNAME'))->where('user_sliders.user_id', env('USERID'))
                ->where('user_sliders.deleted_at', NULL)
                ->join('user_menus', 'user_sliders.menu_id', '=', 'user_menus.id')
                ->join('user_pages', 'user_sliders.page_id', '=', 'user_pages.id')
                ->select('user_sliders.*', 'user_menus.title AS namamenu', 'user_pages.title AS namapage')->get();
        }

        return view('user.user-slider.index', compact('userslider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('userSlider.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $menu = UserMenu::where('user_id', env('USERID'))->get();

        $page = UserPage::where('user_id', env('USERID'))->get();

        return view('user.user-slider.create', compact('menu', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('userSlider.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'menu_id' => 'required',
            'page_id' => 'required',
            'order' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');


            if (!empty($request->get('caption'))) {

                $data = $request->get('caption');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }


            if (!empty($request->get('caption_2'))) {

                $data2 = $request->get('caption_2');
                //loading the html data from the summernote editor and select the img tags from it
                $dom2 = new \DomDocument();
                @$dom2->loadHtml($data2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom2->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data2 = $dom2->saveHTML();
            } else {
                $data2 = "";
            }


            $UserSlider = new UserSlider();
            $UserSlider->user_id = $user_id;
            $UserSlider->username = $username;
            $UserSlider->menu_id = $request->get('menu_id');
            $UserSlider->page_id = $request->get('page_id');
            $UserSlider->caption = $data;
            $UserSlider->caption_2 = $data2;
            // $UserSlider->sliderTextType = $request->get('sliderTextType');
            $UserSlider->order = $request->get('order');
            // $UserSlider->link = $request->get('link');


            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Slider_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1280, 720)->save(public_path('img/userslider/') . $fileName);

                $UserSlider->foto = $fileName;
            }

            if (!empty($request->file('thumbnail'))) {
                $imageThumb = $request->file('thumbnail');

                $filenames = 'Slider_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageThumb)->resize(461, 1024)->save(public_path('img/userslider/') . $filenames);

                $UserSlider->thumbnail = $filenames;
            }

            $UserSlider->save();
        }

        Alert::success('Sukses', 'Simpan Data User Slider');

        return redirect()->route('user-slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('userSlider.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userslider = DB::table('user_sliders')->where('user_sliders.username',  env('USERNAME'))->where('user_sliders.user_id', env('USERID'))
            ->where('user_sliders.id', $id)
            ->join('user_menus', 'user_sliders.menu_id', '=', 'user_menus.id')
            ->join('user_pages', 'user_sliders.page_id', '=', 'user_pages.id')
            ->select('user_sliders.*', 'user_menus.title AS namamenu', 'user_pages.title AS namapage')->first();

        $menu = UserMenu::where('user_id', env('USERID'))->get();

        $page = UserPage::where('user_id', env('USERID'))->get();


        return view('user.user-slider.show', compact('userslider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('userSlider.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userslider = UserSlider::findOrFail($id);

        $menu = UserMenu::where('user_id', env('USERID'))->get();

        $page = UserPage::where('user_id', env('USERID'))->get();

        return view('user.user-slider.edit', compact('userslider', 'menu', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('userSlider.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'menu_id' => 'required',
            'page_id' => 'required',
            'order' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            if (!empty($request->get('caption'))) {

                $data = $request->get('caption');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }


            if (!empty($request->get('caption_2'))) {

                $data2 = $request->get('caption_2');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data2, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data2 = $dom->saveHTML();
            } else {
                $data2 = "";
            }

            $UserSlider = UserSlider::findOrFail($id);
            $UserSlider->user_id = $user_id;
            $UserSlider->username = $username;
            $UserSlider->menu_id = $request->get('menu_id');
            $UserSlider->page_id = $request->get('page_id');
            $UserSlider->caption = $data;
            $UserSlider->caption_2 = $data2;
            // $UserSlider->sliderTextType = $request->get('sliderTextType');
            $UserSlider->order = $request->get('order');
            // $UserSlider->link = $request->get('link');


            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Slider_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1280, 720)->save(public_path('img/userslider/') . $fileName);

                $UserSlider->foto = $fileName;
            }

            if (!empty($request->file('thumbnail'))) {
                $imageThumb = $request->file('thumbnail');

                $filenames = 'Slider_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageThumb)->resize(461, 1024)->save(public_path('img/userslider/') . $filenames);

                $UserSlider->thumbnail = $filenames;
            }

            $UserSlider->save();
        }

        Alert::success('Sukses', 'Update Data User Slider');

        return redirect()->route('user-slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('userSlider.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserSlider::destroy($id);

        Alert::success('Sukses', 'Simpan Data User Slider');

        return redirect()->route('user-slider.index');
    }


    /**
     * Update Active Slider
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        $Setting = UserSlider::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update User Slider');

        return redirect()->back();
    }
}
