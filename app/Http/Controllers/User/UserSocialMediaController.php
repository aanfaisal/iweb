<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;

use App\Models\UserSocialMedia;

class UserSocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('UserSocialMedia.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $usersocialmedia = UserSocialMedia::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('icon', 'LIKE', "%$keyword%")
                ->orWhere('detail', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $usersocialmedia = UserSocialMedia::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-social-media.index', compact('usersocialmedia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('UserSocialMedia.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('user.user-social-media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('UserSocialMedia.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'username' => 'required',
            'icon' => 'required',
            'detail' => 'required|url',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            $UserSocialMedia = new UserSocialMedia();
            $UserSocialMedia->user_id = $user_id;
            $UserSocialMedia->username = $username;
            $UserSocialMedia->icon = $request->get('icon');
            $UserSocialMedia->detail = $request->get('detail');

            $UserSocialMedia->save();
        }

        Alert::success('Sukses', 'Simpan Data User Social Media');

        return redirect()->route('user-social-media.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('UserSocialMedia.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usersocialmedia = UserSocialMedia::findOrFail($id);

        return view('user.user-social-media.show', compact('usersocialmedia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('UserSocialMedia.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usersocialmedia = UserSocialMedia::findOrFail($id);

        return view('user.user-social-media.edit', compact('usersocialmedia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('UserSocialMedia.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'username' => 'required',
            'icon' => 'required',
            'detail' => 'required|url',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            $UserSocialMedia = UserSocialMedia::findOrFail($id);
            $UserSocialMedia->user_id = $user_id;
            $UserSocialMedia->username = $username;
            $UserSocialMedia->icon = $request->get('icon');
            $UserSocialMedia->detail = $request->get('detail');

            $UserSocialMedia->save();
        }

        Alert::success('Sukses', 'Update Data User Social Media');

        return redirect()->route('user-social-media.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('UserSocialMedia.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserSocialMedia::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Social Media');

        return redirect()->route('user-social-media.index');
    }

    /**
     * Update Active Banner
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        $Setting = UserSocialMedia::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update Aktifkan User Social Media');

        return redirect()->back();
    }
}
