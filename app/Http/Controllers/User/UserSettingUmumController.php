<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserSettingUmum;

class UserSettingUmumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('UserSettingUmum.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $usersettingumum = UserSettingUmum::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('namaweb', 'LIKE', "%$keyword%")
                ->orWhere('tema', 'LIKE', "%$keyword%")
                ->orWhere('webaddress', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $usersettingumum = UserSettingUmum::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-setting-umum.index', compact('usersettingumum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('UserSettingUmum.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('user.user-setting-umum.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('UserSettingUmum.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'username' => 'required',
            'logo' => 'required',
            'namaweb' => 'required',
            'tema' => 'required',
            'favicon' => 'required',
            'webaddress' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            $UserSettingUmum = new UserSettingUmum();
            $UserSettingUmum->user_id = $user_id;
            $UserSettingUmum->username = $username;
            $UserSettingUmum->namaweb = $request->get('namaweb');
            $UserSettingUmum->tema = $request->get('tema');
            $UserSettingUmum->webaddress = $request->get('webaddress');

            if (!empty($request->file('logo'))) {
                $imageData = $request->file('logo');

                $fileName = 'Logo_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1600, 800)->save(public_path('img/logouser/') . $fileName);

                $UserSettingUmum->logo = $fileName;
            }

            if (!empty($request->file('favicon'))) {
                $imageData = $request->file('favicon');

                $fileName = 'Favicon_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(16, 16)->save(public_path('img/logouser/') . $fileName);

                $UserSettingUmum->favicon = $fileName;
            }

            $UserSettingUmum->save();
        }

        Alert::success('Sukses', 'Simpan Data User Setting Umum');

        return redirect()->route('user-setting-umum.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('UserSettingUmum.list')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usersettingumum = UserSettingUmum::findOrFail($id);

        return view('user.user-setting-umum.show', compact('usersettingumum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('UserSettingUmum.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $usersettingumum = UserSettingUmum::findOrFail($id);

        return view('user.user-setting-umum.edit', compact('usersettingumum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('UserSettingUmum.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'username' => 'required',
            'namaweb' => 'required',
            'tema' => 'required',
            'webaddress' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            $UserSettingUmum = UserSettingUmum::findOrFail($id);
            $UserSettingUmum->user_id = $user_id;
            $UserSettingUmum->username = $username;
            $UserSettingUmum->namaweb = $request->get('namaweb');
            $UserSettingUmum->tema = $request->get('tema');
            $UserSettingUmum->webaddress = $request->get('webaddress');

            if (!empty($request->file('logo'))) {
                $imageData = $request->file('logo');

                $fileName = 'Logo_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(480, 480)->save(public_path('img/logouser/') . $fileName);

                $UserSettingUmum->logo = $fileName;
            }

            $UserSettingUmum->save();
        }

        Alert::success('Sukses', 'Update Data User Setting Umum');

        return redirect()->route('user-setting-umum.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('UserSettingUmum.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserSettingUmum::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Setting Umum');

        return redirect()->route('user-setting-umum.index');
    }

    /**
     * Update Active Menu
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // Set ALL records to a status of 0
        DB::table('user_setting_umums')->where('username',  env('USERNAME'))
            ->where('user_id', env('USERID'))->where('active', 1)->update(['active' => 0]);

        $Setting = UserSettingUmum::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update Aktif User Setting Umum');

        return redirect()->back();
    }
}
