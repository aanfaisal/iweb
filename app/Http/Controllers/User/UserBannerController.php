<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserBanner;

class UserBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('userBanner.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $userbanner = UserBanner::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('foto', 'LIKE', "%$keyword%")
                ->orWhere('thumbnail', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('order', 'LIKE', "%$keyword%")
                ->orWhere('link', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $userbanner = UserBanner::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-banner.index', compact('userbanner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('userBanner.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('user.user-banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('userBanner.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'order' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $hitung = UserBanner::where('active', 1)->where('username',  env('USERNAME'))->where('user_id', env('USERID'))->count();

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } elseif ($hitung == '2') {
            Alert::warning('Maaf', 'Banner Aktif Sudah 2 Buah');

            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');



            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }


            $UserBanner = new UserBanner();
            $UserBanner->user_id = $user_id;
            $UserBanner->username = $username;
            $UserBanner->title = $request->get('title');
            $UserBanner->order = $request->get('order');
            $UserBanner->link = $request->get('link');
            $UserBanner->detail = $data;
            $UserBanner->active = $request->get('active');

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Banner_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1200, 809)->save(public_path('img/userbanner/') . $fileName);

                $UserBanner->foto = $fileName;

                $filenames = 'Banner_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 403)->save(public_path('img/userbanner/') . $filenames);

                $UserBanner->thumbnail = $filenames;
            }

            $UserBanner->save();
        }

        Alert::success('Sukses', 'Simpan Data User Banner');

        return redirect()->route('user-banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('userBanner.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userbanner = UserBanner::findOrFail($id);

        return view('user.user-banner.show', compact('userbanner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('userBanner.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userbanner = UserBanner::findOrFail($id);

        return view('user.user-banner.edit', compact('userbanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('userBanner.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'order' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');


            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }

            $UserBanner = UserBanner::findOrFail($id);
            $UserBanner->user_id = $user_id;
            $UserBanner->username = $username;
            $UserBanner->title = $request->get('title');
            $UserBanner->order = $request->get('order');
            $UserBanner->link = $request->get('link');
            $UserBanner->detail = $data;
            $UserBanner->active = $request->get('active');

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Banner_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1200, 809)->save(public_path('img/userbanner/') . $fileName);

                $UserBanner->foto = $fileName;

                $filenames = 'Banner_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 403)->save(public_path('img/userbanner/') . $filenames);

                $UserBanner->thumbnail = $filenames;
            }

            $UserBanner->save();
        }

        Alert::success('Sukses', 'Simpan Data User Banner');

        return redirect()->route('user-banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('userBanner.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserBanner::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Banner');

        return redirect()->route('user-banner.index');
    }

    /**
     * Update Active Banner
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        $Setting = UserBanner::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update Aktif User Banner');

        return redirect()->back();
    }
}
