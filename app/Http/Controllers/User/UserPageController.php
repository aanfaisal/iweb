<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserPage;
use App\Models\UserMenu;

class UserPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('userPage.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $userpage = UserPage::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('foto', 'LIKE', "%$keyword%")
                ->orWhere('thumbnail', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('detail', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $userpage = DB::table('user_pages')->where('user_pages.username',  env('USERNAME'))->where('user_pages.user_id', env('USERID'))
                ->where('user_pages.deleted_at', NULL)
                ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
                ->select('user_pages.*', 'user_menus.title AS namamenu')->get();
        }

        return view('user.user-page.index', compact('userpage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('userPage.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $menu = UserMenu::where('user_id', auth()->user()->id)->get();

        return view('user.user-page.create', compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('userPage.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'menu_id' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }


            $UserPage = new UserPage();
            $UserPage->user_id = $user_id;
            $UserPage->username = $username;
            $UserPage->menu_id = $request->get('menu_id');
            $UserPage->title = $request->get('title');
            $UserPage->detail = $data;

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Page_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1280, 375)->save(public_path('img/pageuser/') . $fileName);

                $UserPage->foto = $fileName;

                $filenames = 'Page_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(415, 125)->save(public_path('img/pageuser/') . $filenames);

                $UserPage->thumbnail = $filenames;
            }


            $UserPage->save();
        }

        Alert::success('Sukses', 'Simpan Data User Page');

        return redirect()->route('user-page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('userPage.list')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userpage = DB::table('user_pages')->where('user_pages.username',  env('USERNAME'))->where('user_pages.user_id', env('USERID'))
            ->where('user_pages.id', $id)
            ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
            ->select('user_pages.*', 'user_menus.title AS namamenu')->first();

        $menu = UserMenu::where('user_id', env('USERID'))->get();

        return view('user.user-page.show', compact('userpage', 'menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('userPage.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userpage = UserPage::findOrFail($id);

        $menu = UserMenu::where('user_id', env('USERID'))->get();

        return view('user.user-page.edit', compact('userpage', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'menu_id' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }

            $UserPage = UserPage::findOrFail($id);
            $UserPage->user_id = $user_id;
            $UserPage->username = $username;
            $UserPage->menu_id = $request->get('menu_id');
            $UserPage->title = $request->get('title');
            $UserPage->detail = $data;

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Page_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1280, 375)->save(public_path('img/pageuser/') . $fileName);

                $UserPage->foto = $fileName;

                $filenames = 'Page_Thumbnail_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(415, 125)->save(public_path('img/pageuser/') . $filenames);


                $UserPage->thumbnail = $filenames;
            }


            $UserPage->save();
        }

        Alert::success('Sukses', 'Update Data User Page');

        return redirect()->route('user-page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('userPage.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserPage::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Page');

        return redirect()->route('user-page.index');
    }

    /**
     * Update Active Page
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // $counter = UserFooter::where('active', 1)->get();
        // $count = $counter->count();

        $Setting = UserPage::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update Aktif User Page');

        return redirect()->back();
    }
}
