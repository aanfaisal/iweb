<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserFooter;

class UserFooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('userFooter.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $userfooter = UserFooter::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('detail', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $userfooter = UserFooter::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-footer.index', compact('userfooter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('userFooter.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('user.user-footer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('userFooter.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'username' => 'required',
            'title' => 'required',
            'detail' => 'required',
        ]);

        $hitung = UserFooter::where('active', 1)->where('username',  env('USERNAME'))->where('user_id', env('USERID'))->count();

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } elseif ($hitung == '2') {
            Alert::warning('Maaf', 'Footer Aktif Sudah 2 Buah');

            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }

            $UserFooter = new UserFooter();
            $UserFooter->user_id = $user_id;
            $UserFooter->username = $username;
            $UserFooter->title = $request->get('title');
            $UserFooter->detail = $data;

            $UserFooter->save();
        }

        Alert::success('Sukses', 'Simpan Data User Footer');

        return redirect()->route('user-footer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('userFooter.show')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userfooter = UserFooter::findOrFail($id);

        return view('user.user-footer.show', compact('userfooter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('userFooter.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userfooter = UserFooter::findOrFail($id);

        return view('user.user-footer.edit', compact('userfooter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('userFooter.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'username' => 'required',
            'title' => 'required',
            'detail' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');


            if (!empty($request->get('detail'))) {

                $data = $request->get('detail');
                //loading the html data from the summernote editor and select the img tags from it
                $dom = new \DomDocument();
                @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $images = $dom->getElementsByTagName('img');

                // foreach <img> in the submited message
                foreach ($images as $img) {
                    $src = $img->getAttribute('src');

                    // if the img source is 'data-url'
                    if (preg_match('/data:image/', $src)) {

                        // get the mimetype
                        preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                        $mimetype = $groups['mime'];

                        // Generating a random filename
                        $filename = uniqid();
                        $filepath = "/img/editor/$filename.$mimetype";

                        // @see http://image.intervention.io/api/
                        $image = Image::make($src)
                            ->encode($mimetype, 100)     // encode file to the specified mimetype
                            ->save(public_path($filepath));

                        $new_src = asset($filepath);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $new_src);
                    } // <!--endif
                } // <!--endforeach

                $data = $dom->saveHTML();
            } else {
                $data = "";
            }

            $UserFooter = UserFooter::findOrFail($id);
            $UserFooter->user_id = $user_id;
            $UserFooter->username = $username;
            $UserFooter->title = $request->get('title');
            $UserFooter->detail = $data;

            $UserFooter->save();
        }

        Alert::success('Sukses', 'Update Data User Footer');

        return redirect()->route('user-footer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('userFooter.delete')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserFooter::destroy($id);

        Alert::success('Sukses', 'Hapus Data User Footer');

        return redirect()->route('user-footer.index');
    }


    /**
     * Update Active Banner
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        $counter = UserFooter::where('active', 1)->get();

        $count = $counter->count();

        $Setting = UserFooter::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();


        Alert::success('Sukses', 'Update Aktif User Footer');


        return redirect()->back();
    }
}
