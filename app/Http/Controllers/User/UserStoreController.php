<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\Userstore;
use App\Models\UserMenu;
use App\Models\UserPage;

class UserStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('Userstore.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $userstore = UserStore::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('foto', 'LIKE', "%$keyword%")
                ->orWhere('detail', 'LIKE', "%$keyword%")
                ->orWhere('harga', 'LIKE', "%$keyword%")
                ->orWhere('link', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $userstore = UserStore::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-store.index', compact('userstore'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('Userstore.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $menu = UserMenu::where('user_id', env('USERID'))->where('active', 1)->get();
        $page = UserPage::where('user_id', env('USERID'))->where('active', 1)->get();

        return view('user.user-store.create', compact('menu', 'page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('Userstore.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'user_id' => 'required',
            'menu_id' => 'required',
            'page_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            // if (!empty($request->get('detail'))) {

            //     $data1 = $request->get('detail');
            //     //loading the html data from the summernote editor and select the img tags from it
            //     $dom1 = new \DomDocument();
            //     @$dom1->loadHtml($data1, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            //     $images = $dom1->getElementsByTagName('img');

            //     // foreach <img> in the submited message
            //     foreach ($images as $img) {
            //         $src = $img->getAttribute('src');

            //         // if the img source is 'data-url'
            //         if (preg_match('/data:image/', $src)) {

            //             // get the mimetype
            //             preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
            //             $mimetype = $groups['mime'];

            //             // Generating a random filename
            //             $filename = uniqid();
            //             $filepath = "/img/editor/$filename.$mimetype";

            //             // @see http://image.intervention.io/api/
            //             $image = Image::make($src)
            //                 ->encode($mimetype, 100)     // encode file to the specified mimetype
            //                 ->save(public_path($filepath));

            //             $new_src = asset($filepath);
            //             $img->removeAttribute('src');
            //             $img->setAttribute('src', $new_src);
            //         } // <!--endif
            //     } // <!--endforeach

            //     $data1 = $dom1->saveHTML();
            // } else {
            //     $data1 = "";
            // }


            $Userstore = new UserStore();
            $Userstore->user_id = $user_id;
            $Userstore->username = $username;
            $Userstore->menu_id = $request->get('menu_id');
            $Userstore->page_id = $request->get('page_id');
            $Userstore->detail = $request->get('detail');
            $Userstore->title = $request->get('title');
            $Userstore->harga = $request->get('harga');

            $Userstore->active = '0';


            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Product_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 600)->save(public_path('img/userstore/') . $fileName);

                $Userstore->foto = $fileName;
            }

            $Userstore->save();
        }

        Alert::success('Sukses', 'Simpan Data User Store Produk');

        return redirect()->route('user-store.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('Userstore.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userstore = UserStore::findOrFail($id);

        return view('user.user-store.show', compact('userstore'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('Userstore.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userstore = UserStore::findOrFail($id);
        $menu = UserMenu::where('user_id', env('USERID'))->where('active', 1)->get();
        $page = UserPage::where('user_id', env('USERID'))->where('active', 1)->get();

        return view('user.user-store.edit', compact('userstore', 'menu', 'page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'user_id' => 'required',
            'menu_id' => 'required',
            'page_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $user_id = env('USERID');
            $username = env('USERNAME');

            // if (!empty($request->get('detail'))) {

            //     $data1 = $request->get('detail');
            //     //loading the html data from the summernote editor and select the img tags from it
            //     $dom1 = new \DomDocument();
            //     @$dom1->loadHtml($data1, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            //     $images = $dom1->getElementsByTagName('img');

            //     // foreach <img> in the submited message
            //     foreach ($images as $img) {
            //         $src = $img->getAttribute('src');

            //         // if the img source is 'data-url'
            //         if (preg_match('/data:image/', $src)) {

            //             // get the mimetype
            //             preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
            //             $mimetype = $groups['mime'];

            //             // Generating a random filename
            //             $filename = uniqid();
            //             $filepath = "/img/editor/$filename.$mimetype";

            //             // @see http://image.intervention.io/api/
            //             $image = Image::make($src)
            //                 ->encode($mimetype, 100)     // encode file to the specified mimetype
            //                 ->save(public_path($filepath));

            //             $new_src = asset($filepath);
            //             $img->removeAttribute('src');
            //             $img->setAttribute('src', $new_src);
            //         } // <!--endif
            //     } // <!--endforeach

            //     $data1 = $dom1->saveHTML();
            // } else {
            //     $data1 = "";
            // }


            $Userstore = UserStore::findOrFail($id);
            $Userstore->user_id = $user_id;
            $Userstore->username = $username;
            $Userstore->menu_id = $request->get('menu_id');
            $Userstore->page_id = $request->get('page_id');
            $Userstore->detail = $request->get('detail');
            $Userstore->title = $request->get('title');
            $Userstore->harga = $request->get('harga');
            $Userstore->active = '0';


            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Product_' .  $username . '_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(600, 600)->save(public_path('img/userstore/') . $fileName);

                $Userstore->foto = $fileName;
            }

            $Userstore->save();
        }

        Alert::success('Sukses', 'Update Data Produk');

        return redirect()->route('user-store.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('Userstore.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        Userstore::destroy($id);

        Alert::success('Sukses', 'Hapus Data Produk');

        return redirect()->route('user-store.index');
    }

    /**
     * Update Active Store
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {

        $Setting = UserStore::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        Alert::success('Sukses', 'Update Aktifkan Produk');

        return redirect()->back();
    }
}
