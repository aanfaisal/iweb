<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\UserPostDetail;

class UserPostDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('userPostDetail.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $userpostdetail = UserPostDetail::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))
                ->orWhere('username', 'LIKE', "%$keyword%")
                ->orWhere('menu_id', 'LIKE', "%$keyword%")
                ->orWhere('page_id', 'LIKE', "%$keyword%")
                ->orWhere('foto', 'LIKE', "%$keyword%")
                ->orWhere('thumbnail', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('detail', 'LIKE', "%$keyword%")
                ->orWhere('link', 'LIKE', "%$keyword%")
                ->orWhere('category', 'LIKE', "%$keyword%")
                ->orWhere('file', 'LIKE', "%$keyword%")
                ->latest()->get($perPage);
        } else {
            $userpostdetail = UserPostDetail::where('username',  env('USERNAME'))
                ->where('user_id', env('USERID'))->get();
        }

        return view('user.user-post-detail.index', compact('userpostdetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('userPostDetail.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('user.user-post-detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        UserPostDetail::create($requestData);

        // if (!auth()->user()->can('userPostDetail.create')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            '' => 'required|max:255',
            '' => 'required',
            '' => 'required|numeric',
            '' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $UserPostDetail = new UserPostDetail();
            $UserPostDetail->UserPostDetail = $request->get('');
            $UserPostDetail->UserPostDetail = $request->get('');
            $UserPostDetail->UserPostDetail = $request->get('');

            $UserPostDetail->save();
        }

        alert()->success('Simpan Data UserPostDetail', 'Sukses')->autoclose(10000);

        return redirect('user-post-detail');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('userPostDetail.show')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userpostdetail = UserPostDetail::findOrFail($id);

        return view('user.user-post-detail.show', compact('userpostdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('userPostDetail.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $userpostdetail = UserPostDetail::findOrFail($id);

        return view('user.user-post-detail.edit', compact('userpostdetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $userpostdetail = UserPostDetail::findOrFail($id);
        $userpostdetail->update($requestData);

        // if (!auth()->user()->can('userPostDetail.edit')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            '' => 'required|max:255',
            '' => 'required',
            '' => 'required|numeric',
            '' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $UserPostDetail = new UserPostDetail();
            $UserPostDetail->UserPostDetail = $request->get('');
            $UserPostDetail->UserPostDetail = $request->get('');
            $UserPostDetail->UserPostDetail = $request->get('');

            $UserPostDetail->save();
        }

        alert()->success('Update Data UserPostDetail', 'Sukses')->autoclose(10000);

        return redirect('user-post-detail');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('userPostDetail.delete')) {
        //    abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        UserPostDetail::destroy($id);

        alert()->success('Hapus Data UserPostDetail', 'Sukses')->autoclose(10000);

        return redirect('user-post-detail');
    }
}
