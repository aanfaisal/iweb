<?php

namespace App\Http\Controllers\Main\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\SettingTentang;

class SettingTentangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('SettingTentang.index')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $settingtentang = SettingTentang::where('judul', 'LIKE', "%$keyword%")
                ->orWhere('keterangan', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $settingtentang = SettingTentang::latest()->get();
        }

        return view('main.setting-tentang.index', compact('settingtentang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('SettingTentang.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('main.setting-tentang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('SettingTentang.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'keterangan' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = $request->get('keterangan');
            //loading the html data from the summernote editor and select the img tags from it
            $dom = new \DomDocument();
            @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');

            // foreach <img> in the submited message
            foreach ($images as $img) {
                $src = $img->getAttribute('src');

                // if the img source is 'data-url'
                if (preg_match('/data:image/', $src)) {

                    // get the mimetype
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];

                    // Generating a random filename
                    $filename = uniqid();
                    $filepath = "/img/editor/$filename.$mimetype";

                    // @see http://image.intervention.io/api/
                    $image = Image::make($src)
                        ->encode($mimetype, 100)     // encode file to the specified mimetype
                        ->save(public_path($filepath));

                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } // <!--endif
            } // <!--endforeach

            $data = $dom->saveHTML();

            $SettingTentang = new SettingTentang();
            $SettingTentang->judul = $request->get('judul');
            $SettingTentang->keterangan = $data;

            $SettingTentang->save();
        }

        Alert::success('Sukses', 'Simpan Data Setting About Us');

        return redirect()->route('setting-tentang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('SettingTentang.list')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settingtentang = SettingTentang::findOrFail($id);

        return view('main.setting-tentang.show', compact('settingtentang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('SettingTentang.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settingtentang = SettingTentang::findOrFail($id);

        return view('main.setting-tentang.edit', compact('settingtentang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // if (!auth()->user()->can('SettingTentang.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'keterangan' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = $request->get('keterangan');
            //loading the html data from the summernote editor and select the img tags from it
            $dom = new \DomDocument();
            @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');

            // foreach <img> in the submited message
            foreach ($images as $img) {
                $src = $img->getAttribute('src');

                // if the img source is 'data-url'
                if (preg_match('/data:image/', $src)) {

                    // get the mimetype
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];

                    // Generating a random filename
                    $filename = uniqid();
                    $filepath = "/img/editor/$filename.$mimetype";

                    // @see http://image.intervention.io/api/
                    $image = Image::make($src)
                        ->encode($mimetype, 100)     // encode file to the specified mimetype
                        ->save(public_path($filepath));

                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } // <!--endif
            } // <!--endforeach

            $data = $dom->saveHTML();

            $SettingTentang = SettingTentang::findOrFail($id);
            $SettingTentang->judul = $request->get('judul');
            $SettingTentang->keterangan = $data;

            $SettingTentang->save();
        }

        Alert::success('Sukses', 'Update Data Setting About Us');

        return redirect()->route('setting-tentang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('SettingTentang.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        SettingTentang::destroy($id);

        Alert::success('Sukses', 'Hapus Data Setting About Us');

        return redirect()->route('setting-tentang.index');
    }

    /**
     * Update Active Menu
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // Set ALL records to a status of 0
        DB::table('setting_tentangs')->where('active', 1)->update(['active' => 0]);

        $SettingTentang = SettingTentang::findOrFail($id);
        $SettingTentang->active = $request->get('active');
        $SettingTentang->save();

        return redirect()->back();
    }
}
