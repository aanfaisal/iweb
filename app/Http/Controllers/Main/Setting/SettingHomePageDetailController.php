<?php

namespace App\Http\Controllers\Main\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;

use App\Models\SettingHomePageDetail;

class SettingHomePageDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('SettingHomePageDetail.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $settinghomepagedetail = SettingHomePageDetail::where('settinghome_id', 'LIKE', "%$keyword%")
                ->orWhere('slidernumber', 'LIKE', "%$keyword%")
                ->orWhere('keterangan1', 'LIKE', "%$keyword%")
                ->orWhere('keterangan2', 'LIKE', "%$keyword%")
                ->orWhere('keterangan3', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $settinghomepagedetail = SettingHomePageDetail::latest()->paginate($perPage);
        }

        return view('main.setting-home-page-detail.index', compact('settinghomepagedetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('SettingHomePageDetail.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('main.setting-home-page-detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {


        $requestData = $request->all();

        SettingHomePageDetail::create($requestData);

        // if (!auth()->user()->can('SettingHomePageDetail.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            '' => 'required|max:255',
            '' => 'required',
            '' => 'required|numeric',
            '' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $SettingHomePageDetail = new SettingHomePageDetail();
            $SettingHomePageDetail->SettingHomePageDetail = $request->get('');
            $SettingHomePageDetail->SettingHomePageDetail = $request->get('');
            $SettingHomePageDetail->SettingHomePageDetail = $request->get('');

            $SettingHomePageDetail->save();
        }

        alert()->success('Simpan Data SettingHomePageDetail', 'Sukses')->autoclose(10000);

        return redirect('setting-home-page-detail');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('SettingHomePageDetail.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settinghomepagedetail = SettingHomePageDetail::findOrFail($id);

        return view('main.setting-home-page-detail.show', compact('settinghomepagedetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('SettingHomePageDetail.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settinghomepagedetail = SettingHomePageDetail::findOrFail($id);

        return view('main.setting-home-page-detail.edit', compact('settinghomepagedetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $settinghomepagedetail = SettingHomePageDetail::findOrFail($id);
        $settinghomepagedetail->update($requestData);

        // if (!auth()->user()->can('SettingHomePageDetail.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            '' => 'required|max:255',
            '' => 'required',
            '' => 'required|numeric',
            '' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $SettingHomePageDetail = new SettingHomePageDetail();
            $SettingHomePageDetail->SettingHomePageDetail = $request->get('');
            $SettingHomePageDetail->SettingHomePageDetail = $request->get('');
            $SettingHomePageDetail->SettingHomePageDetail = $request->get('');

            $SettingHomePageDetail->save();
        }

        alert()->success('Update Data SettingHomePageDetail', 'Sukses')->autoclose(10000);

        return redirect('setting-home-page-detail');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('SettingHomePageDetail.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        SettingHomePageDetail::destroy($id);

        alert()->success('Hapus Data SettingHomePageDetail', 'Sukses')->autoclose(10000);

        return redirect('setting-home-page-detail');
    }
}
