<?php

namespace App\Http\Controllers\Main\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\SettingTestimoni;

class SettingTestimoniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('SettingTestimoni.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $settingtestimoni = SettingTestimoni::where('judul', 'LIKE', "%$keyword%")
                ->orWhere('keterangan', 'LIKE', "%$keyword%")
                ->orWhere('foto', 'LIKE', "%$keyword%")
                ->latest()->get($perPage);
        } else {
            $settingtestimoni = SettingTestimoni::orderBy('id')->get();
        }

        return view('main.setting-testimoni.index', compact('settingtestimoni'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('SettingTestimoni.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('main.setting-testimoni.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'keterangan' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $data = $request->get('keterangan');
            //loading the html data from the summernote editor and select the img tags from it
            $dom = new \DomDocument();
            @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');

            // foreach <img> in the submited message
            foreach ($images as $img) {
                $src = $img->getAttribute('src');

                // if the img source is 'data-url'
                if (preg_match('/data:image/', $src)) {

                    // get the mimetype
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];

                    // Generating a random filename
                    $filename = uniqid();
                    $filepath = "/img/editor/$filename.$mimetype";

                    // @see http://image.intervention.io/api/
                    $image = Image::make($src)
                        ->encode($mimetype, 100)     // encode file to the specified mimetype
                        ->save(public_path($filepath));

                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } // <!--endif
            } // <!--endforeach

            $data = $dom->saveHTML();

            $SettingTestimoni = new SettingTestimoni();
            $SettingTestimoni->judul = $request->get('judul');
            $SettingTestimoni->keterangan = $data;

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Testimoni_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(640, 640)->save(public_path('img/testimoni/') . $fileName);

                $SettingTestimoni->foto = $fileName;
            }


            $SettingTestimoni->save();
        }

        Alert::success('Sukses', 'Simpan Data Testimoni');

        return redirect()->route('setting-testimoni.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('SettingTestimoni.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settingtestimoni = SettingTestimoni::findOrFail($id);

        return view('main.setting-testimoni.show', compact('settingtestimoni'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('SettingTestimoni.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settingtestimoni = SettingTestimoni::findOrFail($id);

        return view('main.setting-testimoni.edit', compact('settingtestimoni'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'judul' => 'required|max:255',
            'keterangan' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = $request->get('keterangan');
            //loading the html data from the summernote editor and select the img tags from it
            $dom = new \DomDocument();
            @$dom->loadHtml($data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getElementsByTagName('img');

            // foreach <img> in the submited message
            foreach ($images as $img) {
                $src = $img->getAttribute('src');

                // if the img source is 'data-url'
                if (preg_match('/data:image/', $src)) {

                    // get the mimetype
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $mimetype = $groups['mime'];

                    // Generating a random filename
                    $filename = uniqid();
                    $filepath = "/img/editor/$filename.$mimetype";

                    // @see http://image.intervention.io/api/
                    $image = Image::make($src)
                        ->encode($mimetype, 100)     // encode file to the specified mimetype
                        ->save(public_path($filepath));

                    $new_src = asset($filepath);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                } // <!--endif
            } // <!--endforeach

            $data = $dom->saveHTML();

            $SettingTestimoni = SettingTestimoni::findOrFail($id);
            $SettingTestimoni->judul = $request->get('judul');
            $SettingTestimoni->keterangan = $data;

            if (!empty($request->file('foto'))) {
                $imageData = $request->file('foto');

                $fileName = 'Testimoni_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(640, 640)->save(public_path('img/testimoni/') . $fileName);

                $SettingTestimoni->foto = $fileName;
            }

            $SettingTestimoni->save();
        }

        Alert::success('Sukses', 'Update Data Testimoni');

        return redirect()->route('setting-testimoni.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('SettingTestimoni.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        SettingTestimoni::destroy($id);

        Alert::success('Sukses', 'Hapus Data Testimoni');

        return redirect()->route('setting-testimoni.index');
    }
}
