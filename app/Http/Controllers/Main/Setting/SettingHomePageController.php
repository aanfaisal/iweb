<?php

namespace App\Http\Controllers\Main\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\SettingHomePage;
use App\Models\SettingHomePageDetail;

class SettingHomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('settingHomepage.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $settinghomepage = SettingHomePage::where('sliderbackground', 'LIKE', "%$keyword%")
                ->orWhere('theme', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $settinghomepage = SettingHomePage::All();
        }

        return view('main.setting-home-page.index', compact('settinghomepage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('settingHomepage.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settinghomepagedetail = '';

        return view('main.setting-home-page.create', compact('settinghomepagedetail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'theme' => 'required',
            'kodenegara' => 'required',
            'no_whatapps' => 'required',
            'sliderbackground' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $keterangan1 = $keterangan2 = $keterangan3 = $slidernumber = [];

            $keterangan1 =  array_filter($request->get('keterangan1'));
            $keterangan2 =   array_filter($request->get('keterangan2'));
            $keterangan3 =  array_filter($request->get('keterangan3'));
            $slidernumber =   array_filter($request->get('slidernumber'));

            $indexslider = ['rs-3045', 'rs-2', 'rs-3'];

            $SettingHomePage = new SettingHomePage();
            $SettingHomePage->theme = $request->get('theme');
            $SettingHomePage->caption = $request->get('caption');
            $SettingHomePage->kodenegara = $request->get('kodenegara');
            $SettingHomePage->no_whatapps = $request->get('no_whatapps');


            if (!empty($request->file('sliderbackground'))) {
                $imageData = $request->file('sliderbackground');

                $fileName = 'Slider_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1280, 720)->save(public_path('img/mainslider/') . $fileName);

                $SettingHomePage->sliderbackground = $fileName;
            }

            if (!empty($request->file('slidermobile'))) {
                $imageThumb = $request->file('slidermobile');

                $filenames = 'Slider_Thumbnail_' . date('YmdHis') . '.png';

                Image::make($imageThumb)->resize(461, 1024)->save(public_path('img/mainslider/') . $filenames);

                $SettingHomePage->slidermobile = $filenames;
            }

            $SettingHomePage->save();

            $IdSettingHomepage = SettingHomePage::latest()->first();

            // Simpan Data Detail
            for ($a = 0; $a < count($keterangan1); $a++) {
                if (!empty($keterangan1[$a])) {

                    $SettingHomePageDetail = new SettingHomePageDetail();
                    $SettingHomePageDetail->settinghome_id = $IdSettingHomepage->id;
                    $SettingHomePageDetail->slidernumber = $slidernumber[$a];
                    $SettingHomePageDetail->indexslider = $indexslider[$a];
                    $SettingHomePageDetail->keterangan1 = $keterangan1[$a];
                    $SettingHomePageDetail->keterangan2 = $keterangan2[$a];
                    $SettingHomePageDetail->keterangan3 = $keterangan3[$a];

                    $SettingHomePageDetail->save();
                }
            }
        }

        Alert::success('Sukses', 'Simpan Data SettingHomePage');

        return redirect()->route('setting-homepage.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('settingHomepage.list')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settinghomepage = SettingHomePage::findOrFail($id);
        $settinghomepagedetail = SettingHomePageDetail::where('settinghome_id', $id)->get();

        return view('main.setting-home-page.show', compact('settinghomepage', 'settinghomepagedetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('settingHomepage.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settinghomepage = SettingHomePage::findOrFail($id);
        $settinghomepagedetail = SettingHomePageDetail::where('settinghome_id', $id)->get();

        return view('main.setting-home-page.edit', compact('settinghomepage', 'settinghomepagedetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'theme' => 'required',
            'kodenegara' => 'required',
            'no_whatapps' => 'required',
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $keterangan1 = $keterangan2 = $keterangan3 = $slidernumber = [];

            $keterangan1 =  array_filter($request->get('keterangan1'));
            $keterangan2 =   array_filter($request->get('keterangan2'));
            $keterangan3 =  array_filter($request->get('keterangan3'));
            $slidernumber =   array_filter($request->get('slidernumber'));

            $indexslider = ['rs-3045', 'rs-2', 'rs-3'];

            $SettingHomePage = SettingHomePage::findOrFail($id);
            $SettingHomePage->theme = $request->get('theme');
            $SettingHomePage->caption = $request->get('caption');
            $SettingHomePage->kodenegara = $request->get('kodenegara');
            $SettingHomePage->no_whatapps = $request->get('no_whatapps');

            if (!empty($request->file('sliderbackground'))) {
                $imageData = $request->file('sliderbackground');

                $fileName = 'Slider_' . date('YmdHis') . '.png';

                Image::make($imageData)->resize(1280, 720)->save(public_path('img/mainslider/') . $fileName);

                $SettingHomePage->sliderbackground = $fileName;
            }

            if (!empty($request->file('slidermobile'))) {
                $imageThumb = $request->file('slidermobile');

                $filenames = 'Slider_Thumbnail_' . date('YmdHis') . '.png';

                Image::make($imageThumb)->resize(461, 1024)->save(public_path('img/mainslider/') . $filenames);

                $SettingHomePage->slidermobile = $filenames;
            }

            $SettingHomePage->save();

            // Hapus Data Detail Lama
            for ($a = 0; $a < count($keterangan1); $a++) {
                if (!empty($keterangan1[$a])) {

                    SettingHomePageDetail::where('settinghome_id', $id)->delete();
                }
            }

            // Simpan Data Detail Baru
            for ($a = 0; $a < count($keterangan1); $a++) {
                if (!empty($keterangan1[$a])) {

                    $SettingHomePageDetail = new SettingHomePageDetail();
                    $SettingHomePageDetail->settinghome_id = $id;
                    $SettingHomePageDetail->slidernumber = $slidernumber[$a];
                    $SettingHomePageDetail->indexslider = $indexslider[$a];
                    $SettingHomePageDetail->keterangan1 = $keterangan1[$a];
                    $SettingHomePageDetail->keterangan2 = $keterangan2[$a];
                    $SettingHomePageDetail->keterangan3 = $keterangan3[$a];

                    $SettingHomePageDetail->save();
                }
            }
        }

        Alert::success('Sukses', 'Update Data SettingHomePage');

        return redirect()->route('setting-homepage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('settingHomepage.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        SettingHomePage::destroy($id);

        Alert::success('Sukses', 'Hapus Data SettingHomePage');

        return redirect()->route('setting-homepage.index');
    }

    /**
     * Update Active Menu
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // Set ALL records to a status of 0
        DB::table('setting_home_pages')->where('active', 1)->update(['active' => 0]);

        $Setting = SettingHomePage::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        return redirect()->back();
    }
}
