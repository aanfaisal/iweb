<?php

namespace App\Http\Controllers\Main\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;
use Image;

use App\Models\SettingBiaya;
use App\Models\SettingBiayaDetail;

class SettingBiayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // if (!auth()->user()->can('settingBiaya.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $settingbiaya = SettingBiaya::where('nama', 'LIKE', "%$keyword%")
                ->orWhere('ket', 'LIKE', "%$keyword%")
                ->orWhere('warna', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {

            $settingbiaya = SettingBiaya::orderBy('id')->get();
        }

        return view('main.setting-biaya.index', compact('settingbiaya'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // if (!auth()->user()->can('settingBiaya.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        return view('main.setting-biaya.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // if (!auth()->user()->can('settingBiaya.create')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $validator = Validator::make($request->all(), [
            'namapaket' => 'required|max:255',
            'warna' => 'required',
            'hargaasli' => 'required|numeric',
            'hargadiskon' => 'required|numeric',
            'jumlahmenu' => 'required'
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $detail = [];

            $detail =  array_filter($request->get('detail'));

            $SettingBiaya = new SettingBiaya();
            $SettingBiaya->namapaket = $request->get('namapaket');
            $SettingBiaya->warna = $request->get('warna');
            $SettingBiaya->hargaasli = $request->get('hargaasli');
            $SettingBiaya->hargadiskon = $request->get('hargadiskon');
            $SettingBiaya->jumlahmenu = $request->get('jumlahmenu');
            $SettingBiaya->ket = $request->get('ket');

            $SettingBiaya->save();

            $IdSetting = SettingBiaya::latest()->first();

            // Simpan Data Detail
            for ($a = 0; $a < count($detail); $a++) {
                if (!empty($detail[$a])) {

                    $SettingBiayaDetail = new SettingBiayaDetail();

                    $SettingBiayaDetail->settingbiaya_id = $IdSetting->id;
                    $SettingBiayaDetail->detail = $detail[$a];
                    $SettingBiayaDetail->ket = $detail[$a];

                    $SettingBiayaDetail->save();
                }
            }
        }

        Alert::success('Sukses', 'Simpan Data SettingBiaya');

        return redirect()->route('setting-biaya.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        // if (!auth()->user()->can('settingBiaya.show')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settingbiaya = SettingBiaya::findOrFail($id);
        $settingbiayadetail = SettingBiayaDetail::where('settingbiaya_id', $id)->get();

        return view('main.setting-biaya.show', compact('settingbiaya', 'settingbiayadetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // if (!auth()->user()->can('settingBiaya.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        $settingbiaya = SettingBiaya::findOrFail($id);
        $settingbiayadetail = SettingBiayaDetail::where('settingbiaya_id', $id)->get();

        return view('main.setting-biaya.edit', compact('settingbiaya', 'settingbiayadetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'namapaket' => 'required|max:255',
            'warna' => 'required',
            'hargaasli' => 'required|numeric',
            'hargadiskon' => 'required|numeric',
            'jumlahmenu' => 'required'
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $detail = [];

            $detail =  array_filter($request->get('detail'));

            $SettingBiaya = SettingBiaya::findOrFail($id);
            $SettingBiaya->namapaket = $request->get('namapaket');
            $SettingBiaya->warna = $request->get('warna');
            $SettingBiaya->hargaasli = $request->get('hargaasli');
            $SettingBiaya->hargadiskon = $request->get('hargadiskon');
            $SettingBiaya->jumlahmenu = $request->get('jumlahmenu');
            $SettingBiaya->ket = $request->get('ket');

            $SettingBiaya->save();

            // Hapus Data Detail Lama
            for ($a = 0; $a < count($detail); $a++) {
                if (!empty($detail[$a])) {

                    SettingBiayaDetail::where('settingbiaya_id', $id)->delete();
                }
            }

            // Simpan Data Detail Baru
            for ($a = 0; $a < count($detail); $a++) {
                if (!empty($detail[$a])) {
                    $SettingBiayaDetail = new SettingBiayaDetail();
                    $SettingBiayaDetail->settingbiaya_id = $id;
                    $SettingBiayaDetail->detail = $detail[$a];
                    $SettingBiayaDetail->ket = $detail[$a];
                    $SettingBiayaDetail->save();
                }
            }
        }

        Alert::success('Sukses', 'Udpate Data SettingBiaya');

        return redirect()->route('setting-biaya.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        // if (!auth()->user()->can('settingBiaya.delete')) {
        //     abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        // }

        SettingBiaya::destroy($id);

        Alert::success('Sukses', 'Hapus Data SettingBiaya');

        return redirect()->route('setting-biaya.index');
    }

    /**
     * Update Active Menu
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        // Set ALL records to a status of 0
        DB::table('setting_biayas')->where('active', 1)->update(['active' => 0]);

        $Setting = SettingBiaya::findOrFail($id);
        $Setting->active = $request->get('active');
        $Setting->save();

        return redirect()->back();
    }
}
