<?php

namespace App\Http\Controllers\Main\Manage;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Image;
use Carbon\Carbon;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\User;
use App\Models\UserMenu;
use App\Models\UserPage;
use App\Models\SettingBiaya;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $user = User::where('name', 'LIKE', "%$keyword%")
                ->orWhere('username', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $user = User::orderBy('id')->paginate($perPage);
        }

        return view('main.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $roles_array = Role::all()->pluck('name', 'id');
        $roles = [];
        foreach ($roles_array as $key => $value) {
            $roles[$key] = $value;
        }


        $paket = SettingBiaya::All();

        return view('main.user.create', compact('roles', 'paket'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'username' => ['required', 'string', 'max:100', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            //Buat User
            $user = new User;
            $user->name = $request->get('nama');
            $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->no_whatapps = $request->get('no_whatapps');
            $user->paket = $request->get('paket');
            $user->webaddress = $request->get('webaddress') . '.iweb.co.id';
            $user->keterangan = $request->get('keterangan');
            $user->birthday = $request->get('birthday');


            if (!empty($request->get('password'))) {
                $user->password = Hash::make($request->get('password'));
            }

            $user->assignRole($request->get('role'));


            if ($request->hasFile('foto')) {
                $photo = $request->file('foto');

                $namafoto = time() . '.' . $photo->getClientOriginalExtension();

                $destinationPath = public_path('/img/imageuser/');

                $imgFile = Image::make($photo->getRealPath());

                // resize image to fixed size
                $imgFile->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $namafoto);

                $user->image = $namafoto;
            }

            $user->save();
        }

        Alert::success('Sukses', 'Simpan Data User');

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        if (!auth()->user()->can('user.list')) {
            abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        }

        $user = User::findOrFail($id);

        return view('main.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $user = User::findOrFail($id);


        $roles_array = Role::all()->pluck('name', 'id');
        $roles = [];
        foreach ($roles_array as $key => $value) {
            $roles[$key] = $value;
        }

        $misi = $user->getAllPermissions();
        $permission = [];
        foreach ($misi as $perm) {
            $permission[] = $perm->name;
        }

        $role = $user->roles->first();

        $paket = SettingBiaya::All();

        return view('main.user.edit', compact('role', 'user', 'roles', 'permission', 'paket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'username' => 'required',
            'password' => ['required', 'string', 'min:8'],
        ]);

        if ($validator->fails()) {
            toast('Inputan Ada Yang Salah ! Silahkan Cek Kembali', 'warning');
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $user = User::findOrFail($id);
            $user->name = $request->get('nama');
            $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->no_whatapps = $request->get('no_whatapps');
            $user->paket = $request->get('paket');
            $user->webaddress = $request->get('webaddress') . '.iweb.co.id';
            $user->keterangan = $request->get('keterangan');
            $user->birthday = $request->get('birthday');

            if (!empty($request->get('password'))) {
                $user->password = Hash::make($request->get('password'));
            }

            $user->assignRole($request->get('role'));

            if ($request->hasFile('foto')) {
                $photo = $request->file('foto');

                $namafoto = time() . '.' . $photo->getClientOriginalExtension();

                $destinationPath = public_path('/img/imageuser/');

                $imgFile = Image::make($photo->getRealPath());

                // resize image to fixed size
                $imgFile->resize(150, 150, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $namafoto);

                $user->image = $namafoto;
            }

            $user->save();
        }

        Alert::success('Sukses', 'Update Data User');

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('user.delete')) {
            abort(403, 'Anda Tidak Diijinkan Mengakses Halaman Ini.');
        }

        User::destroy($id);

        Alert::success('Sukses', 'Hapus Data User');

        return redirect()->route('user.index');
    }

    /**
     * Shows profile of logged in user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProfile($id)
    {
        $user = User::findOrFail($id);

        $roles_array = Role::all()->pluck('name', 'id');
        $roles = [];
        foreach ($roles_array as $key => $value) {
            $roles[$key] = $value;
        }

        $misi = $user->getAllPermissions();
        $permission = [];
        foreach ($misi as $perm) {
            $permission[] = $perm->name;
        }

        $role = auth()->user()->roles->first();

        return view('main.user.profile')->with(compact('role', 'roles', 'permission', 'user'));
    }

    /**
     * updates user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $role_id = $request->input('role');
        $user_role = $user->roles->first();

        if ($user_role->id != $role_id) {
            $user->removeRole($user_role->name);

            $role = Role::findOrFail($role_id);
            $user->assignRole($role->name);
        }

        $permissions = $request->input('permissions');

        //$user->revokePermissionTo($permissions);
        $user->syncPermissions($permissions);

        $user->name = $request->get('nama');
        $user->no_whatapps = $request->get('no_whatapps');
        $user->webaddress = $request->get('webaddress') . '.iweb.co.id';
        $user->keterangan = $request->get('keterangan');
        $user->birthday = $request->get('birthday');

        if (!empty($request->input('password'))) {
            $user->password =  Hash::make($request->input('password'));
        }

        $user->save();

        Alert::success('Sukses', 'Update Data User');


        return redirect()->back();
    }

    /**
     * Store Foto User.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storefotoUpload(Request $request, $id)
    {
        if (!auth()->user()->can('user.edit')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
        ]);

        if ($request->hasFile('image')) {
            $photo = $request->file('image');

            $input['image'] = time() . '.' . $photo->getClientOriginalExtension();

            $destinationPath = public_path('/img/imageuser/');

            $imgFile = Image::make($photo->getRealPath());

            // resize image to fixed size
            $imgFile->resize(150, 150, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $input['image']);


            DB::table('users')->where('id', $id)->update(['image' => $input['image']]);
        }

        Alert::success('Sukses', 'Upload Foto User');

        return redirect()->back();
    }


    /**
     * Update Active User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request, $id)
    {
        $User = User::findOrFail($id);
        $username = $User->username;

        $link = $User->webaddress;

        $User->paket_aktif = $request->get('active');
        $User->paket_tanggal = Carbon::now();
        $User->save();

        // Membuat Website Aktif + Menu2 nya Otomatis

        // Meng Insert MENU :
        $data = [
            [
                'user_id' => $id,
                'username' => $username,
                'icon' => 'fas fa-home',
                'title' => 'Home',
                'detail' => 'Menu Home Website',
                'typepage' => 'homepage',
                'order' => '0',
                'link' => 'http://' . $link,
                'alias' => 'home_' . $id . '_' . $username,
                'active' => '1',
            ],
            [
                'user_id' => $id,
                'username' => $username,
                'icon' => 'far fa-credit-card',
                'title' => 'Buka Toko',
                'detail' => 'Menu Produk Website',
                'typepage' => 'storepage',
                'order' => '1',
                'link' => 'http://' . $link . '/produk',
                'alias' => 'produk_' . $id . '_' . $username,
                'active' => '1',
            ],
            [
                'user_id' => $id,
                'username' => $username,
                'icon' => 'fa fa-university',
                'title' => 'Tentang Kami',
                'detail' => 'Menu Tentang Kamu',
                'typepage' => 'tentangpage',
                'order' => '2',
                'link' => 'http://' . $link . '/tentangkami',
                'alias' => 'tentang_' . $id . '_' . $username,
                'active' => '1',
            ],
            [
                'user_id' => $id,
                'username' => $username,
                'icon' => 'fa fa-university',
                'title' => 'Kontak Kami',
                'detail' => 'Menu Kontak Kamu',
                'typepage' => 'kontakpage',
                'order' => '3',
                'link' => 'http://' . $link . '/kontak',
                'alias' => 'kontak_' . $id . '_' . $username,
                'active' => '1',
            ],
            [
                'user_id' => $id,
                'username' => $username,
                'icon' => 'fa fa-comments',
                'title' => 'Testimoni',
                'detail' => 'Menu Testimoni Kamu',
                'typepage' => 'testimonipage',
                'order' => '4',
                'link' => 'http://' . $link . '/testimoni',
                'alias' => 'testimoni_' . $id . '_' . $username,
                'active' => '1',
            ],
            //...
        ];

        UserMenu::insert($data);


        Alert::success('Sukses', 'Update Status User');

        return redirect()->back();
    }
}
