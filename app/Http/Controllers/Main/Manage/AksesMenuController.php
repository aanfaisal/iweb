<?php

namespace App\Http\Controllers\Main\Manage;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Image;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AksesMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->can('aksesmenu.list')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $keyword = $request->get('search');
        $perPage = 18;

        if (!empty($keyword)) {
            $role = Role::where('nama', 'LIKE', "%$keyword%")
                ->latest()->get();
            $permission = Permission::where('nama', 'LIKE', "%$keyword%")
                ->latest()->get();
        } else {
            $role = Role::All();
            $permission = Permission::All();
            $user =  User::All();
        }

        return view('admin.akses.index', compact('role', 'permission', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aksesmenuedit($id)
    {
        if (!auth()->user()->can('aksesmenu.create')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $user = User::findOrFail($id);

        $roles_array = Role::all();
        $roles = [];
        foreach ($roles_array as $key => $value) {
            $roles[$key] = $value;
        }

        $misi = $user->getAllPermissions();
        $permission = [];
        foreach ($misi as $perm) {
            $permission[] = $perm->name;
        }

        return view('admin.akses.edit')->with(compact('roles', 'permission', 'user'));
    }

    /**
     * Update the specified resource
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aksesmenuupdate(Request $request, $id)
    {
        // if (!auth()->user()->can('aksesmenu.edit')) {
        //     abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        // }
        $id = $request->get('id');

        $user = User::findOrFail($id);

        // $role_id = $request->get('role');
        // $user_role = $user->roles->first();

        // if ($user_role === null){
        //     $role = Role::findOrFail($role_id);
        //     $user->assignRole($role->name);
        // }
        // else if ($user_role->id != $role_id) {
        //     $user->removeRole($user_role->name);

        //     $role = Role::findOrFail($role_id);
        //     $user->assignRole($role->name);
        // }

        $permissions = $request->get('permissions');

        $user->syncPermissions($permissions);

        Alert::success('Sukses', 'Mengupdate Data User');

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createpermission()
    {
        if (!auth()->user()->can('aksesmenu.create')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $roles_array = Role::all();
        $roles = [];
        foreach ($roles_array as $key => $value) {
            $roles[$key] = $value;
        }

        $permission = Permission::all();

        return view('admin.akses.createpermission')->with(compact('roles', 'permission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createrole()
    {
        if (!auth()->user()->can('aksesmenu.create')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $roles_array = Role::all();
        $roles = [];
        foreach ($roles_array as $key => $value) {
            $roles[$key] = $value;
        }

        $permission = Permission::all();

        return view('admin.akses.createrole')->with(compact('roles', 'permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function storepermisson(Request $request)
    {
        if (!auth()->user()->can('aksesmenu.create')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $permission = $request->get('permission');

        Permission::create(['name' => $permission]);


        Alert::success('Sukses', 'Menambahkan Permission Baru');

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function storerole(Request $request)
    {
        if (!auth()->user()->can('aksesmenu.create')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $role = $request->get('role');

        Role::create(['name' => $role]);

        Alert::success('Sukses', 'Menambahkan Role Baru');

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function editpermission($id)
    {
        if (!auth()->user()->can('aksesmenu.edit')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $permission = Permission::findOrFail($id);

        return view('admin.akses.editpermission')->with(compact('permission'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function editrole($id)
    {
        if (!auth()->user()->can('aksesmenu.edit')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $role = Role::findOrFail($id);

        return view('admin.akses.editrole')->with(compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function updatepermission(Request $request, $id)
    {
        if (!auth()->user()->can('aksesmenu.edit')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $permission = Permission::findOrFail($id);
        $permission->name = $request->get('name');
        $permission->save();

        Alert::success('Sukses', 'Mengupdate Permission');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function updaterole(Request $request, $id)
    {
        if (!auth()->user()->can('aksesmenu.edit')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        $role = Role::findOrFail($id);
        $role->name = $request->get('name');
        $role->save();

        Alert::success('Sukses', 'Mengupdate Role');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroypermission($id)
    {
        if (!auth()->user()->can('aksesmenu.delete')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        Permission::destroy($id);

        Alert::success('Sukses', 'Menghapus Permission');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyrole($id)
    {
        if (!auth()->user()->can('aksesmenu.delete')) {
            abort(403, 'Anda Tidak Diijinkan Melakukan Aksi Ini.');
        }

        Role::destroy($id);

        Alert::success('Sukses', 'Menghapus Permission');

        return redirect()->back();
    }
}
