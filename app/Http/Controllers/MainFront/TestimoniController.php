<?php

namespace App\Http\Controllers\MainFront;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Image;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\SettingTestimoni;

class TestimoniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $testimoni = SettingTestimoni::All();


        return view('mainfront.new.testimoni', compact('testimoni'));
    }
}
