<?php

namespace App\Http\Controllers\MainFront;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Image;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\SettingBiaya;
use App\Models\SettingBiayaDetail;

class BiayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $biaya = SettingBiaya::All();

        return view('mainfront.princing', compact('biaya'));
    }

    /**
     * Cari Biaya Detail
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return $query
     */
    public static function cari_detail($id)
    {
        if ($query = SettingBiayaDetail::where('settingbiaya_id', $id)->get()) {
            if (!empty($query)) {
                return $query;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } //--
}
