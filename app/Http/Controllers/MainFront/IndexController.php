<?php

namespace App\Http\Controllers\MainFront;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Image;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\SettingHomePage;
use App\Models\SettingHomePageDetail;
use App\Models\SettingTestimoni;


use App\Models\UserBanner;
use App\Models\UserFooter;
use App\Models\UserMenu;
use App\Models\UserPage;
use App\Models\UserSettingUmum;
use App\Models\UserSlider;
use App\Models\UserSocialMedia;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $settinghomepage = SettingHomePage::All();

        $testimoni = SettingTestimoni::All();

        $user_id = '2';
        $username = 'user';

        $menu = UserMenu::where('username', $username)->where('user_id', $user_id)->where('active', 1)->OrderBy('order', 'ASC')->get();

        $banner = UserBanner::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $slider = UserSlider::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $footer = UserFooter::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $socialmedia = UserSocialMedia::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $page = UserPage::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $settingumum = UserSettingUmum::where('username', $username)->where('user_id', $user_id)->where('active', 1)->first();

        if (!empty($settingumum)) {
            $theme = $settingumum->tema;
        } else {
            $theme = 'light';
        }

        if ($theme == 'dark') {
            $warna = 'bg3';
        } elseif ($theme == 'light') {
            $warna = 'bg0';
        } else {
            $warna = 'bg0';
        }

        return view('mainfront.new.homepage', compact('testimoni', 'settinghomepage', 'menu', 'slider', 'banner', 'footer', 'page', 'socialmedia', 'settingumum', 'warna'));
    }
}
