<?php

namespace App\Http\Controllers\UserFront;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
use Image;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\SettingBiaya;
use App\Models\SettingBiayaDetail;

use App\Models\User;
use App\Models\UserBanner;
use App\Models\UserFooter;
use App\Models\UserMenu;
use App\Models\UserPage;
use App\Models\UserSettingUmum;
use App\Models\UserSlider;
use App\Models\UserSocialMedia;
use App\Models\UserPost;
use App\Models\UserPostDetail;
use App\Models\UserText;

class UserFrontendMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function menu(Request $request, $link)
    {
        $user_id = env('USERID');
        $username = env('USERNAME');

        $settingumum = UserSettingUmum::where('username', $username)->where('user_id', $user_id)->where('active', 1)->first();

        $menu = UserMenu::where('username', $username)->where('user_id', $user_id)->where('active', 1)->OrderBy('order', 'ASC')->get();

        $footer = UserFooter::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $banner = UserBanner::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $slider = UserSlider::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $socialmedia = UserSocialMedia::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $itemtext = UserText::where('username', $username)->where('user_id', $user_id)->where('active', 1)->latest()->first();

        $PilihanMenu = UserMenu::where('username', $username)->where('active', 1)
            ->where('user_id', $user_id)
            ->where('active', 1)
            ->where('link', $link)->first();

        $page = UserPage::where('username', $username)->where('user_id', $user_id)->where('active', 1)->where('menu_id', $PilihanMenu->id)->first();

        if (!empty($page)) {
            $listposting = DB::table('user_posts')->where('user_posts.username',  $username)->where('user_posts.user_id', $user_id)
                ->where('user_posts.active', 1)
                ->where('user_posts.deleted_at', NULL)
                ->where('user_posts.page_id', $page->id)
                ->join('user_pages', 'user_posts.page_id', '=', 'user_pages.id')
                ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
                ->select('user_posts.*', 'user_pages.title AS namapage', 'user_menus.typepage AS Tipe', 'user_menus.link AS linkmenu')->get();
        } else {
            $listposting = '';
        }

        $itempage = DB::table('user_pages')->where('user_pages.username',  $username)->where('user_pages.user_id', $user_id)
            ->where('user_pages.active', 1)
            ->where('user_pages.deleted_at', NULL)
            ->where('menu_id', $PilihanMenu->id)
            ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
            ->select('user_pages.*', 'user_menus.title AS namamenu', 'user_menus.typepage AS Tipe')->first();

        $filePDF = UserPost::where('active', 1)
            ->where('user_id', $user_id)
            ->where('menu_id', $PilihanMenu->id)->first();

        if (!empty($settingumum)) {
            $theme = $settingumum->tema;
        } else {
            $theme = 'light';
        }

        if ($theme == 'dark') {
            $warna = 'bg3';
        } elseif ($theme == 'light') {
            $warna = 'bg0';
        } else {
            $warna = 'bg0';
        }

        if ($theme == 'dark') {
            $warnatext = 'cl0';
        } elseif ($theme == 'light') {
            $warnatext = 'cl5';
        } else {
            $warnatext = 'cl5';
        }

        if ($PilihanMenu->typepage == 'homepage') {

            return view('userfront.newface.homepage', compact('menu', 'slider', 'banner', 'footer', 'page', 'socialmedia', 'settingumum', 'listposting', 'warna', 'warnatext'));
            // } elseif ($PilihanMenu->typepage == 'sliderpage') {


            //     return view('userfront.store.sliderpage', compact('menu', 'slider', 'banner', 'footer', 'page', 'socialmedia', 'settingumum', 'listposting', 'itempage', 'warna'));
        } elseif ($PilihanMenu->typepage == 'postpage') {


            return view('userfront.newface.post', compact('menu', 'slider', 'banner', 'footer', 'page', 'socialmedia', 'settingumum', 'listposting', 'itempage', 'warna', 'warnatext'));
        } elseif ($PilihanMenu->typepage == 'textpage') {


            return view('userfront.newface.text', compact('menu', 'slider', 'banner', 'footer', 'page', 'socialmedia', 'settingumum', 'listposting', 'itempage', 'warna', 'warnatext', 'itemtext'));
        }
        // elseif ($PilihanMenu->typepage == 'pdfpage') {


        //     return view('userfront.store.pdf', compact('menu', 'slider', 'banner', 'footer', 'page', 'socialmedia', 'settingumum', 'listposting', 'filePDF', 'warna'));
        // }
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function postdetail(Request $request, $link, $linkpost)
    {
        $user_id = env('USERID');
        $username = env('USERNAME');

        $settingumum = UserSettingUmum::where('username', $username)->where('user_id', $user_id)->where('active', 1)->first();

        $menu = UserMenu::where('username', $username)->where('user_id', $user_id)->where('active', 1)->OrderBy('order', 'ASC')->get();

        $footer = UserFooter::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $banner = UserBanner::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $slider = UserSlider::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $socialmedia = UserSocialMedia::where('username', $username)->where('user_id', $user_id)->where('active', 1)->get();

        $itemtext = UserText::where('username', $username)->where('user_id', $user_id)->where('active', 1)->latest()->first();

        $PilihanMenu = UserMenu::where('username', $username)->where('active', 1)
            ->where('user_id', $user_id)
            ->where('active', 1)
            ->where('link', $link)->first();

        $page = UserPage::where('username', $username)->where('user_id', $user_id)->where('active', 1)->latest()->get();

        $itempage = DB::table('user_pages')->where('user_pages.username',  $username)->where('user_pages.user_id', $user_id)
            ->where('user_pages.active', 1)
            ->where('user_pages.deleted_at', NULL)
            ->where('menu_id', $PilihanMenu->id)
            ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
            ->select('user_pages.*', 'user_menus.title AS namamenu', 'user_menus.typepage AS Tipe')->first();


        $itemposting = UserPost::where('active', 1)->where('user_id', $user_id)->where('link', $linkpost)->first();

        $listposting = DB::table('user_posts')->where('user_posts.username',  $username)->where('user_posts.user_id', $user_id)
            ->where('user_posts.active', 1)
            ->where('user_posts.deleted_at', NULL)
            ->where('user_posts.link', $linkpost)
            ->join('user_pages', 'user_posts.page_id', '=', 'user_pages.id')
            ->join('user_menus', 'user_pages.menu_id', '=', 'user_menus.id')
            ->select('user_posts.*', 'user_pages.title AS namapage', 'user_menus.typepage AS Tipe')->first();

        $detailfoto = UserPostDetail::where('userpost_id', $itemposting->id)->get();

        if (!empty($settingumum)) {
            $theme = $settingumum->tema;
        } else {
            $theme = 'light';
        }

        if ($theme == 'dark') {
            $warna = 'bg3';
        } elseif ($theme == 'light') {
            $warna = 'bg0';
        } else {
            $warna = 'bg0';
        }

        if ($theme == 'dark') {
            $warnatext = 'cl0';
        } elseif ($theme == 'light') {
            $warnatext = 'cl5';
        } else {
            $warnatext = 'cl5';
        }


        // if ($listposting->Tipe == 'sliderpage') {

        //     return view('userfront.store.sliderpagedetail', compact('settingumum', 'menu', 'footer', 'banner', 'slider', 'socialmedia', 'page', 'itempage', 'itemposting', 'detailfoto', 'warna'));
        // } else {

        //}

        return view('userfront.newface.postdetail', compact('settingumum', 'menu', 'footer', 'banner', 'slider', 'socialmedia', 'page', 'itempage', 'itemposting', 'warna', 'warnatext', 'detailfoto', 'itemtext'));
    }
}
