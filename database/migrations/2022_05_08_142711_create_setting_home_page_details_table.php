<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingHomePageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_home_page_details', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('settinghome_id')->nullable();
            $table->string('slidernumber', 5)->nullable();
            $table->string('indexslider', 10)->nullable();
            $table->text('keterangan1')->nullable();
            $table->text('keterangan2')->nullable();
            $table->text('keterangan3')->nullable();

            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->string('deleted_by', 50)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting_home_page_details');
    }
}
