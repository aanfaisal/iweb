<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingHomePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_home_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sliderbackground', 150)->nullable();
            $table->string('theme', 50)->nullable()->comment('white, black');
            $table->tinyText('caption')->nullable();

            $table->string('kodenegara', 50)->nullable();
            $table->string('no_whatapps', 50)->nullable();

            $table->string('active', 5)->nullable()->default('0')->comment('1 = yes 0 = no');

            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->string('deleted_by', 50)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting_home_pages');
    }
}
