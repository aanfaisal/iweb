<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('username', 100)->nullable();
            $table->bigInteger('menu_id')->nullable();
            $table->bigInteger('page_id')->nullable();
            $table->string('foto', 100)->nullable();
            $table->string('thumbnail', 100)->nullable();
            $table->string('title')->nullable();
            $table->string('detail')->nullable();
            $table->string('link')->nullable();
            $table->string('category')->nullable();
            $table->string('file')->nullable();
            $table->string('active', 5)->nullable()->default('0')->comment('1 = yes 0 = no');

            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->string('deleted_by', 50)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_posts');
    }
}
