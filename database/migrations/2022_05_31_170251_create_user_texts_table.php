<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('username', 100)->nullable();
            $table->bigInteger('menu_id')->nullable();
            $table->bigInteger('page_id')->nullable();
            $table->string('title1')->nullable();
            $table->string('foto1')->nullable();
            $table->longText('detail1')->nullable();
            $table->string('title2')->nullable();
            $table->string('foto2')->nullable();
            $table->longText('detail2')->nullable();
            $table->string('link')->nullable();

            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->string('deleted_by', 50)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_texts');
    }
}
