<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('username', 100)->nullable();
            $table->string('icon', 50)->nullable();
            $table->string('title', 100)->nullable();
            $table->text('detail')->nullable();
            $table->string('typepage', 50)->nullable()->comment('1. homepage 2. promopage 3. postpage 4. pdfpage');
            $table->string('order', 5)->nullable();
            $table->string('link')->nullable();
            $table->string('active', 5)->nullable()->default('0')->comment('1 = yes 0 = no');

            $table->string('created_by', 50)->nullable();
            $table->string('updated_by', 50)->nullable();
            $table->string('deleted_by', 50)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_menus');
    }
}
