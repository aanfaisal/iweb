<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SettingHomePage;

class SettingHomepageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ngosongin tabel
        DB::table('setting_home_pages')->truncate();

        SettingHomePage::create([
            'id' => 1,
            'sliderbackground' => 'Slider_20220513090006.png',
            'theme' => 'white',
            'caption' => 'Voxy X',
            'kodenegara' => '+62',
            'no_whatapps' => '85156571133',
            'active' => '1',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-13 02:00:07',
            'updated_at' => '2022-05-13 02:00:07',
            'deleted_at' => NULL
        ]);
    }
}
