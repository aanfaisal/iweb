<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SettingBiaya;

class SettingBiayaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ngosongin tabel
        DB::table('setting_biayas')->truncate();

        SettingBiaya::create([
            'id' => 1,
            'namapaket' => 'Basic',
            'hargaasli' => 399000,
            'hargadiskon' => 600000,
            'jumlahmenu' => '3',
            'ket' => 'Keterangan Basic',
            'warna' => 'blue',
            'active' => '0',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiaya::create([
            'id' => 2,
            'namapaket' => 'Premium',
            'hargaasli' => 720000,
            'hargadiskon' => 449000,
            'jumlahmenu' => '5',
            'ket' => 'Keterangan Premium',
            'warna' => 'default',
            'active' => '0',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiaya::create([
            'id' => 3,
            'namapaket' => 'Bisnis',
            'hargaasli' => 18000000,
            'hargadiskon' => 1349000,
            'jumlahmenu' => '5',
            'ket' => 'Keterangan Bisnis',
            'warna' => 'red',
            'active' => '0',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiaya::create([
            'id' => 4,
            'namapaket' => 'Platinum',
            'hargaasli' => 21000000,
            'hargadiskon' => 1449000,
            'jumlahmenu' => '5',
            'ket' => 'Keterangan Platinum',
            'warna' => 'green',
            'active' => '0',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);
    }
}
