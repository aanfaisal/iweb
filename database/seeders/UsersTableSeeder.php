<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ngosongin tabel
        DB::table('users')->truncate();

        // Membuat role super
        $adminRole = new Role();
        $adminRole->name = 'admin';
        $adminRole->save();

        // Membuat role admin
        $memberRole = new Role();
        $memberRole->name = 'user';
        $memberRole->save();

        // Membuat sample admin
        $admin = new User();
        $admin->name = 'Admin';
        $admin->username = 'admin';
        $admin->email = 'admin@iweb.co.id';
        $admin->password = Hash::make('adminadmin');
        $admin->no_whatapps = '+6285173300054';
        $admin->paket = 'Platinum';
        $admin->paket_aktif = 1;
        $admin->save();
        $admin->assignRole($adminRole);

        // Membuat sample member
        $member0 = new User();
        $member0->name = 'user';
        $member0->username = 'user';
        $member0->email = 'user@iweb.co.id';
        $member0->password = Hash::make('useruser');
        $admin->no_whatapps = '';
        $admin->paket = 'Basic';
        $admin->paket_aktif = 1;
        $member0->save();
        $member0->assignRole($memberRole);

        $permission = [
            'dashboard.list',

            'dashboarduser.list',

            'acl.list',
            'acl.create',
            'acl.edit',
            'acl.delete',

            'user.list',
            'user.create',
            'user.edit',
            'user.delete',

        ];

        $permissionuser = [
            'dashboarduser.list',


        ];

        $admin->givePermissionTo($permission);

        $member0->givePermissionTo($permission);
    }
}
