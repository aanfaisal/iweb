<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(PermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(SettingBiayaSeeder::class);
        $this->call(SettingBiayaDetailSeeder::class);

        $this->call(SettingHomepageSeeder::class);
        $this->call(SettingHomepageDetailSeeder::class);
    }
}
