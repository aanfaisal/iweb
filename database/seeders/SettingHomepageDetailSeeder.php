<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SettingHomePageDetail;

class SettingHomepageDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ngosongin tabel
        DB::table('setting_home_page_details')->truncate();


        SettingHomePageDetail::create([
            'id' => 1,
            'settinghome_id' => 1,
            'slidernumber' => '1',
            'indexslider' => 'rs-3045',
            'keterangan1' => 'Apapun Bisnis Anda',
            'keterangan2' => 'IWEB Solusi Efektif',
            'keterangan3' => 'Promosi Bisnismu',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-13 02:00:19',
            'updated_at' => '2022-05-13 02:00:19',
            'deleted_at' => NULL
        ]);

        SettingHomePageDetail::create([
            'id' => 2,
            'settinghome_id' => 1,
            'slidernumber' => '2',
            'indexslider' => 'rs-2',
            'keterangan1' => 'Yuk Join',
            'keterangan2' => 'IWEB Solusi Efektif',
            'keterangan3' => 'Promosi Bisnismu',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-13 02:00:19',
            'updated_at' => '2022-05-13 02:00:19',
            'deleted_at' => NULL
        ]);

        SettingHomePageDetail::create([
            'id' => 3,
            'settinghome_id' => 1,
            'slidernumber' => '3',
            'indexslider' => 'rs-3',
            'keterangan1' => 'Bikin Website ?',
            'keterangan2' => 'Murah dan Ga Bikin Pusing',
            'keterangan3' => 'Tanpa Harus Coding',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-13 02:00:19',
            'updated_at' => '2022-05-13 02:00:19',
            'deleted_at' => NULL
        ]);
    }
}
