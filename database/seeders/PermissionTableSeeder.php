<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ngosongin tabel
        DB::table('permissions')->delete();

        //bikin data dulu dalam array untuk dimasukkin kedatabase
        $permissions = [
            'dashboard.list',

            'dashboarduser.list',

            'acl.list',
            'acl.create',
            'acl.edit',
            'acl.delete',

            'user.list',
            'user.create',
            'user.edit',
            'user.delete',



        ];

        //masukkan data array ke database
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
