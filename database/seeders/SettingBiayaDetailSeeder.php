<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\SettingBiayaDetail;

class SettingBiayaDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //ngosongin tabel
        DB::table('setting_biaya_details')->truncate();


        // Menu 1

        SettingBiayaDetail::create([
            'id' => 1,
            'settingbiaya_id' => 1,
            'detail' => 'Domain GRATIS selama 1 tahun',
            'ket' => 'Domain GRATIS selama 1 tahun',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);


        SettingBiayaDetail::create([
            'id' => 2,
            'settingbiaya_id' => 1,
            'detail' => '3 menu bar',
            'ket' => '3 menu bar',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 3,
            'settingbiaya_id' => 1,
            'detail' => 'Layanan bantuan customer service',
            'ket' => 'Layanan bantuan customer service',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        // Menu 2

        SettingBiayaDetail::create([
            'id' => 4,
            'settingbiaya_id' => 2,
            'detail' => 'Domain GRATIS selama 1 tahun',
            'ket' => 'Domain GRATIS selama 1 tahun',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);


        SettingBiayaDetail::create([
            'id' => 5,
            'settingbiaya_id' => 2,
            'detail' => '5 Menu Bar',
            'ket' => '5 Menu Bar',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 6,
            'settingbiaya_id' => 2,
            'detail' => 'Layanan bantuan customer service',
            'ket' => 'Layanan bantuan customer service',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        // Menu 3
        SettingBiayaDetail::create([
            'id' => 7,
            'settingbiaya_id' => 3,
            'detail' => 'Domain GRATIS selama 1 tahun',
            'ket' => 'Domain GRATIS selama 1 tahun',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);


        SettingBiayaDetail::create([
            'id' => 8,
            'settingbiaya_id' => 3,
            'detail' => '5 Menu Bar',
            'ket' => '5 Menu Bar',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 9,
            'settingbiaya_id' => 3,
            'detail' => 'Layanan bantuan customer service',
            'ket' => 'Layanan bantuan customer service',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 10,
            'settingbiaya_id' => 3,
            'detail' => '15.000 kunjungan / bulan',
            'ket' => '15.000 kunjungan / bulan',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 11,
            'settingbiaya_id' => 3,
            'detail' => 'Aman google analytic',
            'ket' => 'Aman google analytic',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 12,
            'settingbiaya_id' => 3,
            'detail' => 'Aman google adsense',
            'ket' => 'Aman google adsense',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        // Menu 4
        SettingBiayaDetail::create([
            'id' => 13,
            'settingbiaya_id' => 4,
            'detail' => 'Domain GRATIS selama 1 tahun',
            'ket' => 'Domain GRATIS selama 1 tahun',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);


        SettingBiayaDetail::create([
            'id' => 14,
            'settingbiaya_id' => 4,
            'detail' => '5 Menu Bar',
            'ket' => '5 Menu Bar',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 15,
            'settingbiaya_id' => 4,
            'detail' => 'Layanan bantuan customer service',
            'ket' => 'Layanan bantuan customer service',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 16,
            'settingbiaya_id' => 4,
            'detail' => '25.000 kunjungan / bulan',
            'ket' => '25.000 kunjungan / bulan',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 17,
            'settingbiaya_id' => 4,
            'detail' => 'Aman google analytic',
            'ket' => 'Aman google analytic',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);

        SettingBiayaDetail::create([
            'id' => 18,
            'settingbiaya_id' => 4,
            'detail' => 'Aman google adsense',
            'ket' => 'Aman google adsense',
            'created_by' => '1',
            'updated_by' => NULL,
            'deleted_by' => NULL,
            'created_at' => '2022-05-12 08:30:25',
            'updated_at' => '2022-05-12 08:30:25',
            'deleted_at' => NULL
        ]);
    }
}
