## Setting Main Biaya & Detail

nama
ket
warna

php artisan crud:generate SettingBiaya --fields='nama#string; ket#string; warna#string' --pk="id" --view-path=main --controller-namespace=Main --form-helper=html

php artisan crud:generate SettingBiayaDetail --fields='settingbiaya_id#string; detail#string; ket#string' --pk="id" --view-path=main --controller-namespace=Main --form-helper=html

## Setting Homepage

php artisan crud:generate SettingHomePage --fields='sliderbackground#string; theme#string' --pk="id" --view-path=main --controller-namespace=Main --form-helper=html

php artisan crud:generate SettingHomePageDetail --fields='settinghome_id#string; slidernumber#string; keterangan1#string; keterangan2#string; keterangan3#string' --pk="id" --view-path=main --controller-namespace=Main --form-helper=html

## Setting Tentang

php artisan crud:generate SettingTentang --fields='judul#string; keterangan#string' --pk="id" --view-path=main --controller-namespace=Main --form-helper=html

## Setting Testimonial

php artisan crud:generate SettingTestimoni --fields='judul#string; keterangan#string; foto#string' --pk="id" --view-path=main --controller-namespace=Main --form-helper=html

# ================================================================

# ================================================================

## homepage ada

background image : 1
image promo : 2

typepage => 1. homepage 2. promopage 3. postpage 4. pdfpage

## User Umum Setting

icon
namaweb
tema
web address
user_id
nama usaha :
keterangan usaha :
alamat usaha :

php artisan crud:generate UserSettingUmum --fields='user_id#string; username#string; logo#string; namaweb#string; tema#string; webaddress#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## socialmedia :

user_id => isinya user
icon + detail

php artisan crud:generate UserSocialMedia --fields='user_id#string; username#string; icon#string; detail#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## footers

php artisan crud:generate UserFooter --fields='user_id#string; username#string; title#string; detail#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## user menus

php artisan crud:generate UserMenu --fields='user_id#string; username#string; icon#string; title#string; detail#string; typepage#string; order#string; link#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## user Pages

php artisan crud:generate UserPage --fields='user_id#string; username#string; menu_id#string; foto#string; thumbnail#string; title#string; detail#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## user sliders

php artisan crud:generate UserSlider --fields='user_id#string; username#string; menu_id#string; page_id#string; foto#string; thumbnail#string; caption#string; sliderTextType#string; sliderType#string; order#string; link#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## user Banners

php artisan crud:generate UserBanner --fields='user_id#string; username#string; foto#string; thumbnail#string; title#string; order#string; link#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

# ================================================================

## promo page

title
default foto
foto : 3 sd 10 (slider)
detail

## post pages

title
default foto
detail

## pdf pages

title
link pdf

## slider type text

fadeInDown
fadeInUp
zoomIn
rollIn
lightSpeedIn
slideInUp
rotateInDownLeft
rotateInUpRight
rotateIn

## User Posts

php artisan crud:generate UserPost --fields='user_id#string; username#string; menu_id#string; page_id#string; foto#string; thumbnail#string; title#string; detail#string; link#string; category#string; file#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## User Posts Detail

php artisan crud:generate UserPostDetail --fields='userpost_id#string; user_id#string; username#string; menu_id#string; page_id#string; foto#string; thumbnail#string; title#string; detail#string; link#string; category#string; file#string' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## User Texts

php artisan crud:generate UserText --fields='user_id#string; username#string; menu_id#string; page_id#string; title1#string; foto1#string; detail1#string; title2#string; foto2#string; detail2#string; link#string;' --pk="id" --view-path=user --controller-namespace=User --form-helper=html

## User Stores

php artisan crud:generate UserStore --fields='user_id#string; username#string; menu_id#string; page_id#string; title#string; foto#string; detail#string; harga#string; link#string;' --pk="id" --view-path=user --controller-namespace=User --form-helper=html
